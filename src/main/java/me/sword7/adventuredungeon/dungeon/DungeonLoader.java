package me.sword7.adventuredungeon.dungeon;

import me.sword7.adventuredungeon.AdventureDungeon;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class DungeonLoader extends BukkitRunnable implements Listener {

    private int count;

    public DungeonLoader() {
        Plugin plugin = AdventureDungeon.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        runTaskTimer(plugin, 20, 20);
    }


    @Override
    public void run() {
        count++;
        if (count % 3 == 0) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                activateNear(player.getLocation());
            }
        }
        if (count % 180 == 0) {
            count = 0;
            deActivate();
        }

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        activateNear(e.getPlayer().getLocation());
    }

    @EventHandler
    public void onTp(PlayerTeleportEvent e) {
        activateNear(e.getTo());
    }

    private void activateNear(Location location) {
        DungeonCache.activateNearby(location);
    }

    private void deActivate() {
        DungeonCache.deActivateDungeons();
    }

    //on break make sure loaded?


}
