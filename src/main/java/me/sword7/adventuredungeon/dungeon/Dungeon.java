package me.sword7.adventuredungeon.dungeon;

import me.sword7.adventuredungeon.lootpool.ILootPool;
import me.sword7.adventuredungeon.lootpool.LootTag;
import me.sword7.adventuredungeon.mob.DungeonMobType;
import me.sword7.adventuredungeon.mob.IBestiary;
import me.sword7.adventuredungeon.util.math.Cardinal;
import me.sword7.adventuredungeon.util.math.IBound;
import me.sword7.adventuredungeon.util.math.Point;
import me.sword7.adventuredungeon.util.math.Point2D;
import org.bukkit.Bukkit;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public abstract class Dungeon {

    private UUID id;
    private String name;
    private DungeonType type;
    private UUID worldId;
    private Point entrance;
    private Cardinal direction;
    private Point center;
    private int radius;
    private List<Point2D> grids;
    private Map<Point, List<LootTag>> chestLocations;
    private Map<Point, DungeonMobType> spawnerLocations;
    private Set<UUID> visitors;
    private List<IBound> bounds;

    private List<DungeonTag> tags;
    private ILootPool lootPool;
    private IBestiary bestiary;

    public Dungeon(UUID id,
                   String name,
                   DungeonType type,
                   UUID worldId,
                   Point entrance,
                   Cardinal direction,
                   Point center,
                   int radius,
                   List<Point2D> grids,
                   Map<Point, List<LootTag>> chestLocations,
                   Map<Point, DungeonMobType> spawnerLocations,
                   Set<UUID> visitors,
                   List<IBound> bounds,
                   List<DungeonTag> tags,
                   ILootPool lootPool,
                   IBestiary bestiary) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.worldId = worldId;
        this.entrance = entrance;
        this.direction = direction;
        this.center = center;
        this.radius = radius;
        this.grids = grids;
        this.chestLocations = chestLocations;
        this.spawnerLocations = spawnerLocations;
        this.visitors = visitors;
        this.bounds = bounds;
        this.tags = tags;
        this.lootPool = lootPool;
        this.bestiary = bestiary;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return this.name;
    }

    public DungeonType getType() {
        return type;
    }

    public UUID getWorldId() {
        return worldId;
    }

    public Point getEntrance() {
        return entrance;
    }

    public Cardinal getDirection() {
        return direction;
    }

    public Point getCenter() {
        return center;
    }

    public int getRadius() {
        return radius;
    }

    public List<Point2D> getGrids() {
        return grids;
    }

    public Map<Point, List<LootTag>> getChestLocations() {
        return chestLocations;
    }


    public Map<Point, DungeonMobType> getSpawnerLocations() {
        return spawnerLocations;
    }

    public Set<UUID> getVisitors() {
        return visitors;
    }

    public List<IBound> getBounds() {
        return bounds;
    }

    public List<DungeonTag> getTags() {
        return tags;
    }

    public ILootPool getLootPool() {
        return lootPool;
    }

    public IBestiary getBestiary() {
        return bestiary;
    }

    public void clear() {
        grids.clear();
        for (List<LootTag> tags : chestLocations.values()) {
            tags.clear();
        }
        chestLocations.clear();
        spawnerLocations.clear();
        visitors.clear();
        bounds.clear();
        //lootPool.clear();
    }

    public void print() {
        printLn("===");
        printLn(id.toString());
        printLn(name);
        printLn(type.toString());
        printLn(worldId.toString());
        printLn(entrance.getAsString());
        printLn(direction.toString());
        printLn(center.getAsString());
        printLn(String.valueOf(radius));
        printLn("grids:");
        for (Point2D p : grids) {
            printLn(p.getAsString());
        }
        printLn("chestLocations:");
        for (Point p : chestLocations.keySet()) {
            printLn(p.getAsString());
        }
        printLn("spawnerLocations:");
        for (Point p : spawnerLocations.keySet()) {
            printLn(p.getAsString());
        }
        printLn("visitors:");
        for (UUID id : visitors) {
            printLn(id.toString());
        }
        printLn("bounds");
        for (IBound b : bounds) {
            printLn(b.getMax().getAsString() + " => " + b.getMax().getAsString());
        }
        printLn("===");
    }

    private void printLn(String s) {
        Bukkit.getConsoleSender().sendMessage(s);
    }

}
