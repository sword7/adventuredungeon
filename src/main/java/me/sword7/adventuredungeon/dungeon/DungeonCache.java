package me.sword7.adventuredungeon.dungeon;

import me.sword7.adventuredungeon.dungeon.storage.DungeonFlatFile;
import me.sword7.adventuredungeon.dungeon.storage.DungeonLoaderFlatFile;
import me.sword7.adventuredungeon.util.Util;
import me.sword7.adventuredungeon.util.math.IBound;
import me.sword7.adventuredungeon.util.math.Point;
import me.sword7.adventuredungeon.util.math.Point2D;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.*;

public class DungeonCache {

    public static final int DUNGEON_GRID_LENGTH = 100;
    private static Map<UUID, Map<Point2D, List<UUID>>> dungeonLoadingMap;

    private static Map<UUID, Dungeon> dungeonMap = new HashMap<>();
    private static Map<UUID, Dungeon> cachedDungeons = new HashMap<>();

    public static final int BOUND_GRID_LENGTH = 48;
    private static Map<UUID, Map<Point2D, Map<UUID, List<IBound>>>> boundsMap = new LinkedHashMap<>();

    private static DungeonLoaderFlatFile dungeonLoaderFlatFile = new DungeonLoaderFlatFile();
    private static DungeonFlatFile dungeonFlatFile = new DungeonFlatFile();

    public static void load() {
        dungeonLoadingMap = dungeonLoaderFlatFile.fetch();
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            activateNearby(player.getLocation());
        }
    }

    public static void save() {
        dungeonLoaderFlatFile.store(dungeonLoadingMap);
    }

    public static void shutdown() {
        save();
        for (Map.Entry<UUID, Map<Point2D, List<UUID>>> entry : dungeonLoadingMap.entrySet()) {
            entry.getValue().clear();
        }

        for (Dungeon dungeon : dungeonMap.values()) {
            dungeonFlatFile.store(dungeon);
            dungeon.clear();
        }

        for (Dungeon dungeon : cachedDungeons.values()) {
            dungeonFlatFile.store(dungeon);
            dungeon.clear();
        }

        dungeonLoadingMap.clear();
        dungeonMap.clear();
        cachedDungeons.clear();

    }

    public static void activateNearby(Location location) {
        UUID worldId = location.getWorld().getUID();
        Map<Point2D, List<UUID>> dungeonMap = dungeonLoadingMap.get(worldId);
        if (dungeonMap != null) {
            List<Point2D> nearbyGrids = getNearbyGrids(location, 1);
            for (Point2D grid : nearbyGrids) {
                List<UUID> dungeons = dungeonMap.get(grid);
                if (dungeons != null) {
                    for (UUID dungeonId : dungeons) {
                        activateDungeon(dungeonId);
                    }
                }
            }
        }
    }

    public static boolean hasNearby(Location location, double radius) {
        UUID worldId = location.getWorld().getUID();
        Map<Point2D, List<UUID>> dungeonMap = dungeonLoadingMap.get(worldId);
        if (dungeonMap != null) {
            List<Point2D> nearbyGrids = getNearbyGrids(location, (int) (radius / 100));
            for (Point2D grid : nearbyGrids) {
                List<UUID> dungeons = dungeonMap.get(grid);
                if (dungeons != null) {
                    return true;
                }
            }
        }
        return false;
    }

    private static List<Point2D> getNearbyGrids(Location location, int r) {
        List<Point2D> toCheck = new ArrayList<>();
        Point2D gridSquare = Util.gridPointFrom(DUNGEON_GRID_LENGTH, location);
        for (int x = -r; x <= r; x++) {
            for (int z = -r; z <= r; z++) {
                Point2D p = gridSquare.clone();
                p.add(x, z);
                toCheck.add(p);
            }
        }

        return toCheck;
    }

    public static void activateDungeon(UUID dungeonId) {
        if (!dungeonMap.containsKey(dungeonId)) {
            if (cachedDungeons.containsKey(dungeonId)) {
                Dungeon dungeon = cachedDungeons.get(dungeonId);
                cachedDungeons.remove(dungeonId);
                dungeonMap.put(dungeonId, dungeon);
            } else {
                Dungeon dungeon = dungeonFlatFile.fetch(dungeonId);
                if (dungeon != null) loadDungeonData(dungeon);
            }
        }
    }

    public static void registerDungeon(Dungeon dungeon) {
        UUID dungeonId = dungeon.getId();
        UUID worldId = dungeon.getWorldId();
        Map<Point2D, List<UUID>> gridDungeonMap = dungeonLoadingMap.containsKey(worldId) ? dungeonLoadingMap.get(worldId) : new HashMap<>();
        for (Point2D grid : dungeon.getGrids()) {
            List<UUID> dungeons = gridDungeonMap.containsKey(grid) ? gridDungeonMap.get(grid) : new ArrayList<>();
            dungeons.add(dungeonId);
            gridDungeonMap.put(grid, dungeons);
        }
        dungeonLoadingMap.put(worldId, gridDungeonMap);

        loadDungeonData(dungeon);
    }

    private static void loadDungeonData(Dungeon dungeon) {
        UUID dungeonId = dungeon.getId();
        dungeonMap.put(dungeonId, dungeon);
        UUID worldId = dungeon.getWorldId();

        //bounds
        Map<Point2D, Map<UUID, List<IBound>>> gridDungeonBoundMap = boundsMap.containsKey(worldId) ? boundsMap.get(worldId) : new HashMap<>();
        for (IBound bound : dungeon.getBounds()) {
            for (Point2D grid : Util.getBoundGrids(bound, BOUND_GRID_LENGTH)) {
                Map<UUID, List<IBound>> dungeonBoundsMap = gridDungeonBoundMap.containsKey(grid) ? gridDungeonBoundMap.get(grid) : new HashMap<>();
                List<IBound> bounds = dungeonBoundsMap.containsKey(dungeonId) ? dungeonBoundsMap.get(dungeonId) : new ArrayList<>();
                bounds.add(bound);
                dungeonBoundsMap.put(dungeonId, bounds);
                gridDungeonBoundMap.put(grid, dungeonBoundsMap);
            }
        }
        boundsMap.put(worldId, gridDungeonBoundMap);

    }


    public static void deActivateDungeons() {
        List<Dungeon> toDeActivate = new ArrayList<>();
        for (Dungeon dungeon : dungeonMap.values()) {
            World world = Bukkit.getWorld(dungeon.getWorldId());
            if (world != null) {
                Point p = dungeon.getCenter();
                Location center = new Location(world, p.getX(), p.getY(), p.getZ());
                int rad = dungeon.getRadius() + 100;
                boolean playerNearby = false;
                for (Entity entity : world.getNearbyEntities(center, rad, rad, rad)) {
                    if (entity instanceof Player) {
                        playerNearby = true;
                        break;
                    }
                }
                if (!playerNearby) {
                    toDeActivate.add(dungeon);
                }
            }
        }

        for (Dungeon dungeon : toDeActivate) {
            deActivate(dungeon);
        }

    }

    private static void deActivate(Dungeon dungeon) {

        UUID dungeonId = dungeon.getId();
        dungeonMap.remove(dungeonId);
        cachedDungeons.put(dungeonId, dungeon);

        //remove eldest from cache
        if (cachedDungeons.size() > 10 + 1) {
            Iterator<UUID> iterator = cachedDungeons.keySet().iterator();
            if (iterator.hasNext()) {
                UUID eldestId = iterator.next();
                Dungeon eldest = cachedDungeons.get(eldestId);
                cachedDungeons.remove(eldestId);
                forgetDungeon(eldest);
                dungeonFlatFile.store(eldest);
                eldest.clear();
            }
        }

    }

    private static void forgetDungeon(Dungeon dungeon) {

        UUID dungeonId = dungeon.getId();

        //bounds
        UUID worldId = dungeon.getWorldId();
        if (boundsMap.containsKey(worldId)) {
            Map<Point2D, Map<UUID, List<IBound>>> gridDungeonBoundMap = boundsMap.get(worldId);
            for (IBound bound : dungeon.getBounds()) {
                for (Point2D grid : Util.getBoundGrids(bound, BOUND_GRID_LENGTH)) {
                    if (gridDungeonBoundMap.containsKey(grid)) {
                        Map<UUID, List<IBound>> dungeonBoundMap = gridDungeonBoundMap.get(grid);
                        dungeonBoundMap.remove(dungeonId);
                        if (dungeonBoundMap.isEmpty()) gridDungeonBoundMap.remove(grid);
                    }
                }
            }
        }

        dungeon.clear();
    }

    public static Dungeon getDungeonAt(Location location) {
        UUID worldId = location.getWorld().getUID();
        if (boundsMap.containsKey(worldId)) {
            Map<Point2D, Map<UUID, List<IBound>>> gridDungeonBoundMap = boundsMap.get(worldId);
            Point2D grid = Util.gridPointFrom(BOUND_GRID_LENGTH, location);
            if (gridDungeonBoundMap.containsKey(grid)) {
                Map<UUID, List<IBound>> dungeonBoundMap = gridDungeonBoundMap.get(grid);
                for (Map.Entry<UUID, List<IBound>> entry : dungeonBoundMap.entrySet()) {
                    for (IBound bound : entry.getValue()) {
                        if (bound.contains(location)) {
                            return getDungeon(entry.getKey());
                        }
                    }
                }
            }

        }

        return null;
    }

    private static Dungeon getDungeon(UUID dungeonId) {
        if (dungeonMap.containsKey(dungeonId)) {
            return dungeonMap.get(dungeonId);
        } else if (cachedDungeons.containsKey(dungeonId)) {
            return cachedDungeons.get(dungeonId);
        } else {
            return null;
        }
    }


}
