package me.sword7.adventuredungeon.dungeon.container;

import org.bukkit.*;
import org.bukkit.block.Block;

public class Pot implements IContainer {

    @Override
    public boolean isSelf(Block block) {
        return block.getType() == Material.FLOWER_POT;
    }

    @Override
    public void playBreakEffect(Location location) {
        World world = location.getWorld();
        world.spawnParticle(
                Particle.BLOCK_CRACK,
                location, 5, 0.1, 0.1, 0.1, Material.FLOWER_POT.createBlockData()
        );
        world.playSound(location, Sound.BLOCK_GLASS_BREAK, 1f, 0.5f);
    }

    @Override
    public double getDropHeightOffset() {
        return 0.2;
    }

}
