package me.sword7.adventuredungeon.dungeon.container;

import me.sword7.adventuredungeon.structure.HeadBlock;
import org.bukkit.*;
import org.bukkit.block.Block;

public class Crate implements IContainer {

    private HeadBlock headBlock = HeadBlock.CRATE_1;

    @Override
    public boolean isSelf(Block block) {
        return headBlock.isSelf(block);
    }

    @Override
    public void playBreakEffect(Location location) {
        World world = location.getWorld();
        world.spawnParticle(
                Particle.BLOCK_CRACK,
                location, 5, 0.1, 0.1, 0.1, Material.BARREL.createBlockData()
        );
        world.playSound(location, Sound.BLOCK_FENCE_GATE_CLOSE, 1f, 1.2f);
        world.playSound(location, Sound.ITEM_AXE_STRIP, 2f, 1.2f);
    }

    @Override
    public double getDropHeightOffset() {
        return 0.2;
    }
}
