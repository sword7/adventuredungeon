package me.sword7.adventuredungeon.dungeon.container;

public enum ContainerType {
    POT(new Pot()),
    BARREL(new Barrel()),
    CRATE(new Crate()),
    SKULL(new Skullcap()),
    ;

    private IContainer container;

    ContainerType(IContainer container) {
        this.container = container;
    }

    public IContainer getContainer() {
        return container;
    }

}
