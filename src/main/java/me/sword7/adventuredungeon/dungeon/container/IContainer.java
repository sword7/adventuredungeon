package me.sword7.adventuredungeon.dungeon.container;

import org.bukkit.Location;
import org.bukkit.block.Block;

public interface IContainer {

    boolean isSelf(Block block);

    void playBreakEffect(Location location);

    double getDropHeightOffset();

}
