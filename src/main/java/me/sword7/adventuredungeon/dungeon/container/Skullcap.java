package me.sword7.adventuredungeon.dungeon.container;

import org.bukkit.*;
import org.bukkit.block.Block;

public class Skullcap implements IContainer {

    @Override
    public boolean isSelf(Block block) {
        return block.getType() == Material.SKELETON_SKULL;
    }

    @Override
    public void playBreakEffect(Location location) {
        World world = location.getWorld();
        world.spawnParticle(
                Particle.BLOCK_CRACK,
                location, 5, 0.1, 0.1, 0.1, Material.BONE_BLOCK.createBlockData()
        );
        world.playSound(location, Sound.ENTITY_SKELETON_STEP, 1f, 0.8f);
    }

    @Override
    public double getDropHeightOffset() {
        return 0.2;
    }

}
