package me.sword7.adventuredungeon.dungeon;

import com.google.common.collect.ImmutableMap;
import me.sword7.adventuredungeon.dungeons.crypt.*;
import me.sword7.adventuredungeon.dungeons.lizardbase.*;
import me.sword7.adventuredungeon.structure.IThemeFactory;
import me.sword7.adventuredungeon.worldgen.IDungeonPlacer;
import org.bukkit.block.Biome;

import java.util.Map;

public enum DungeonType {

    CRYPT(new CryptPlacer(), new CryptThemeFactory()),
    LIZARD_BASE(new LizardBasePlacer(), new LizardBaseThemeFactory());
    //MOUNDS,
    //SLIME_SANCTUARY,
    //FIRE_TEMPLE,
    //MINOTAUR MAZE
    //SPIDER CAVE
    //PYRAMID UNDERGROUND
    //PARCORE TOWER
    //WIZARD BASE
    //WOODLAND MANSION
    //CASTLE
    //NETHER CASTLE
    //MAZE
    //NETHER SOULSAND CAVERNS
    //POE HILLS
    ;

    private String slug;
    private IDungeonPlacer placer;
    private IThemeFactory themeFactory;
    private Map<Biome, Double> biomeToChance;

    DungeonType(IDungeonPlacer placer, IThemeFactory themeFactory) {
        this.placer = placer;
        this.themeFactory = themeFactory;
        this.slug = this.toString().toLowerCase().replace("_", "");
    }

    public static void init() {
        CryptFrame.init();
        CryptLoot.init();
        CryptLootPool.init();
        CryptLootPoolSupplier.init();

        CRYPT.biomeToChance = new ImmutableMap.Builder<Biome, Double>()
                .put(Biome.FOREST, 0.007)
                .put(Biome.BIRCH_FOREST, 0.007)
                .put(Biome.DARK_FOREST, 0.007)
                .put(Biome.TALL_BIRCH_FOREST, 0.007)
                .put(Biome.TAIGA, 0.007)
                .put(Biome.TAIGA_MOUNTAINS, 0.007)
                .put(Biome.GIANT_SPRUCE_TAIGA, 0.007)
                .put(Biome.GIANT_TREE_TAIGA, 0.007)
                .put(Biome.SNOWY_MOUNTAINS, 0.007)
                .put(Biome.SNOWY_TAIGA, 0.007)
                .put(Biome.SNOWY_TUNDRA, 0.007)
                .put(Biome.SNOWY_TAIGA_MOUNTAINS, 0.007)
                .put(Biome.MOUNTAINS, 0.007)
                .put(Biome.GRAVELLY_MOUNTAINS, 0.007)
                .put(Biome.MODIFIED_GRAVELLY_MOUNTAINS, 0.007)
                .put(Biome.WOODED_MOUNTAINS, 0.007)
                .put(Biome.PLAINS, 0.007)
                .put(Biome.SUNFLOWER_PLAINS, 0.007)
                .put(Biome.BEACH, 0.007)
                .put(Biome.SAVANNA, 0.007)
                .put(Biome.SWAMP, 0.007).build();

        LizardBaseFrame.init();
        LizardBaseLoot.init();
        LizardBaseLootPoolSupplier.init();
        LIZARD_BASE.biomeToChance = new ImmutableMap.Builder<Biome, Double>()
                .put(Biome.FOREST, 0.007)
                .put(Biome.BIRCH_FOREST, 0.007)
                .put(Biome.DARK_FOREST, 0.007)
                .put(Biome.TALL_BIRCH_FOREST, 0.007)
                .put(Biome.TAIGA, 0.007)
                .put(Biome.TAIGA_MOUNTAINS, 0.007)
                .put(Biome.GIANT_SPRUCE_TAIGA, 0.007)
                .put(Biome.GIANT_TREE_TAIGA, 0.007)
                .put(Biome.SNOWY_MOUNTAINS, 0.007)
                .put(Biome.SNOWY_TAIGA, 0.007)
                .put(Biome.SNOWY_TUNDRA, 0.007)
                .put(Biome.SNOWY_TAIGA_MOUNTAINS, 0.007)
                .put(Biome.MOUNTAINS, 0.007)
                .put(Biome.GRAVELLY_MOUNTAINS, 0.007)
                .put(Biome.MODIFIED_GRAVELLY_MOUNTAINS, 0.007)
                .put(Biome.WOODED_MOUNTAINS, 0.007)
                .put(Biome.PLAINS, 0.007)
                .put(Biome.SUNFLOWER_PLAINS, 0.007)
                .put(Biome.BEACH, 0.007)
                .put(Biome.SAVANNA, 0.007)
                .put(Biome.SWAMP, 0.007).build();

    }

    public IDungeonPlacer getPlacer() {
        return placer;
    }

    public IThemeFactory getThemeFactory() {
        return themeFactory;
    }

    public Map<Biome, Double> getBiomeToChance() {
        return biomeToChance;
    }

    public String getSlug() {
        return slug;
    }


}