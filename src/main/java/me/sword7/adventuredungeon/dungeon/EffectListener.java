package me.sword7.adventuredungeon.dungeon;

import com.google.common.collect.ImmutableSet;
import me.sword7.adventuredungeon.AdventureDungeon;
import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.dungeon.container.ContainerType;
import me.sword7.adventuredungeon.dungeon.container.IContainer;
import me.sword7.adventuredungeon.lootpool.ILootPool;
import me.sword7.adventuredungeon.lootpool.LootTag;
import me.sword7.adventuredungeon.lootpool.item.EntityItem;
import me.sword7.adventuredungeon.lootpool.item.ILootItem;
import me.sword7.adventuredungeon.lootpool.loot.ILoot;
import me.sword7.adventuredungeon.mob.DungeonMobType;
import me.sword7.adventuredungeon.mob.IBestiary;
import me.sword7.adventuredungeon.util.math.Point;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class EffectListener implements Listener {

    private Random random = new Random();

    public EffectListener() {
        Plugin plugin = AdventureDungeon.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(ignoreCancelled = true)
    public void onHit(PlayerInteractEvent e) {
        if (e.hasItem() && WEAPONS.contains(e.getItem().getType()) && e.getAction() == Action.LEFT_CLICK_BLOCK) {
            Block block = e.getClickedBlock();
            for (ContainerType containerType : ContainerType.values()) {
                IContainer container = containerType.getContainer();
                if (container.isSelf(block)) {
                    Dungeon dungeon = DungeonCache.getDungeonAt(block.getLocation());
                    if (dungeon != null) {
                        block.setType(Material.AIR);
                        Location location = block.getLocation().add(0.5, container.getDropHeightOffset(), 0.5);
                        World world = location.getWorld();
                        world.playSound(location, Sound.ENTITY_PLAYER_ATTACK_SWEEP, 0.5f, 1f);
                        container.playBreakEffect(location);
                        ILootPool lootPool = dungeon.getLootPool();
                        if (random.nextDouble() < lootPool.getContainerDropChance(containerType)) {
                            ILootItem lootItem = lootPool.selectContainer(containerType, random);
                            if (lootItem instanceof EntityItem) {
                                ((EntityItem) lootItem).spawnEntity(location);
                            } else if (lootItem != null) {
                                world.dropItemNaturally(location, lootItem.create(random));
                            }
                        }
                    }
                    break;
                }
            }
        }

    }

    @EventHandler
    public void onPickUp(EntityPickupItemEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            ItemStack itemStack = e.getItem().getItemStack();
            if (itemStack.getType() == Material.RED_DYE && itemStack.getItemMeta().getCustomModelData() == 701) {
                player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1f, 1f);
                e.setCancelled(true);
                e.getItem().remove();
                double maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
                double health = player.getHealth();
                health += 2;
                if (health > maxHealth) health = maxHealth;
                player.setHealth(health);
                int foodLevel = player.getFoodLevel();
                foodLevel += 2;
                if (foodLevel > 20) foodLevel = 20;
                player.setFoodLevel(foodLevel);
            }
        }
    }

    private static final Set<Material> WEAPONS = buildWeapons();

    private static Set<Material> buildWeapons() {
        ImmutableSet.Builder<Material> builder = new ImmutableSet.Builder<>();

        builder.add(Material.WOODEN_SWORD).add(Material.STONE_SWORD).add(Material.IRON_SWORD)
                .add(Material.GOLDEN_SWORD).add(Material.DIAMOND_SWORD)
                .add(Material.WOODEN_AXE).add(Material.STONE_AXE).add(Material.IRON_AXE)
                .add(Material.GOLDEN_AXE).add(Material.DIAMOND_AXE);
        if (Version.getValue() >= 116) {
            builder.add(Material.NETHERITE_SWORD).add(Material.NETHERITE_AXE);
        }
        return builder.build();
    }

    @EventHandler
    public void onSpawn(SpawnerSpawnEvent e) {
        Block spawner = e.getSpawner().getBlock();
        Location location = spawner.getLocation();
        Dungeon dungeon = DungeonCache.getDungeonAt(spawner.getLocation());
        if (dungeon != null) {
            Map<Point, DungeonMobType> spawnerLocations = dungeon.getSpawnerLocations();
            Point p = new Point(location.getBlockX(), location.getBlockY(), location.getBlockZ());
            if (spawnerLocations.containsKey(p)) {
                e.setCancelled(true);
                DungeonMobType mobType = spawnerLocations.get(p);
                dungeon.getBestiary().spawn(mobType, e.getLocation(), random);
            }
        }
    }

    @EventHandler
    public void onAmbientSpawn(CreatureSpawnEvent e) {
        if (e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL && e.getEntity() instanceof Monster) {
            Location spawnLoc = e.getLocation();
            Dungeon dungeon = DungeonCache.getDungeonAt(spawnLoc);
            if (dungeon != null) {
                IBestiary bestiary = dungeon.getBestiary();
                if (random.nextDouble() < bestiary.getAmbientSpawnChance()) {
                    if (random.nextDouble() < bestiary.getOverrideSpawnChance()) {
                        bestiary.ambientSpawn(spawnLoc, random);
                    }
                } else {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        Material material = e.getBlock().getType();
        if (breakables.contains(material)) {
            Dungeon dungeon = DungeonCache.getDungeonAt(e.getBlock().getLocation());
            if (dungeon != null) e.setCancelled(true);
        }
    }

    private static Set<Material> breakables = new ImmutableSet.Builder<Material>()
            .add(Material.FLOWER_POT)
            .add(Material.BARREL)
            .add(Material.SKELETON_SKULL).build();

    @EventHandler
    public void onBreakable(PlayerInteractEvent e) {
        if ((e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
            Material type = e.getClickedBlock().getType();
            if (type == Material.BARREL || type == Material.FLOWER_POT) {
                Block b = e.getClickedBlock();
                Dungeon dungeon = DungeonCache.getDungeonAt(b.getLocation());
                if (dungeon != null) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onChest(PlayerInteractEvent e) {
        if ((e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType() == Material.CHEST)) {
            Block chest = e.getClickedBlock();
            Location location = chest.getLocation();
            Dungeon dungeon = DungeonCache.getDungeonAt(chest.getLocation());
            if (dungeon != null) {
                Map<Point, List<LootTag>> chestLocations = dungeon.getChestLocations();
                Point p = new Point(location.getBlockX(), location.getBlockY(), location.getBlockZ());
                if (chestLocations.containsKey(p)) {
                    List<LootTag> lootTags = chestLocations.get(p);
                    chestLocations.remove(p);
                    fillChest(chest, dungeon.getLootPool(), dungeon.getTags(), lootTags, 0);
                }
            }
        }
    }

    private void fillChest(Block block, ILootPool pool, List<DungeonTag> dungeonTags, List<LootTag> lootTags, int luck) {
        Chest chest = (Chest) block.getState();
        ILoot loot = pool.selectChest(random, dungeonTags, lootTags, luck);
        loot.fill(chest.getSnapshotInventory(), random, dungeonTags, lootTags, luck);
        chest.update();
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerBreak(BlockBreakEvent e) {
        onBreak(e.getBlock());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockExplode(BlockExplodeEvent e) {
        for (Block b : e.blockList()) {
            onBreak(b);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityExplode(EntityExplodeEvent e) {
        for (Block b : e.blockList()) {
            onBreak(b);
        }
    }

    private void onBreak(Block block) {
        Material material = block.getType();
        if (material == Material.CHEST || material == Material.SPAWNER) {
            Location location = block.getLocation();
            Dungeon dungeon = DungeonCache.getDungeonAt(location);
            if (dungeon != null) {
                Point p = new Point(location.getBlockX(), location.getBlockY(), location.getBlockZ());
                //check chest
                Map<Point, List<LootTag>> chestLocations = dungeon.getChestLocations();
                if (chestLocations.containsKey(p)) {
                    List<LootTag> lootTags = chestLocations.get(p);
                    chestLocations.remove(p);
                    fillChest(block, dungeon.getLootPool(), dungeon.getTags(), lootTags, 0);
                }

                //check spawner
                Map<Point, DungeonMobType> spawnerLocations = dungeon.getSpawnerLocations();
                if (spawnerLocations.containsKey(p)) {
                    spawnerLocations.remove(p);
                }
            }
        }
    }


}
