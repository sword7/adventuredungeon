package me.sword7.adventuredungeon.dungeon.storage;

import me.sword7.adventuredungeon.dungeon.Dungeon;
import me.sword7.adventuredungeon.dungeon.DungeonType;
import me.sword7.adventuredungeon.dungeons.crypt.Crypt;
import me.sword7.adventuredungeon.lootpool.LootTag;
import me.sword7.adventuredungeon.mob.DungeonMobType;
import me.sword7.adventuredungeon.mob.Stalfos;
import me.sword7.adventuredungeon.util.BlockColor;
import me.sword7.adventuredungeon.util.StorageUtil;
import me.sword7.adventuredungeon.util.Util;
import me.sword7.adventuredungeon.util.math.Cardinal;
import me.sword7.adventuredungeon.util.math.IBound;
import me.sword7.adventuredungeon.util.math.Point;
import me.sword7.adventuredungeon.util.math.Point2D;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.*;

public class DungeonFlatFile {

    private static final String FOLDER = "plugins/AdventureDungeon/data/dungeons";
    private static final String HEAD = "dungeonData";
    private static final String TYPE_HEAD = "TypeData";

    public static void delete(UUID dungeonId) {
        new File(FOLDER, dungeonId.toString()).delete();
    }

    public static void store(Dungeon dungeon) {
        File file = getFile(dungeon.getId());
        if (file.exists()) file.delete();
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        storeTypeData(config, dungeon);
        storeBaseData(config, dungeon);

        Util.save(config, file);
    }

    private static void storeBaseData(FileConfiguration config, Dungeon dungeon) {
        config.set(HEAD + ".id", dungeon.getId().toString());
        config.set(HEAD + ".name", dungeon.getName());
        config.set(HEAD + ".type", dungeon.getType().toString());
        config.set(HEAD + ".worldId", dungeon.getWorldId().toString());
        config.set(HEAD + ".entrance", dungeon.getEntrance().getAsString());
        config.set(HEAD + ".direction", dungeon.getDirection().toString());
        config.set(HEAD + ".center", dungeon.getCenter().getAsString());
        config.set(HEAD + ".radius", dungeon.getRadius());

        List<String> gridStrings = new ArrayList<>();
        for (Point2D p : dungeon.getGrids()) {
            gridStrings.add(p.getAsString());
        }
        config.set(HEAD + ".grids", gridStrings);

        String key = HEAD + ".chestLocations";
        for (Map.Entry<Point, List<LootTag>> entry : dungeon.getChestLocations().entrySet()) {
            List<LootTag> tags = entry.getValue();
            String tagString = "";
            for (LootTag tag : tags) {
                tagString += tag.toString() + "|";
            }
            if (tagString.endsWith("|")) tagString = tagString.substring(0, tagString.length() - 1);
            config.set(key + "." + entry.getKey().getAsString(), tagString);
        }


        key = HEAD + ".spawnerLocations";
        for (Map.Entry<Point, DungeonMobType> entry : dungeon.getSpawnerLocations().entrySet()) {
            config.set(key + "." + entry.getKey().getAsString(), entry.getValue().toString());
        }

        List<String> playerIdStrings = new ArrayList<>();
        for (UUID playerId : dungeon.getVisitors()) {
            playerIdStrings.add(playerId.toString());
        }
        config.set(HEAD + ".visitors", playerIdStrings);

        List<String> boundStrings = new ArrayList<>();
        for (IBound bound : dungeon.getBounds()) {
            boundStrings.add(StorageUtil.getAsString(bound));
        }
        config.set(HEAD + ".bounds", boundStrings);
    }

    private static void storeTypeData(FileConfiguration config, Dungeon dungeon) {
        DungeonType type = dungeon.getType();
        String head = TYPE_HEAD;
        if (type == DungeonType.CRYPT) {
            Crypt crypt = (Crypt) dungeon;
            head = "crypt" + head;
            config.set(head + ".garment", crypt.getGarment().toString());
            config.set(head + ".color", crypt.getColor().toString());
        }
    }

    public static Dungeon fetch(UUID dungeonId) {
        File file = getFile(dungeonId);
        if (file.exists()) {
            FileConfiguration config = YamlConfiguration.loadConfiguration(file);
            if (config.contains(HEAD)) {
                try {
                    UUID id = UUID.fromString(config.getString(HEAD + ".id"));
                    String name = config.getString(HEAD + ".name");
                    DungeonType type = DungeonType.valueOf(config.getString(HEAD + ".type"));
                    UUID worldId = UUID.fromString(config.getString(HEAD + ".worldId"));
                    Point entrance = Point.fromString(config.getString(HEAD + ".entrance"));
                    Cardinal direction = Cardinal.valueOf(config.getString(HEAD + ".direction"));
                    Point center = Point.fromString(config.getString(HEAD + ".center"));
                    int radius = config.getInt(HEAD + ".radius");

                    if (id != null && name != null && worldId != null && entrance != null && direction != null && center != null) {

                        List<Point2D> grids = new ArrayList<>();
                        String key = HEAD + ".grids";
                        if (config.contains(key)) {
                            List<String> gridStrings = config.getStringList(key);
                            for (String s : gridStrings) {
                                grids.add(Point2D.fromString(s));
                            }
                            gridStrings.clear();
                        }

                        Map<Point, List<LootTag>> chestLocations = new HashMap<>();
                        key = HEAD + ".chestLocations";
                        if (config.contains(key)) {
                            for (String s : config.getConfigurationSection(key).getKeys(false)) {
                                Point p = Point.fromString(s);
                                String tagString = config.getString(key + "." + s);
                                List<LootTag> tags = tagString.equals("") ? Collections.EMPTY_LIST : new ArrayList<>();
                                for (String t : tagString.split("\\|")) {
                                    LootTag tag = LootTag.getTag(type, t);
                                    if (tag != null) tags.add(tag);
                                }
                                chestLocations.put(p, tags);
                            }
                        }

                        Map<Point, DungeonMobType> spawnerLocations = new HashMap<>();
                        key = HEAD + ".spawnerLocations";
                        if (config.contains(key)) {
                            for (String s : config.getConfigurationSection(key).getKeys(false)) {
                                Point p = Point.fromString(s);
                                DungeonMobType mobType = DungeonMobType.valueOf(config.getString(key + "." + s));
                                spawnerLocations.put(p, mobType);
                            }
                        }

                        Set<UUID> visitors = new HashSet<>();
                        key = HEAD + ".visitors";
                        if (config.contains(key)) {
                            List<String> playerIdStrings = config.getStringList(key);
                            for (String s : playerIdStrings) {
                                visitors.add(UUID.fromString(s));
                            }
                            playerIdStrings.clear();
                        }

                        List<IBound> bounds = new ArrayList<>();
                        List<String> boundStrings = config.getStringList(HEAD + ".bounds");
                        for (String s : boundStrings) {
                            bounds.add(StorageUtil.boundFrom(s));
                        }
                        boundStrings.clear();

                        String head = TYPE_HEAD;
                        if (type == DungeonType.CRYPT) {
                            head = "crypt" + head;
                            Stalfos.Garment garment = Stalfos.Garment.valueOf(config.getString(head + ".garment"));
                            BlockColor blockColor = BlockColor.valueOf(config.getString(head + ".color"));
                            if (garment != null && blockColor != null) {
                                return new Crypt(id, name, worldId, entrance, direction, center, radius,
                                        grids, chestLocations, spawnerLocations, visitors, bounds,
                                        garment, blockColor);
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    private static File getFile(UUID dungeonId) {
        return new File(FOLDER, dungeonId.toString() + ".yml");
    }


}
