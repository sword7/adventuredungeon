package me.sword7.adventuredungeon.dungeon.storage;

import me.sword7.adventuredungeon.util.Util;
import me.sword7.adventuredungeon.util.math.Point2D;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.*;

public class DungeonLoaderFlatFile {

    private File file = new File("plugins/AdventureDungeon/data", "dungeonLoader.yml");

    public void store(Map<UUID, Map<Point2D, List<UUID>>> dungeonLoadingMap) {
        file.delete();
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        for (Map.Entry<UUID, Map<Point2D, List<UUID>>> wEntry : dungeonLoadingMap.entrySet()) {
            String wordIdString = wEntry.getKey().toString();
            Map<Point2D, List<UUID>> dungeonMap = wEntry.getValue();
            for (Map.Entry<Point2D, List<UUID>> entry : dungeonMap.entrySet()) {
                String p = entry.getKey().getAsString();
                List<String> idStrings = new ArrayList<>();
                for (UUID id : entry.getValue()) {
                    idStrings.add(id.toString());
                }
                config.set(wordIdString + "." + p, idStrings);
            }
        }


        Util.save(config, file);
    }

    public Map<UUID, Map<Point2D, List<UUID>>> fetch() {
        Map<UUID, Map<Point2D, List<UUID>>> worldMap = new HashMap<>();
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        for (String worldIdString : config.getRoot().getKeys(false)) {
            try {
                UUID worldId = UUID.fromString(worldIdString);
                Map<Point2D, List<UUID>> dungeonMap = new HashMap<>();
                for (String s : config.getConfigurationSection(worldIdString).getKeys(false)) {
                    Point2D p = Point2D.fromString(s);
                    List<String> idStrings = config.getStringList(worldIdString + "." + s);
                    List<UUID> ids = new ArrayList<>();
                    for (String idString : idStrings) {
                        ids.add(UUID.fromString(idString));
                    }
                    idStrings.clear();
                    dungeonMap.put(p, ids);
                }
                worldMap.put(worldId, dungeonMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return worldMap;

    }


}
