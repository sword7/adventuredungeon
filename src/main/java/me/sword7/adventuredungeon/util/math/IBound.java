package me.sword7.adventuredungeon.util.math;

import org.bukkit.Location;

public interface IBound {

    boolean contains(int x, int y, int z);

    boolean contains(Location location);

    Point getMin();

    Point getMax();

}
