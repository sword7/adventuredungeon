package me.sword7.adventuredungeon.util.math;

import java.util.Objects;

public class Point {

    public static final Point ORIGIN = new Point(0, 0, 0);

    private int x;
    private int y;
    private int z;

    public Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public Point add(int x, int y, int z) {
        return new Point(this.x + x, this.y + y, this.z + z);
    }

    public Point add(Point p) {
        return new Point(this.x + p.getX(), this.y + p.getY(), this.z + p.getZ());
    }

    public Point average(Point p) {
        return new Point((this.x + p.getX()) / 2, (this.y + p.getY()) / 2, (this.z + p.getZ()) / 2);
    }

    public void adjustMinCorner(int x, int y, int z) {
        if (x < this.x) this.x = x;
        if (y < this.y) this.y = y;
        if (z < this.z) this.z = z;
    }

    public void adjustMaxCorner(int x, int y, int z) {
        if (x > this.x) this.x = x;
        if (y > this.y) this.y = y;
        if (z > this.z) this.z = z;
    }

    public static void order(Point min, Point max) {
        int minX = min.getX() < max.getX() ? min.getX() : max.getX();
        int minY = min.getY() < max.getY() ? min.getY() : max.getY();
        int minZ = min.getZ() < max.getZ() ? min.getZ() : max.getZ();
        int maxX = max.getX() > min.getX() ? max.getX() : min.getX();
        int maxY = max.getY() > min.getY() ? max.getY() : min.getY();
        int maxZ = max.getZ() > min.getZ() ? max.getZ() : min.getZ();
        min.x = minX;
        min.y = minY;
        min.z = minZ;
        max.x = maxX;
        max.y = maxY;
        max.z = maxZ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x && y == point.y && z == point.z;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    public Point clone() {
        return new Point(x, y, z);
    }

    public String getAsString() {
        return "[" + x + "," + y + "," + z + "]";
    }

    public static Point fromString(String string) {
        try {
            String s = string.replace("[", "").replace("]", "");
            String[] parts = s.split(",");
            int x = Integer.valueOf(parts[0]);
            int y = Integer.valueOf(parts[1]);
            int z = Integer.valueOf(parts[2]);
            return new Point(x, y, z);
        } catch (Exception e) {
            return null;
        }
    }

}
