package me.sword7.adventuredungeon.util.math;

import com.google.common.collect.ImmutableSet;
import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.structure.block.DungeonBlock;
import me.sword7.adventuredungeon.structure.block.MinecraftBlock;
import me.sword7.adventuredungeon.structure.block.ThemeBlock;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.MultipleFacing;
import org.bukkit.block.data.Rotatable;
import org.bukkit.block.data.type.Wall;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public enum Rotation {
    R_0,
    R_90,
    R_180,
    R_270;

    public static Rotation fromInt(int r) {
        if (r % 4 == 0) {
            return R_0;
        } else if ((r + 1) % 4 == 0) {
            return r > 0 ? R_270 : R_90;
        } else if (r % 2 == 0) {
            return R_180;
        } else if ((r + 1) % 2 == 0) {
            return r > 0 ? R_90 : R_270;
        } else {
            return R_0;
        }
    }

    public void rotateBlock(DungeonBlock block) {
        BlockData blockData = null;
        if (block instanceof ThemeBlock) {
            ThemeBlock themeBlock = (ThemeBlock) block;
            blockData = themeBlock.getData();
        } else if (block instanceof MinecraftBlock) {
            MinecraftBlock minecraftBlock = (MinecraftBlock) block;
            blockData = minecraftBlock.getBlockData();
        }
        if (blockData instanceof MultipleFacing) {
            MultipleFacing multipleFacing = (MultipleFacing) blockData;
            Set<BlockFace> faces = new HashSet<>();
            faces.addAll(multipleFacing.getFaces());
            Set<BlockFace> rotatedFaces = new HashSet<>();
            for (BlockFace face : faces) {
                multipleFacing.setFace(face, false);
                rotatedFaces.add(this.rotateFace(face));
            }
            for (BlockFace face : rotatedFaces) {
                multipleFacing.setFace(face, true);
            }
        }
        if (blockData instanceof Rotatable) {
            Rotatable rotatable = (Rotatable) blockData;
            rotatable.setRotation(this.rotateFace(rotatable.getRotation()));
        }
        if (blockData instanceof Directional) {
            Directional directional = (Directional) blockData;
            directional.setFacing(this.rotateFace(directional.getFacing()));
        }
        if (Version.getValue() >= 116 && blockData instanceof Wall) {
            Wall wall = (Wall) blockData;
            Map<BlockFace, Wall.Height> faceToHeight = new HashMap<>();
            for (BlockFace face : cardinalFaces) {
                faceToHeight.put(face, wall.getHeight(face));
            }
            Map<BlockFace, Wall.Height> rotatedFaceToHeight = new HashMap<>();
            for (Map.Entry<BlockFace, Wall.Height> entry : faceToHeight.entrySet()) {
                rotatedFaceToHeight.put(this.rotateFace(entry.getKey()), entry.getValue());
            }
            for (Map.Entry<BlockFace, Wall.Height> entry : rotatedFaceToHeight.entrySet()) {
                wall.setHeight(entry.getKey(), entry.getValue());
            }
        }
    }

    private static Set<BlockFace> cardinalFaces = new ImmutableSet.Builder<BlockFace>()
            .add(BlockFace.NORTH)
            .add(BlockFace.EAST)
            .add(BlockFace.SOUTH)
            .add(BlockFace.WEST)
            .build();

    private BlockFace rotateFace(BlockFace blockFace) {
        switch (this) {
            case R_0:
                return blockFace;
            case R_90:
                switch (blockFace) {
                    case EAST:
                        return BlockFace.SOUTH;
                    case SOUTH:
                        return BlockFace.WEST;
                    case WEST:
                        return BlockFace.NORTH;
                    case NORTH:
                        return BlockFace.EAST;
                    case NORTH_EAST:
                        return BlockFace.SOUTH_EAST;
                    case NORTH_WEST:
                        return BlockFace.NORTH_EAST;
                    case SOUTH_EAST:
                        return BlockFace.SOUTH_WEST;
                    case SOUTH_WEST:
                        return BlockFace.NORTH_WEST;
                    case EAST_NORTH_EAST:
                        return BlockFace.SOUTH_SOUTH_EAST;
                    case EAST_SOUTH_EAST:
                        return BlockFace.SOUTH_SOUTH_WEST;
                    case WEST_NORTH_WEST:
                        return BlockFace.NORTH_NORTH_EAST;
                    case SOUTH_SOUTH_WEST:
                        return BlockFace.WEST_NORTH_WEST;
                    case NORTH_NORTH_EAST:
                        return BlockFace.EAST_SOUTH_EAST;
                    case SOUTH_SOUTH_EAST:
                        return BlockFace.WEST_SOUTH_WEST;
                    case NORTH_NORTH_WEST:
                        return BlockFace.EAST_NORTH_EAST;
                    case WEST_SOUTH_WEST:
                        return BlockFace.NORTH_NORTH_WEST;
                    default:
                        return blockFace;
                }
            case R_180:
                switch (blockFace) {
                    case EAST:
                        return BlockFace.WEST;
                    case SOUTH:
                        return BlockFace.NORTH;
                    case WEST:
                        return BlockFace.EAST;
                    case NORTH:
                        return BlockFace.SOUTH;
                    case NORTH_EAST:
                        return BlockFace.SOUTH_WEST;
                    case NORTH_WEST:
                        return BlockFace.SOUTH_EAST;
                    case SOUTH_EAST:
                        return BlockFace.NORTH_WEST;
                    case SOUTH_WEST:
                        return BlockFace.NORTH_EAST;
                    case EAST_NORTH_EAST:
                        return BlockFace.WEST_SOUTH_WEST;
                    case EAST_SOUTH_EAST:
                        return BlockFace.WEST_NORTH_WEST;
                    case WEST_NORTH_WEST:
                        return BlockFace.EAST_SOUTH_EAST;
                    case SOUTH_SOUTH_WEST:
                        return BlockFace.NORTH_NORTH_EAST;
                    case NORTH_NORTH_EAST:
                        return BlockFace.SOUTH_SOUTH_WEST;
                    case SOUTH_SOUTH_EAST:
                        return BlockFace.NORTH_NORTH_WEST;
                    case NORTH_NORTH_WEST:
                        return BlockFace.SOUTH_SOUTH_EAST;
                    case WEST_SOUTH_WEST:
                        return BlockFace.EAST_NORTH_EAST;
                    default:
                        return blockFace;
                }
            case R_270:
                switch (blockFace) {
                    case EAST:
                        return BlockFace.NORTH;
                    case SOUTH:
                        return BlockFace.EAST;
                    case WEST:
                        return BlockFace.SOUTH;
                    case NORTH:
                        return BlockFace.WEST;
                    case NORTH_EAST:
                        return BlockFace.NORTH_WEST;
                    case NORTH_WEST:
                        return BlockFace.SOUTH_WEST;
                    case SOUTH_EAST:
                        return BlockFace.NORTH_EAST;
                    case SOUTH_WEST:
                        return BlockFace.SOUTH_EAST;
                    case EAST_NORTH_EAST:
                        return BlockFace.NORTH_NORTH_WEST;
                    case EAST_SOUTH_EAST:
                        return BlockFace.NORTH_NORTH_EAST;
                    case WEST_NORTH_WEST:
                        return BlockFace.SOUTH_SOUTH_WEST;
                    case SOUTH_SOUTH_WEST:
                        return BlockFace.EAST_SOUTH_EAST;
                    case NORTH_NORTH_EAST:
                        return BlockFace.WEST_NORTH_WEST;
                    case SOUTH_SOUTH_EAST:
                        return BlockFace.EAST_NORTH_EAST;
                    case NORTH_NORTH_WEST:
                        return BlockFace.WEST_SOUTH_WEST;
                    case WEST_SOUTH_WEST:
                        return BlockFace.SOUTH_SOUTH_EAST;
                    default:
                        return blockFace;
                }
            default:
                return null;
        }
    }

    public Point rotatePoint(Point p, int width, int height) {
        switch (this) {
            case R_90:
                return new Point(height - 1 - p.getZ(), p.getY(), p.getX());
            case R_180:
                return new Point(width - 1 - p.getX(), p.getY(), height - 1 - p.getZ());
            case R_270:
                return new Point(p.getZ(), p.getY(), width - 1 - p.getX());
            default:
                return new Point(p.getX(), p.getY(), p.getZ());
        }
    }

    public int rotateX(int x, int z, int width, int height) {
        switch (this) {
            case R_90:
                return height - 1 - z;
            case R_180:
                return width - 1 - x;
            case R_270:
                return z;
            default:
                return x;
        }
    }

    public int rotateZ(int x, int z, int width, int height) {
        switch (this) {
            case R_90:
                return x;
            case R_180:
                return height - 1 - z;
            case R_270:
                return width - 1 - x;
            default:
                return z;
        }
    }

    public Point rotateDimensions(Point p) {
        if (this == R_90 || this == R_270) {
            return new Point(p.getZ(), p.getY(), p.getX());
        } else {
            return new Point(p.getX(), p.getY(), p.getZ());
        }
    }

    public Cardinal rotateCardinal(Cardinal c) {
        switch (this) {
            case R_0:
                return c;
            case R_90:
                return c.next();
            case R_180:
                return c.next().next();
            case R_270:
                return c.next().next().next();
            default:
                return null;
        }
    }

}
