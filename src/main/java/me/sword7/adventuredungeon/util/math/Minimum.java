package me.sword7.adventuredungeon.util.math;

public class Minimum {

    private int min;

    public Minimum(){
        this.min = Integer.MAX_VALUE;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMin() {
        return min;
    }
}
