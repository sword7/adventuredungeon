package me.sword7.adventuredungeon.util.math;

import org.apache.commons.lang.builder.HashCodeBuilder;

public class Point2D {

    private int x;
    private int z;

    public Point2D(int x, int z) {
        this.x = x;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    public void add(int x, int z) {
        this.x += x;
        this.z += z;
    }

    public Point2D clone() {
        return new Point2D(x, z);
    }


    public String getAsString() {
        return "[" + x + "," + z + "]";
    }

    public static Point2D fromString(String string) {
        try {
            String s = string.replace("[", "").replace("]", "");
            String[] parts = s.split(",");
            int x = Integer.valueOf(parts[0]);
            int z = Integer.valueOf(parts[1]);
            return new Point2D(x, z);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Point2D) {
            Point2D point2D = (Point2D) o;
            return (this.x == point2D.x && this.z == point2D.z);
        } else {
            return super.equals(o);
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(x).append(z).toHashCode();
    }

}
