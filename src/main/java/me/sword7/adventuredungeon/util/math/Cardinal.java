package me.sword7.adventuredungeon.util.math;

import com.google.common.collect.ImmutableSet;
import org.bukkit.block.BlockFace;

import java.util.Collections;
import java.util.Set;

public enum Cardinal {
    N(new Point(0, 0, -1)),
    S(new Point(0, 0, 1)),
    E(new Point(1, 0, 0)),
    W(new Point(-1, 0, 0));

    private static Cardinal[] cardinals2D = new Cardinal[]{N, S, E, W};

    private Point direction;

    Cardinal(Point direction) {
        this.direction = direction;
    }

    public Cardinal getOpposite() {
        switch (this) {
            case N:
                return S;
            case S:
                return N;
            case E:
                return W;
            case W:
                return E;
            default:
                return null;
        }
    }

    public BlockFace asFace() {
        switch (this) {
            case N:
                return BlockFace.NORTH;
            case S:
                return BlockFace.SOUTH;
            case E:
                return BlockFace.EAST;
            case W:
                return BlockFace.WEST;
            default:
                return null;
        }
    }

    private static Cardinal[] cardinals = new Cardinal[]{N, S, E, W};

    public static Cardinal getRandom() {
        int index = (int) (Math.random() * cardinals.length);
        return cardinals[index];
    }

    public static Cardinal getRandom2D() {
        int index = (int) (Math.random() * cardinals2D.length);
        return cardinals2D[index];
    }

    public static Cardinal[] getCardinals2D() {
        return cardinals2D;
    }

    public Point getDirection() {
        return direction;
    }

    public Cardinal next() {
        switch (this) {
            case N:
                return E;
            case E:
                return S;
            case S:
                return W;
            case W:
                return N;
            default:
                return null;
        }
    }

    public static Set<Cardinal> buildSet(Cardinal... cardinals) {
        if (cardinals.length == 0) return Collections.EMPTY_SET;
        ImmutableSet.Builder<Cardinal> builder = new ImmutableSet.Builder<>();
        for (Cardinal cardinal : cardinals) {
            builder.add(cardinal);
        }
        return builder.build();
    }

}
