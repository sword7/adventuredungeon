package me.sword7.adventuredungeon.util.math;

public class Orientation {

    private Point shift;
    private Rotation rotation;

    public Orientation(Point shift, Rotation rotation){
        this.shift = shift;
        this.rotation = rotation;
    }

    public Rotation getRotation() {
        return rotation;
    }

    public Point getShift() {
        return shift;
    }

}
