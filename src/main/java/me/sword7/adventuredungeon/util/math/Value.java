package me.sword7.adventuredungeon.util.math;

public class Value<T> {

    private T t;

    public Value(T t) {
        this.t = t;
    }

    public T getValue() {
        return t;
    }

    public void setValue(T t) {
        this.t = t;
    }
}
