package me.sword7.adventuredungeon.util.math;

import org.bukkit.Location;

import javax.annotation.Nonnull;

public class BoundRect implements IBound {

    private final Point min;
    private final Point max;

    public BoundRect(Point min, Point max) {
        Point.order(min, max);
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean contains(int x, int y, int z) {
        return min.getX() <= x && x <= max.getX() &&
                min.getY() <= y && y <= max.getY() &&
                min.getZ() <= z && z <= max.getZ();
    }

    @Override
    @Nonnull
    public boolean contains(Location location) {
        int blockX = location.getBlockX();
        int blockY = location.getBlockY();
        int blockZ = location.getBlockZ();
        return contains(blockX, blockY, blockZ);
    }

    @Override
    public Point getMin() {
        return min;
    }

    @Override
    public Point getMax() {
        return max;
    }


}
