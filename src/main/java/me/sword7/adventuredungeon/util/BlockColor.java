package me.sword7.adventuredungeon.util;

import org.bukkit.DyeColor;
import org.bukkit.Material;

public enum BlockColor {

    PURPLE,
    BLUE,
    GREEN,
    RED,

    ;

    public Material getBanner() {
        switch (this) {
            case RED:
                return Material.RED_WALL_BANNER;
            case GREEN:
                return Material.GREEN_WALL_BANNER;
            case BLUE:
                return Material.BLUE_WALL_BANNER;
            case PURPLE:
                return Material.PURPLE_WALL_BANNER;
            default:
                return Material.WHITE_WALL_BANNER;
        }
    }

    public Material getCarpet() {
        switch (this) {
            case RED:
                return Material.RED_CARPET;
            case GREEN:
                return Material.GREEN_CARPET;
            case BLUE:
                return Material.BLUE_CARPET;
            case PURPLE:
                return Material.PURPLE_CARPET;
            default:
                return Material.WHITE_CARPET;
        }
    }

    public DyeColor getDyeColor() {
        switch (this) {
            case RED:
                return DyeColor.RED;
            case GREEN:
                return DyeColor.GREEN;
            case BLUE:
                return DyeColor.BLUE;
            case PURPLE:
                return DyeColor.PURPLE;
            default:
                return DyeColor.WHITE;
        }
    }

}
