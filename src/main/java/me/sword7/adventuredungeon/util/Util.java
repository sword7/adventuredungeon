package me.sword7.adventuredungeon.util;

import me.sword7.adventuredungeon.structure.VoxelData;
import me.sword7.adventuredungeon.structure.VoxelState;
import me.sword7.adventuredungeon.util.math.IBound;
import me.sword7.adventuredungeon.util.math.Point;
import me.sword7.adventuredungeon.util.math.Point2D;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Util {

    private static Random random = new Random();

    public static int randomInt(int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }

    public static double randomDouble(double min, double max) {
        return random.nextDouble() * (max - min) + min;
    }

    public static void applyDurability(ItemStack itemStack, double minDurability, double maxDurability) {
        ItemMeta meta = itemStack.getItemMeta();
        if (meta instanceof Damageable) {
            Damageable damageable = (Damageable) meta;
            int itemDurability = itemStack.getType().getMaxDurability();
            double durability = Util.randomDouble(minDurability, maxDurability);
            double toTake = 1 - durability;
            int damage = (int) Math.floor(itemDurability * toTake);
            damageable.setDamage(damage);
            itemStack.setItemMeta(meta);
        }
    }

    public static boolean isEmpty(ItemStack itemStack) {
        return itemStack == null || itemStack.getType() == Material.AIR;
    }

    public static Point2D gridPointFrom(int gridLength, Location location) {
        return gridPointFrom(gridLength, location.getBlockX(), location.getBlockZ());
    }

    public static Point2D gridPointFrom(int gridLength, int blockX, int blockZ) {

        int offsetX = blockX < 0 ? -1 : 0;
        int x = (blockX / gridLength) + offsetX;

        int offsetZ = blockZ < 0 ? -1 : 0;
        int z = (blockZ / gridLength) + offsetZ;

        return new Point2D(x, z);
    }

    public static void save(FileConfiguration config, File file) {
        try {
            config.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static List<Point2D> getBoundGrids(IBound bound, int gridLength) {
        List<Point2D> grids = new ArrayList<>();
        Point min = bound.getMin();
        Point max = bound.getMax();
        Point2D c1 = Util.gridPointFrom(gridLength, min.getX(), min.getZ());
        grids.add(c1);
        Point2D c2 = Util.gridPointFrom(gridLength, min.getX(), max.getZ());
        if (!grids.contains(c2)) grids.add(c2);
        Point2D c3 = Util.gridPointFrom(gridLength, max.getX(), max.getZ());
        if (!grids.contains(c3)) grids.add(c3);
        Point2D c4 = Util.gridPointFrom(gridLength, max.getX(), min.getZ());
        if (!grids.contains(c4)) grids.add(c4);
        return grids;
    }

    public static void placeBlocksSync(List<Block> toAir, List<VoxelData> toData, List<VoxelState> toState) {
        for (Block block : toAir) {
            block.setType(Material.AIR);
        }
        for (VoxelData voxelData : toData) {
            voxelData.block.setBlockData(voxelData.data);
        }
        for (VoxelState voxelState : toState) {
            voxelState.stateUpdater.accept(voxelState.block);
        }
    }


}
