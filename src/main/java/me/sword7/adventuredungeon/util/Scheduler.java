package me.sword7.adventuredungeon.util;

import me.sword7.adventuredungeon.AdventureDungeon;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class Scheduler {

    private static BukkitScheduler scheduler = Bukkit.getScheduler();
    private static Plugin plugin = AdventureDungeon.getPlugin();

    public static void run(Runnable r) {
        scheduler.runTask(plugin, r);
    }

    public static void runAsync(Runnable r) {
        scheduler.runTaskAsynchronously(plugin, r);
    }

    public static void runLater(Runnable r, int ticks) {
        scheduler.runTaskLater(plugin, r, ticks);
    }

    public static void runLaterAsync(Runnable r, int ticks) {
        scheduler.runTaskLaterAsynchronously(plugin, r, ticks);
    }

}
