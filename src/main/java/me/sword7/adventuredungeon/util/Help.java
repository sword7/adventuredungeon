package me.sword7.adventuredungeon.util;

import me.sword7.adventuredungeon.config.Language;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Help {

    public static void sendTo(CommandSender sender) {
        sender.sendMessage("");
        sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "__Adventure Dungeon_______");
        sender.sendMessage(ChatColor.DARK_GRAY.toString() + ChatColor.BOLD + "------- " + Language.LABEL_COMMANDS + " -------");
        sendCommand(sender, "&7/dgen &8{NONE/LOW/MED/HIGH}&7: &f" + Language.HELP_DGEN);

    }

    private static void sendCommand(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }


}
