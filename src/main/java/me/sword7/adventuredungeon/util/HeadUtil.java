package me.sword7.adventuredungeon.util;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.block.Skull;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.UUID;

public class HeadUtil {


    public static void assignTexture(ItemStack head, String url, String profileName, UUID profileID) {
        SkullMeta skullMeta = (SkullMeta) head.getItemMeta();
        GameProfile profile = new GameProfile(profileID, profileName);
        profile.getProperties().put("textures", new Property("textures", url));
        try {
            Field profileField = skullMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(skullMeta, profile);
        } catch (IllegalArgumentException | SecurityException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        head.setItemMeta(skullMeta);
    }

    public static UUID getPlayerID(SkullMeta skullMeta) {
        return skullMeta.getOwningPlayer().getUniqueId();
    }

    public static UUID getPlayerID(Skull skull) {
        return skull.getOwningPlayer().getUniqueId();
    }


}
