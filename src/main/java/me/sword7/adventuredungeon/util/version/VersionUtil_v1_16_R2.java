package me.sword7.adventuredungeon.util.version;

import org.bukkit.Axis;
import org.bukkit.block.data.BlockData;
import org.bukkit.craftbukkit.v1_16_R2.block.impl.CraftRotatable;

public class VersionUtil_v1_16_R2 implements IVersionUtil {

    @Override
    public Axis getWoodAxis(BlockData blockData) {
        CraftRotatable craftRotatable = (CraftRotatable) blockData;
        return craftRotatable.getAxis();
    }

    @Override
    public void setWoodAxis(BlockData blockData, Axis axis) {
        CraftRotatable craftRotatable = (CraftRotatable) blockData;
        craftRotatable.setAxis(axis);
    }

}
