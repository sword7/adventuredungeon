package me.sword7.adventuredungeon.util.version;

import org.bukkit.Axis;
import org.bukkit.block.data.BlockData;

public interface IVersionUtil {

    Axis getWoodAxis(BlockData blockData);

    void setWoodAxis(BlockData blockData, Axis axis);

}
