package me.sword7.adventuredungeon.util.version;


import me.sword7.adventuredungeon.config.Version;
import org.bukkit.Axis;
import org.bukkit.block.data.BlockData;

public class VersionUtil {

    private static IVersionUtil versionUtil = selectVersionUtil();

    private static IVersionUtil selectVersionUtil() {
        switch (Version.getCurrent()) {
            case v1_17_R1:
                return new VersionUtil_v1_17_R1();
            case v1_16_R3:
                return new VersionUtil_v1_16_R3();
            case v1_16_R2:
                return new VersionUtil_v1_16_R2();
            case v1_16_R1:
                return new VersionUtil_v1_16_R1();
            case v1_15_R1:
                return new VersionUtil_v1_15_R1();
            case v1_14_R1:
                return new VersionUtil_v1_14_R1();
            default:
                return new VersionUtil_Unknown();
        }
    }

    public static Axis getWoodAxis(BlockData blockData) {
        return versionUtil.getWoodAxis(blockData);
    }

    public static void setWoodAxis(BlockData blockData, Axis axis) {
        versionUtil.setWoodAxis(blockData, axis);
    }


}
