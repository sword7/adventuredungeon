package me.sword7.adventuredungeon.util;

import me.sword7.adventuredungeon.util.math.BoundRect;
import me.sword7.adventuredungeon.util.math.IBound;
import me.sword7.adventuredungeon.util.math.Point;

public class StorageUtil {

    public static String getAsString(IBound bound) {
        if (bound instanceof BoundRect) {
            BoundRect r = (BoundRect) bound;
            return "R:" + r.getMin().getAsString() + "=>" + r.getMax().getAsString();
        } else {
            return null;
        }
    }

    public static IBound boundFrom(String sting) {
        try {
            String[] parts = sting.split(":");
            if (parts[0].equals("R")) {
                String[] points = parts[1].split("=>");
                Point min = Point.fromString(points[0]);
                Point max = Point.fromString(points[1]);
                return new BoundRect(min, max);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }


}
