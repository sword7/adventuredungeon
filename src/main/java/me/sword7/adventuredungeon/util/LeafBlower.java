package me.sword7.adventuredungeon.util;

import com.google.common.collect.ImmutableList;
import me.sword7.adventuredungeon.util.math.Minimum;
import me.sword7.adventuredungeon.util.math.Point;
import me.sword7.adventuredungeon.util.math.Value;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LeafBlower {

    private static final int LEAVES_CLEAR_RADIUS = 6;
    private static final int FIND_LOG_DEPTH = 2;

    public static void clearTrees(World world, Point min, Point max) {

        Point logsMin = min.average(max);
        Point logsMax = min.average(max);

        boolean woodCleared = false;
        for (int x = min.getX(); x <= max.getX(); x++) {
            for (int y = min.getY(); y <= max.getY(); y++) {
                for (int z = min.getZ(); z <= max.getZ(); z++) {
                    Block block = world.getBlockAt(x, y, z);
                    if (MaterialUtil.WOOD.contains(block.getType())) {
                        clearLogs(block, logsMin, logsMax);
                        woodCleared = true;
                    }
                }
            }
        }

        if (woodCleared) {
            int rad = LEAVES_CLEAR_RADIUS;
            for (int x = logsMin.getX() - rad; x <= logsMax.getX() + rad; x++) {
                for (int y = logsMin.getY(); y <= logsMax.getY() + rad; y++) {
                    for (int z = logsMin.getZ() - rad; z <= logsMax.getZ() + rad; z++) {
                        Block block = world.getBlockAt(x, y, z);
                        if (MaterialUtil.LEAVES.contains(block.getType())) {
                            clearLeaves(block);
                        }
                    }
                }
            }

            replaceLeafTypes(world, logsMin, logsMax);
        }

    }

    private static void clearLogs(Block source, Point min, Point max) {
        Set<Block> logs = getLogsRecursive(source, new HashSet<>(), min, max);
        for (Block block : logs) {
            block.setType(Material.AIR);
            Block bellow = block.getLocation().add(0, -1, 0).getBlock();
            if (bellow.getType() == Material.DIRT) {
                bellow.setType(Material.GRASS_BLOCK);
            }
        }
    }

    private static Set<Block> getLogsRecursive(Block current, HashSet<Block> traversed, Point min, Point max) {
        if (!MaterialUtil.WOOD.contains(current.getType())) return traversed;
        if (traversed.size() > 256) return traversed;
        if (!traversed.add(current)) return traversed;
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                for (int z = -1; z <= 1; z++) {
                    if (!(x == 0 && y == 0 && z == 0)) {
                        getLogsRecursive(current.getRelative(x, y, z), traversed, min, max);
                    }
                }
            }
        }
        Location loc = current.getLocation();
        min.adjustMinCorner(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        max.adjustMaxCorner(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        traversed.add(current);
        return traversed;
    }

    private static void clearLeaves(Block block) {
        Set<Location> explored = new HashSet<>();
        explored.add(block.getLocation());
        boolean woodFound = findWoodRecursive(block, 0, explored);
        if (!woodFound) {
            block.setType(Material.AIR);
        }
    }

    private static List<BlockFace> directions = new ImmutableList.Builder<BlockFace>()
            .add(BlockFace.UP).add(BlockFace.DOWN).add(BlockFace.NORTH).add(BlockFace.SOUTH).add(BlockFace.EAST).add(BlockFace.WEST).build();

    private static boolean findWoodRecursive(Block block, int dist, Set<Location> explored) {
        if (dist > FIND_LOG_DEPTH) return false;
        for (BlockFace f : directions) {
            Block b = block.getRelative(f, 1);
            if (!explored.contains(b.getLocation())) {
                if (MaterialUtil.WOOD.contains(b.getType())) {
                    return true;
                } else if (MaterialUtil.LEAVES.contains(b.getType())) {
                    Set<Location> exploredRecursive = new HashSet<>();
                    exploredRecursive.addAll(explored);
                    explored.add(b.getLocation());
                    boolean woodFound = findWoodRecursive(b, dist + 1, explored);
                    exploredRecursive.clear();
                    if (woodFound == true) return true;
                }
            }
        }
        return false;
    }

    private static void replaceLeafTypes(World world, Point min, Point max) {
        int rad = LEAVES_CLEAR_RADIUS;
        for (int x = min.getX() - rad; x <= max.getX() + rad; x++) {
            for (int y = min.getY(); y <= max.getY() + rad; y++) {
                for (int z = min.getZ() - rad; z <= max.getZ() + rad; z++) {
                    Block block = world.getBlockAt(x, y, z);
                    if (MaterialUtil.LEAVES.contains(block.getType())) {
                        replaceLeafType(block);
                    }
                }
            }
        }
    }

    private static void replaceLeafType(Block block) {
        Set<Location> explored = new HashSet<>();
        explored.add(block.getLocation());
        Material material = block.getType();
        Value<Material> v = new Value(material);
        findLeafTypeRecursive(block, 0, v, new Minimum(), explored);
        block.setType(v.getValue());

    }

    private static void findLeafTypeRecursive(Block block, int dist, Value<Material> current, Minimum minDistance, Set<Location> explored) {
        if (dist > 4) return;
        for (BlockFace f : directions) {
            Block b = block.getRelative(f, 1);
            if (!explored.contains(b.getLocation())) {
                Material type = b.getType();
                if (MaterialUtil.WOOD.contains(type)) {
                    if (dist < minDistance.getMin()) {
                        current.setValue(MaterialUtil.WOOD_TO_LEAVES.get(type));
                        minDistance.setMin(dist);
                    }
                    return;
                } else if (MaterialUtil.LEAVES.contains(type)) {
                    Set<Location> exploredRecursive = new HashSet<>();
                    exploredRecursive.addAll(explored);
                    explored.add(b.getLocation());
                    findLeafTypeRecursive(b, dist + 1, current, minDistance, explored);
                }
            }
        }
        return;
    }


}
