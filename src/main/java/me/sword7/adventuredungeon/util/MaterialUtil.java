package me.sword7.adventuredungeon.util;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.bukkit.Material;

import java.util.Map;
import java.util.Set;

public class MaterialUtil {


    public static Set<Material> LEAVES = new ImmutableSet.Builder<Material>()
            .add(Material.OAK_LEAVES)
            .add(Material.SPRUCE_LEAVES)
            .add(Material.BIRCH_LEAVES)
            .add(Material.JUNGLE_LEAVES)
            .add(Material.ACACIA_LEAVES)
            .add(Material.DARK_OAK_LEAVES)
            .build();

    public static Set<Material> WOOD = new ImmutableSet.Builder<Material>()
            .add(Material.OAK_LOG)
            .add(Material.OAK_WOOD)
            .add(Material.SPRUCE_LOG)
            .add(Material.SPRUCE_WOOD)
            .add(Material.BIRCH_LOG)
            .add(Material.BIRCH_WOOD)
            .add(Material.JUNGLE_LOG)
            .add(Material.JUNGLE_WOOD)
            .add(Material.ACACIA_LOG)
            .add(Material.ACACIA_WOOD)
            .add(Material.DARK_OAK_LOG)
            .add(Material.DARK_OAK_WOOD)
            .build();


    public static Set<Material> NATURAL_GROUND = new ImmutableSet.Builder<Material>()
            .add(Material.DIRT)
            .add(Material.COARSE_DIRT)
            .add(Material.GRASS_BLOCK)
            .add(Material.MYCELIUM)
            .add(Material.PODZOL)
            .add(Material.GRAVEL)
            .add(Material.SAND)
            .add(Material.RED_SAND)
            .add(Material.GRANITE)
            .add(Material.DIORITE)
            .add(Material.ANDESITE)
            .add(Material.STONE)
            .add(Material.SANDSTONE)
            .add(Material.RED_SANDSTONE)
            .add(Material.CLAY)
            .add(Material.IRON_ORE)
            .add(Material.COAL_ORE)
            .add(Material.TERRACOTTA)
            .add(Material.CACTUS)
            .add(Material.PUMPKIN)
            .build();


    public static Map<Material, Material> WOOD_TO_LEAVES = new ImmutableMap.Builder<Material, Material>()
            .put(Material.OAK_LOG, Material.OAK_LEAVES)
            .put(Material.OAK_WOOD, Material.OAK_LEAVES)
            .put(Material.SPRUCE_LOG, Material.SPRUCE_LEAVES)
            .put(Material.SPRUCE_WOOD, Material.SPRUCE_LEAVES)
            .put(Material.BIRCH_LOG, Material.BIRCH_LEAVES)
            .put(Material.BIRCH_WOOD, Material.BIRCH_LEAVES)
            .put(Material.JUNGLE_LOG, Material.JUNGLE_LEAVES)
            .put(Material.JUNGLE_WOOD, Material.JUNGLE_LEAVES)
            .put(Material.ACACIA_LOG, Material.ACACIA_LEAVES)
            .put(Material.ACACIA_WOOD, Material.ACACIA_LEAVES)
            .put(Material.DARK_OAK_LOG, Material.DARK_OAK_LEAVES)
            .put(Material.DARK_OAK_WOOD, Material.DARK_OAK_LEAVES)
            .build();


    public static Set<Material> GROWTH = new ImmutableSet.Builder<Material>()
            .add(Material.GRASS)
            .add(Material.TALL_GRASS)
            .add(Material.FERN)
            .add(Material.LARGE_FERN)
            .add(Material.SUNFLOWER)
            .add(Material.DANDELION)
            .add(Material.POPPY)
            .add(Material.BLUE_ORCHID)
            .add(Material.AZURE_BLUET)
            .add(Material.RED_TULIP)
            .add(Material.ORANGE_TULIP)
            .add(Material.PINK_TULIP)
            .add(Material.WHITE_TULIP)
            .add(Material.OXEYE_DAISY)
            .add(Material.CORNFLOWER)
            .add(Material.LILY_OF_THE_VALLEY)
            .add(Material.BROWN_MUSHROOM)
            .add(Material.RED_MUSHROOM)
            .add(Material.LILAC)
            .add(Material.ROSE_BUSH)
            .add(Material.PEONY)
            .build();


    public static Set<Material> FLOWERS = new ImmutableSet.Builder<Material>()
            .add(Material.SUNFLOWER)
            .add(Material.DANDELION)
            .add(Material.POPPY)
            .add(Material.BLUE_ORCHID)
            .add(Material.AZURE_BLUET)
            .add(Material.RED_TULIP)
            .add(Material.ORANGE_TULIP)
            .add(Material.PINK_TULIP)
            .add(Material.WHITE_TULIP)
            .add(Material.OXEYE_DAISY)
            .add(Material.CORNFLOWER)
            .add(Material.LILY_OF_THE_VALLEY)
            .add(Material.LILAC)
            .add(Material.ROSE_BUSH)
            .add(Material.PEONY)
            .build();


    public static Set<Material> WATER = new ImmutableSet.Builder<Material>()
            .add(Material.WATER)
            .add(Material.ICE)
            .add(Material.BLUE_ICE)
            .add(Material.FROSTED_ICE)
            .add(Material.PACKED_ICE)
            .build();


}
