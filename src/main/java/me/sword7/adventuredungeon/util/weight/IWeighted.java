package me.sword7.adventuredungeon.util.weight;


import java.util.Random;

public interface IWeighted<T> {

    int getWeight();

    T get(Random rand);

}