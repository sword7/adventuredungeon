package me.sword7.adventuredungeon.util;

import com.google.common.collect.ImmutableList;
import me.sword7.adventuredungeon.AdventureDungeon;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AutoCompleteListener implements Listener {

    public AutoCompleteListener() {
        Plugin plugin = AdventureDungeon.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onComplete(TabCompleteEvent e) {
        if (e.getSender() instanceof Player) {
            String buffer = e.getBuffer();
            if (buffer.startsWith("/adventuredungeon ")) {
                int args = numberOfFullArgs(buffer);
                if (args == 0) {
                    e.setCompletions(getRefinedCompletions("/adventuredungeon", buffer, PLUGIN_COMPLETIONS));
                } else {
                    e.setCompletions(Collections.EMPTY_LIST);
                }
            } else if (buffer.startsWith("/dgen ")) {
                int args = numberOfFullArgs(buffer);
                if (args == 0) {
                    e.setCompletions(getRefinedCompletions("/dgen", buffer, RATE_COMPLETIONS));
                } else {
                    e.setCompletions(Collections.EMPTY_LIST);
                }
            }
        }
    }

    private List<String> getRefinedCompletions(String root, String buffer, List<String> completions) {
        if (buffer.equalsIgnoreCase(root + " ")) {
            return completions;
        } else {
            List<String> refinedCompletions = new ArrayList<>();
            String bufferFromRoot = buffer.split(root + " ")[1];
            for (String completion : completions) {
                if (bufferFromRoot.length() < completion.length()) {
                    if (completion.substring(0, bufferFromRoot.length()).equalsIgnoreCase(bufferFromRoot)) {
                        refinedCompletions.add(completion);
                    }
                }
            }
            return refinedCompletions;
        }
    }

    private int numberOfFullArgs(String buffer) {
        int lastNotCompletedPenalty = endsInSpace(buffer) ? 0 : -1;
        return buffer.split(" ").length - 1 + lastNotCompletedPenalty;
    }

    private boolean endsInSpace(String buffer) {
        return ' ' == buffer.charAt(buffer.length() - 1);
    }

    private String getArg(String buffer, int arg) {
        return buffer.split(" ")[arg + 1];
    }

    private static final List<String> RATE_COMPLETIONS = ImmutableList.<String>builder()
            .add("NONE")
            .add("LOW")
            .add("MED")
            .add("HIGH")
            .build();

    private static final List<String> PLUGIN_COMPLETIONS = ImmutableList.<String>builder()
            .add("help")
            .add("info")
            .build();

}
