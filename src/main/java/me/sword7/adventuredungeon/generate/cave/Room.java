package me.sword7.adventuredungeon.generate.cave;

import java.util.ArrayList;
import java.util.List;

public class Room implements Comparable<Room> {

    public List<Coord> tiles;
    public List<Coord> edgeTiles;
    public List<Room> connectedRooms;
    public int roomSize;
    public boolean isAccessibleFromMainRoom;
    public boolean isMainRoom;

    public Room() {

    }

    public Room(List<Coord> roomTiles, int[][][] map) {
        tiles = roomTiles;
        roomSize = tiles.size();
        connectedRooms = new ArrayList<>();
        edgeTiles = new ArrayList<>();
        for (Coord tile : tiles) {
            for (int x = tile.x - 1; x <= tile.x + 1; x++) {
                for (int y = tile.y - 1; y <= tile.y + 1; y++) {
                    for (int z = tile.z - 1; z <= tile.z + 1; z++) {
                        if (x == tile.x || y == tile.y || z == tile.z) {
                            if (map[x][y][z] == 1) {
                                edgeTiles.add(tile);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void connectRooms(Room roomA, Room roomB) {
        if (roomA.isAccessibleFromMainRoom) {
            roomB.setAccessibleFromMainRoom();
        } else if (roomB.isAccessibleFromMainRoom) {
            roomA.setAccessibleFromMainRoom();
        }
        roomA.connectedRooms.add(roomB);
        roomB.connectedRooms.add(roomA);
    }

    public boolean isConnected(Room otherRoom) {
        return connectedRooms.contains(otherRoom);
    }

    public void setAccessibleFromMainRoom() {
        if (!isAccessibleFromMainRoom) {
            isAccessibleFromMainRoom = true;
            for (Room connectedRoom : connectedRooms) {
                connectedRoom.setAccessibleFromMainRoom();
                ;
            }
        }
    }

    @Override
    public int compareTo(Room otherRoom) {
        if (roomSize > otherRoom.roomSize) {
            return 1;
        } else if (roomSize < otherRoom.roomSize) {
            return -1;
        } else {
            return 0;
        }
    }

}
