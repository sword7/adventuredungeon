package me.sword7.adventuredungeon.generate.cave;

public class Coord {

    public int x;
    public int y;
    public int z;

    public Coord() {

    }

    public Coord(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

}
