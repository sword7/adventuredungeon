package me.sword7.adventuredungeon.generate.cave;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.*;

public class CaveGenerator {

    private Location location;
    private int width;
    private int height;
    private int depth;

    private Random random = new Random();
    private double randomFillPercent = 0.49;
    private double randomFillEdgePercent = 0.98;
    private int[][][] map;

    private List<Hall> halls = new ArrayList<>();

    public CaveGenerator(Location location, int width, int height, int depth) {
        this.location = location;
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public void generate() {
        map = new int[width][height][depth];
        randomFill();
        for (int i = 0; i < 5; i++) {
            smoothMap();
        }
        processMap();
        draw();
    }

    private void randomFill() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1 || z == 0 || z == depth - 1) {
                        map[x][y][z] = random.nextDouble() < randomFillEdgePercent ? 1 : 0;
                    } else {
                        map[x][y][z] = random.nextDouble() < randomFillPercent ? 1 : 0;
                    }
                }
            }
        }
    }

    private void smoothMap() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    int neighborWallTiles = getSurroundingWallCount(x, y, z);
                    if (neighborWallTiles > 15) {
                        map[x][y][z] = 1;
                    } else if (neighborWallTiles < 11) {
                        map[x][y][z] = 0;
                    }
                }
            }
        }
    }

    private int getSurroundingWallCount(int gridX, int gridY, int gridZ) {
        int wallCount = 0;
        for (int neighborX = gridX - 1; neighborX <= gridX + 1; neighborX++) {
            for (int neighborY = gridY - 1; neighborY <= gridY + 1; neighborY++) {
                for (int neighborZ = gridZ - 1; neighborZ <= gridZ + 1; neighborZ++) {
                    if (isInMapRange(neighborX, neighborY, neighborZ)) {
                        if (neighborX != gridX || neighborY != gridY || neighborZ != gridZ) {
                            wallCount += map[neighborX][neighborY][neighborZ];
                        }
                    } else {
                        wallCount++;
                    }
                }
            }
        }
        return wallCount;
    }

    private List<List<Coord>> getRegions(int tileType) {
        List<List<Coord>> regions = new ArrayList<>();
        int[][][] mapFlags = new int[width][height][depth];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    if (mapFlags[x][y][z] == 0 && map[x][y][z] == tileType) {
                        List<Coord> newRegion = getRegionTiles(x, y, z);
                        regions.add(newRegion);
                        for (Coord tile : newRegion) {
                            mapFlags[tile.x][tile.y][tile.z] = 1;
                        }
                    }
                }
            }
        }

        return regions;
    }

    private void processMap() {
        List<List<Coord>> wallRegions = getRegions(1);
        int wallThresholdSize = 50;

        for (List<Coord> wallRegion : wallRegions) {
            if (wallRegion.size() < wallThresholdSize) {
                for (Coord tile : wallRegion) {
                    map[tile.x][tile.y][tile.z] = 0;
                }
            }
        }

        List<List<Coord>> roomRegions = getRegions(0);
        int roomThresholdSize = 50;
        List<Room> survivingRooms = new ArrayList<>();

        for (List<Coord> roomRegion : roomRegions) {
            if (roomRegion.size() < roomThresholdSize) {
                for (Coord tile : roomRegion) {
                    map[tile.x][tile.y][tile.z] = 1;
                }
            } else {
                //survivingRooms.add(new Room(roomRegion, map));
            }
        }

        /*
        Collections.sort(survivingRooms);
        survivingRooms.get(0).isMainRoom = true;
        survivingRooms.get(0).isAccessibleFromMainRoom = true;
        connectClosestRooms(survivingRooms);
        */
    }

    private void connectClosestRooms(List<Room> allRooms) {
        connectClosestRooms(allRooms, false);
    }

    private void connectClosestRooms(List<Room> allRooms, boolean forceAccessibilityFromMainRoom) {

        List<Room> roomListA = new ArrayList<>();
        List<Room> roomListB = new ArrayList<>();

        if (forceAccessibilityFromMainRoom) {
            for (Room room : allRooms) {
                if (room.isAccessibleFromMainRoom) {
                    roomListB.add(room);
                } else {
                    roomListA.add(room);
                }
            }
        } else {
            roomListA = allRooms;
            roomListB = allRooms;
        }

        int bestDistance = 0;
        Coord bestTileA = new Coord();
        Coord bestTileB = new Coord();
        Room bestRoomA = new Room();
        Room bestRoomB = new Room();
        boolean possibleConnectionFound = false;

        for (Room roomA : roomListA) {
            if (!forceAccessibilityFromMainRoom) {
                possibleConnectionFound = false;
                if (roomA.connectedRooms.size() > 0) continue;
            }
            for (Room roomB : roomListB) {
                if (roomA == roomB || roomA.isConnected(roomB)) continue;
                for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.size(); tileIndexA++) {
                    for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.size(); tileIndexB++) {
                        Coord tileA = roomA.edgeTiles.get(tileIndexA);
                        Coord tileB = roomB.edgeTiles.get(tileIndexB);
                        int distanceBetweenRooms = (int) (Math.pow(tileA.x - tileB.x, 2) + Math.pow(tileA.y - tileB.y, 2) + Math.pow(tileA.z - tileB.z, 2));

                        if (distanceBetweenRooms < bestDistance || !possibleConnectionFound) {
                            bestDistance = distanceBetweenRooms;
                            possibleConnectionFound = true;
                            bestTileA = tileA;
                            bestTileB = tileB;
                            bestRoomA = roomA;
                            bestRoomB = roomB;
                        }

                    }
                }
            }

            if (possibleConnectionFound && !forceAccessibilityFromMainRoom) {
                createPassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            }
        }

        if (possibleConnectionFound && forceAccessibilityFromMainRoom) {
            createPassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            connectClosestRooms(allRooms, true);
        }

        if (!forceAccessibilityFromMainRoom) {
            connectClosestRooms(allRooms, true);
        }

    }

    private void createPassage(Room roomA, Room roomB, Coord tileA, Coord tileB) {
        Room.connectRooms(roomA, roomB);

        List<Coord> line = getLine(tileA, tileB);
        for (Coord c : line) {
            drawCircle(c, 2);
        }
        halls.add(new Hall(tileA, tileB));
    }

    private void drawCircle(Coord c, int r) {
        for (int x = -r; x <= r; x++) {
            for (int y = -r; y <= r; y++) {
                for (int z = -r; z <= r; z++) {
                    if (x * x + z * z + y * y <= r * r) {
                        int drawX = c.x + x;
                        int drawY = c.y + y;
                        int drawZ = c.z + z;
                        if (isInMapRange(drawX, drawY, drawZ)) {
                            map[drawX][drawY][drawZ] = 0;
                        }
                    }
                }
            }
        }
    }

    private List<Coord> getLine(Coord from, Coord to) {
        List<Coord> line = new ArrayList<>();
        /*
        int x = from.x;
        int z = from.z;

        boolean inverted = false;
        int dx = to.x - from.x;
        int dz = to.z - from.z;
        int step = (int) Math.signum(dx);
        int gradientStep = (int) Math.signum(dz);

        int longest = Math.abs(dx);
        int shortest = Math.abs(dz);

        if (longest < shortest) {
            inverted = true;
            longest = Math.abs(dz);
            shortest = Math.abs(dx);
            step = (int) Math.signum(dz);
            gradientStep = (int) Math.signum(dx);
        }

        int gradientAccumulation = longest / 2;

        for (int i = 0; i < longest; i++) {
            line.add(new Coord(x, z));
            if (inverted) {
                z += step;
            } else {
                x += step;
            }
            gradientAccumulation += shortest;
            if (gradientAccumulation >= longest) {
                if (inverted) {
                    x += gradientStep;
                } else {
                    z += gradientStep;
                }
                gradientAccumulation -= longest;
            }
        }
         */
        return line;
    }

    private List<Coord> getRegionTiles(int startX, int startY, int startZ) {
        List<Coord> regionTiles = new ArrayList<>();
        int[][][] mapFlags = new int[width][height][depth];
        int tileType = map[startX][startY][startZ];
        Queue<Coord> queue = new LinkedList<>();
        queue.add(new Coord(startX, startY, startZ));
        mapFlags[startX][startY][startZ] = 1;

        while (queue.size() > 0) {
            Coord tile = queue.poll();
            regionTiles.add(tile);

            for (int x = tile.x - 1; x <= tile.x + 1; x++) {
                for (int y = tile.y - 1; y <= tile.y + 1; y++) {
                    for (int z = tile.z - 1; z <= tile.z + 1; z++) {
                        if (isInMapRange(x, y, z) && (x == tile.x || y == tile.y || z == tile.z)) {
                            if (mapFlags[x][y][z] == 0 && map[x][y][z] == tileType) {
                                mapFlags[x][y][z] = 1;
                                queue.add(new Coord(x, y, z));
                            }
                        }
                    }
                }
            }

        }

        return regionTiles;
    }

    private boolean isInMapRange(int x, int y, int z) {
        return x >= 0 && x < width && y >= 0 && y < height && z >= 0 && z < depth;
    }


    private void draw() {

        List<Block> solidBlocks = new ArrayList<>();
        List<Block> airBlocks = new ArrayList<>();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    int value = map[x][y][z];
                    if (value == 1) {
                        solidBlocks.add(location.clone().add(x, y, z).getBlock());
                    } else {
                        airBlocks.add(location.clone().add(x, y, z).getBlock());
                    }
                }
            }
        }

        new BlockPlaceTask(solidBlocks, Material.STONE);
        new BlockPlaceTask(airBlocks, Material.AIR);

    }


}
