package me.sword7.adventuredungeon.generate.cave;

import me.sword7.adventuredungeon.AdventureDungeon;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class BlockPlaceTask extends BukkitRunnable {

    private static final int BATCH_SIZE = 1024;

    private int index = 0;
    private List<Block> blockList;
    private Material toSet;

    public BlockPlaceTask(List<Block> blockList, Material toSet) {
        this.blockList = blockList;
        this.toSet = toSet;
        runTaskTimer(AdventureDungeon.getPlugin(), 0, 1);
    }

    @Override
    public void run() {
        if (index < blockList.size()) {
            int count = 0;
            while (count < BATCH_SIZE) {

                if (index < blockList.size()) {
                    blockList.get(index).setType(toSet);
                } else {
                    stop();
                    break;
                }

                index++;
                count++;
            }
        } else {
            stop();
        }
    }

    private void stop() {
        cancel();
        blockList.clear();
    }


}
