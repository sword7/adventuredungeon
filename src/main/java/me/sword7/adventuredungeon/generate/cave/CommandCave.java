package me.sword7.adventuredungeon.generate.cave;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandCave implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            CaveGenerator generator = new CaveGenerator(player.getLocation(), 50, 50, 50);
            generator.generate();
        }


        return false;
    }
}
