package me.sword7.adventuredungeon.generate;

import me.sword7.adventuredungeon.structure.IFrame;
import me.sword7.adventuredungeon.util.Util;
import me.sword7.adventuredungeon.util.math.Cardinal;
import me.sword7.adventuredungeon.util.math.Orientation;
import me.sword7.adventuredungeon.util.math.Point;
import me.sword7.adventuredungeon.util.math.Rotation;
import org.bukkit.Location;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class GridGenerator implements IGenerator {

    private Location location;
    private Point gridScale;
    private Cardinal direction;
    private int generation = 0;
    private static final int MAX_GENERATION = 8;
    private IFrame[] frames;


    public GridGenerator(Location location, IFrame[] frames, Point gridScale, Cardinal direction) {
        this.location = location;
        this.frames = frames;
        this.gridScale = gridScale;
        this.direction = direction;
    }

    private Map<Point, Set<Cardinal>> floorLayout = new HashMap<>();
    private List<Point> toCreate = new ArrayList<>();
    private List<Room> rooms = new ArrayList<>();

    @Override
    public IBlueprint generate() {

        rooms.clear();
        generation = 0;

        //first room
        initialize();
        generation++;

        //main
        for (int i = 1; i < MAX_GENERATION; i++) {
            Set<Point> toCreateNext = new HashSet<>();
            for (Point coord : toCreate) {
                IFrame frame = selectFrame(coord);
                if (frame != null) {
                    Orientation orientation = selectOrientation(coord, frame);
                    Room room = new Room(coord, frame, orientation);
                    rooms.add(room);

                    for (Map.Entry<Point, Set<Cardinal>> entry : room.getFinalLayout().entrySet()) {
                        Point c = entry.getKey();
                        Set<Cardinal> entrances = entry.getValue();
                        floorLayout.put(c, entrances);
                        for (Cardinal e : entrances) {
                            Point p = c.add(e.getDirection());
                            if (!floorLayout.containsKey(p)) {
                                toCreateNext.add(p);
                            }
                        }
                    }
                }
            }

            toCreate.clear();
            toCreate.addAll(toCreateNext);
            generation++;
        }

        //seal
        for (Point coord : toCreate) {
            IFrame frame = selectFrame(coord, true, false, (IFrame f) -> true);
            if (frame != null) {
                Orientation orientation = selectOrientation(coord, frame, true, false);
                Room room = new Room(coord, frame, orientation);
                rooms.add(room);

                for (Map.Entry<Point, Set<Cardinal>> entry : room.getFinalLayout().entrySet()) {
                    Point c = entry.getKey();
                    Set<Cardinal> entrances = entry.getValue();
                    floorLayout.put(c, entrances);
                }
            }
        }
        generation++;

        List<Room> roomList = new ArrayList<>();
        roomList.addAll(rooms);
        return new GridBlueprint(roomList);

    }

    private boolean isValidCoordinate(Point c) {
        boolean valid = true;
        if (c.getY() > 0) return false;
        if (location.getBlockY() + (c.getY() * gridScale.getY()) < 5) return false;
        if (floorLayout.containsKey(c)) {
            valid = false;
        } else if (direction == Cardinal.N) {
            if (c.getX() == 0 && c.getZ() == -1) valid = false;
        } else if (direction == Cardinal.S) {
            if (c.getX() == 0 && c.getZ() == 1) valid = false;
        } else if (direction == Cardinal.E) {
            if (c.getZ() == 0 && c.getX() == 1) valid = false;
        } else if (direction == Cardinal.W) {
            if (c.getZ() == 0 && c.getX() == -1) valid = false;
        }
        return valid;
    }

    private void initialize() {
        int doors = Util.randomInt(2, 4);
        Point coord = new Point(0, 0, 0);
        IFrame frame = selectFrame(coord, false, true, (IFrame f) -> f.getDoors() == doors);
        Orientation orientation = selectOrientation(coord, frame, false, true);

        Room room = new Room(coord, frame, orientation);
        rooms.add(room);

        for (Map.Entry<Point, Set<Cardinal>> entry : room.getFinalLayout().entrySet()) {
            Point c = entry.getKey();
            Set<Cardinal> entrances = entry.getValue();
            floorLayout.put(c, entrances);
            for (Cardinal e : entrances) {
                if (!c.equals(Point.ORIGIN) || e != direction) {
                    toCreate.add(c.add(e.getDirection()));
                }
            }
        }
    }

    private IFrame selectFrame(Point coord) {
        return selectFrame(coord, false, false, (IFrame f) -> true);
    }

    private IFrame selectFrame(Point coord, boolean sealOff, boolean doEntrance, Predicate<IFrame> framePredicate) {
        List<IFrame> validFrames = new ArrayList<>();
        for (IFrame frame : frames) {
            List<Orientation> validOrientations = getValidOrientations(coord, frame, sealOff, doEntrance, true);
            if (validOrientations.size() > 0) {
                validFrames.add(frame);
            }
        }
        List<IFrame> filteredFrames = validFrames.stream().filter(framePredicate).collect(Collectors.toList());
        if (filteredFrames.size() > 0) validFrames = filteredFrames;
        if (validFrames.size() > 0) {
            int index = (int) (Math.random() * validFrames.size());
            return validFrames.get(index);
        } else {
            return null;
        }
    }

    private List<Orientation> getValidOrientations(Point coord, IFrame frame, boolean sealOff, boolean doEntrance, boolean stopAtFirst) {
        List<Orientation> orientations = new ArrayList<>();
        Point dim = frame.getDimensions();
        for (Rotation r : Rotation.values()) {
            Point rotatedDim = (r == Rotation.R_90 || r == Rotation.R_270) ? new Point(dim.getZ(), dim.getY(), dim.getX()) : dim;
            for (int shiftX = 0; shiftX > rotatedDim.getX() * -1; shiftX--) {
                for (int shiftY = 0; shiftY > rotatedDim.getY() * -1; shiftY--) {
                    for (int shiftZ = 0; shiftZ > rotatedDim.getZ() * -1; shiftZ--) {
                        Map<Point, Set<Cardinal>> shiftedShape = new HashMap<>();
                        for (Map.Entry<Point, Set<Cardinal>> entry : frame.getGridCoordToEntrances().entrySet()) {
                            Point originalPoint = entry.getKey();
                            Set<Cardinal> originalCardinals = entry.getValue();
                            Point shiftedPoint = r.rotatePoint(originalPoint, dim.getX(), dim.getZ()).add(shiftX, shiftY, shiftZ);
                            Set<Cardinal> shiftedCardinals = new HashSet<>();
                            for (Cardinal c : originalCardinals) {
                                shiftedCardinals.add(r.rotateCardinal(c));
                            }
                            shiftedShape.put(shiftedPoint, shiftedCardinals);
                        }
                        if (isValidShape(coord, shiftedShape, sealOff) && shiftedShape.containsKey(Point.ORIGIN)) { //target point has to be filled (for orientations of L shape)
                            if (!doEntrance || shiftedShape.get(Point.ORIGIN).contains(direction)) {
                                orientations.add(new Orientation(new Point(shiftX, shiftY, shiftZ), r));
                                if (stopAtFirst) return orientations;
                            }
                        }
                    }
                }
            }
        }
        return orientations;
    }

    private Orientation selectOrientation(Point coord, IFrame frame) {
        List<Orientation> validOrientations = getValidOrientations(coord, frame, false, false, false);
        int index = (int) (Math.random() * validOrientations.size());
        return validOrientations.get(index);
    }

    private Orientation selectOrientation(Point coord, IFrame frame, boolean sealOff, boolean doEntrance) {
        List<Orientation> validOrientations = getValidOrientations(coord, frame, sealOff, doEntrance, false);
        int index = (int) (Math.random() * validOrientations.size());
        return validOrientations.get(index);
    }

    private boolean isValidShape(Point coord, Map<Point, Set<Cardinal>> coordToEntrances, boolean sealOff) {
        for (Map.Entry<Point, Set<Cardinal>> entry : coordToEntrances.entrySet()) {
            if (!isValidUnit(coord, entry, sealOff)) {
                return false;
            }
        }
        return true;
    }

    private boolean isValidUnit(Point coord, Map.Entry<Point, Set<Cardinal>> entry, boolean sealOff) {
        Point point = entry.getKey().add(coord);
        if (!isValidCoordinate(point)) return false;
        Set<Cardinal> cardinalSet = entry.getValue();
        for (Cardinal cardinal : Cardinal.values()) {
            Point adjacentPoint = point.add(cardinal.getDirection());
            if (floorLayout.containsKey(adjacentPoint)) {
                Set<Cardinal> adjacentCardinals = floorLayout.get(adjacentPoint);
                if ((adjacentCardinals.contains(cardinal.getOpposite()) && !cardinalSet.contains(cardinal)) ||
                        (!adjacentCardinals.contains(cardinal.getOpposite()) && cardinalSet.contains(cardinal))) {
                    return false;
                }
            } else {
                if (cardinalSet.contains(cardinal)) {
                    if (sealOff || (!isValidCoordinate(adjacentPoint) && !entranceException(adjacentPoint))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean entranceException(Point point) {
        return generation == 0 && point.equals(direction.getDirection());
    }

}
