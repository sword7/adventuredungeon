package me.sword7.adventuredungeon.generate;

import java.util.List;

public class GridBlueprint implements IBlueprint {

    private List<Room> rooms;

    public GridBlueprint(List<Room> rooms) {
        this.rooms = rooms;
    }

    public List<Room> getRooms() {
        return rooms;
    }

}
