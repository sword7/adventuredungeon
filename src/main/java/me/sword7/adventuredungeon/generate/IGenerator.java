package me.sword7.adventuredungeon.generate;

public interface IGenerator {

    IBlueprint generate();

}
