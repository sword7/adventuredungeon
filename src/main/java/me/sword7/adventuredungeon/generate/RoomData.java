package me.sword7.adventuredungeon.generate;

import me.sword7.adventuredungeon.util.math.Point;

import java.util.List;

public class RoomData {

    private List<Point> chestLocations;
    private List<Point> spawnerLocation;

    public RoomData(List<Point> chestLocations, List<Point> spawnerLocation) {
        this.chestLocations = chestLocations;
        this.spawnerLocation = spawnerLocation;
    }

    public List<Point> getChestLocations() {
        return chestLocations;
    }

    public List<Point> getSpawnerLocation() {
        return spawnerLocation;
    }

}
