package me.sword7.adventuredungeon.generate;

import me.sword7.adventuredungeon.dungeon.Dungeon;

public interface IConstructor {

    Dungeon construct();

}
