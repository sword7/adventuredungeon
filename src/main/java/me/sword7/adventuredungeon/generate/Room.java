package me.sword7.adventuredungeon.generate;

import me.sword7.adventuredungeon.structure.*;
import me.sword7.adventuredungeon.util.math.*;
import org.bukkit.Location;

import java.util.*;

public class Room {

    private static Random random = new Random();
    private Point coord;
    private IFrame frame;
    private Orientation orientation;

    public Room(Point coord, IFrame frame, Orientation orientation) {
        this.coord = coord;
        this.frame = frame;
        this.orientation = orientation;
    }

    public RoomData draw(Location dungeonLoc, ITheme theme, Point gridScale) {

        Rotation rot = orientation.getRotation();
        Point shift = orientation.getShift();

        int x = dungeonLoc.getBlockX() + (gridScale.getX() * (coord.getX() + shift.getX()));
        int y = dungeonLoc.getBlockY() + (gridScale.getY() * (coord.getY() + shift.getY()));
        int z = dungeonLoc.getBlockZ() + (gridScale.getZ() * (coord.getZ() + shift.getZ()));
        Location roomLoc = new Location(dungeonLoc.getWorld(), x, y, z);

        Structure frameStruct = frame.getStructure().clone();
        frameStruct.rotate(rot);
        frameStruct.draw(roomLoc, theme);


        List<Point> chestLocations = Collections.EMPTY_LIST;
        List<Point> spawnerLocations = Collections.EMPTY_LIST;

        Decoration decoration = frame.selectDecoration(random);
        if (decoration != null) {
            Structure decStruct = decoration.getStructure().clone();
            decStruct.rotate(rot);
            RoomData data = decStruct.drawForData(roomLoc, theme);
            chestLocations = data.getChestLocations();
            spawnerLocations = data.getSpawnerLocation();
        }

        return new RoomData(chestLocations, spawnerLocations);

    }

    public IBound getBounds(Location dungeonLoc, Point gridScale) {
        Point shift = orientation.getShift();

        int x = dungeonLoc.getBlockX() + (gridScale.getX() * (coord.getX() + shift.getX()));
        int y = dungeonLoc.getBlockY() + (gridScale.getY() * (coord.getY() + shift.getY()));
        int z = dungeonLoc.getBlockZ() + (gridScale.getZ() * (coord.getZ() + shift.getZ()));

        Point min = new Point(x, y, z);

        Point dim = orientation.getRotation().rotateDimensions(frame.getDimensions());
        Point max = min.clone().add(dim.getX() * gridScale.getX() - 1, dim.getY() * gridScale.getY() - 1, dim.getZ() * gridScale.getZ() - 1);

        return new BoundRect(min, max);
    }

    public Map<Point, Set<Cardinal>> getFinalLayout() {
        Map<Point, Set<Cardinal>> shiftedMap = new HashMap<>();
        Point dim = frame.getDimensions();
        Point shift = orientation.getShift();
        Rotation r = orientation.getRotation();
        for (Map.Entry<Point, Set<Cardinal>> entry : frame.getGridCoordToEntrances().entrySet()) {
            Point originalPoint = entry.getKey();
            Set<Cardinal> originalCardinals = entry.getValue();
            Point shiftedPoint = r.rotatePoint(originalPoint, dim.getX(), dim.getZ()).add(shift.getX(), shift.getY(), shift.getZ());
            Set<Cardinal> shiftedCardinals = new HashSet<>();
            for (Cardinal c : originalCardinals) {
                shiftedCardinals.add(r.rotateCardinal(c));
            }
            shiftedMap.put(coord.add(shiftedPoint), shiftedCardinals);
        }
        return shiftedMap;
    }

}
