package me.sword7.adventuredungeon;

import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.dungeon.DungeonType;
import me.sword7.adventuredungeon.dungeons.crypt.CryptLootPoolSupplier;
import me.sword7.adventuredungeon.lootpool.loot.ILoot;
import me.sword7.adventuredungeon.mob.CrossBowZombie;
import net.minecraft.server.v1_16_R3.WorldServer;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.Random;

public class CommandTest implements CommandExecutor {

    private static Random random = new Random();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            testLizardBase(player);

            /*
            CrossBowZombie crossBowZombie = new CrossBowZombie(player.getLocation());
            Zombie zombie = (Zombie) crossBowZombie.getBukkitEntity();
            zombie.getEquipment().setItemInMainHand(new ItemStack(Material.CROSSBOW));
            WorldServer worldServer = ((CraftWorld) player.getWorld()).getHandle();
            worldServer.addEntity(crossBowZombie);
            */

            //DungeonMobType.DINOLFOS.getDungeonMob().spawn(player.getLocation(), random);

            sender.sendMessage("a");
        }
        return false;
    }

    private void testLizardBase(Player player) {
        DungeonType dungeonType = DungeonType.LIZARD_BASE;
        Chunk source = player.getLocation().getChunk();
        World world = player.getLocation().getWorld();

        int chunkX = source.getX() * 16;
        int chunkZ = source.getZ() * 16;

        for (int i = 0; i < 3; i++) {
            int x = chunkX + random.nextInt(15);
            int z = chunkZ + random.nextInt(15);
            Biome biome = Version.getValue() < 115 ? world.getBiome(chunkX, chunkZ) : world.getBiome(chunkX, 60, chunkZ);
            if (dungeonType.getPlacer().place(world, x, z, dungeonType.getThemeFactory().select(biome, random), random))
                break;
        }
    }


    private void testLoot(Player player) {
        Location location = player.getLocation();
        Block block = location.getBlock();
        block.setType(Material.CHEST);
        Chest chest = (Chest) block.getState();
        ILoot loot = CryptLootPoolSupplier.getLootPool().selectChest(random, Collections.EMPTY_LIST, Collections.EMPTY_LIST, 0);
        loot.fill(chest.getSnapshotInventory(), random, Collections.EMPTY_LIST, Collections.EMPTY_LIST, 0);
        chest.update();
    }

}
