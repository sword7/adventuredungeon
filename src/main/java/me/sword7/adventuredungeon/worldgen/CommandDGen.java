package me.sword7.adventuredungeon.worldgen;

import me.sword7.adventuredungeon.config.Language;
import me.sword7.adventuredungeon.config.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.generator.BlockPopulator;

public class CommandDGen implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (Permissions.canGenerate(sender)) {
                if (args.length > 0) {
                    processSetRate(player, args);
                    for (BlockPopulator blockPopulator : player.getWorld().getPopulators()) {
                        Bukkit.getConsoleSender().sendMessage(blockPopulator.getClass().toString());
                    }
                } else {
                    processStatus(player);
                }
            } else {
                sender.sendMessage(ChatColor.RED + Language.WARN_NOT_PERMITTED.toString());
            }
        }
        return false;
    }

    private void processSetRate(Player player, String[] args) {
        World world = player.getWorld();
        SpawnRate spawnRate = getSpawnRate(args[0].toUpperCase());
        if (world.getEnvironment() == World.Environment.NORMAL) {
            if (spawnRate != null) {
                Generator.setSpawnRate(world, spawnRate);
                player.sendMessage(ChatColor.GRAY + Language.SUCCESS_SET_RATE.fromWorldAndRate(world.getName(), spawnRate.toString()));
            } else {
                player.sendMessage(ChatColor.RED + Language.WARN_UNKNOWN_RATE.toString());
            }
        } else {
            player.sendMessage(ChatColor.RED + Language.WARN_INVALID_WORLD.toString());
        }
    }

    private SpawnRate getSpawnRate(String s) {
        try {
            return SpawnRate.valueOf(s);
        } catch (Exception e) {
            return null;
        }
    }

    private void processStatus(Player player) {
        World world = player.getWorld();
        SpawnRate spawnRate = Generator.getSpawnRate(world);
        player.sendMessage(ChatColor.GRAY + Language.STATUS_CHECK_RATE.fromWorldAndRate(world.getName(), spawnRate.toString()));
    }

}
