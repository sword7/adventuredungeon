package me.sword7.adventuredungeon.worldgen;

import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.dungeon.DungeonCache;
import me.sword7.adventuredungeon.dungeon.DungeonType;
import me.sword7.adventuredungeon.structure.IThemeFactory;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.generator.BlockPopulator;

import java.util.Map;
import java.util.Random;

public class DungeonPopulator extends BlockPopulator {

    private static final int PLACEMENT_ATTEMPTS = 3;

    private Map<Biome, Double> biomeToChance;
    private SpawnRate spawnRate;
    private IDungeonPlacer placer;
    private IThemeFactory themeFactory;

    public DungeonPopulator(SpawnRate spawnRate) {
        this.spawnRate = spawnRate;
        this.biomeToChance = DungeonType.CRYPT.getBiomeToChance();
        this.placer = DungeonType.CRYPT.getPlacer();
        this.themeFactory = DungeonType.CRYPT.getThemeFactory();
    }


    @Override
    public void populate(World world, Random random, Chunk source) {


        int chunkX = source.getX() * 16;
        int chunkZ = source.getZ() * 16;

        Biome biome = Version.getValue() < 116 ? world.getBiome(chunkX, chunkZ) : world.getBiome(chunkX, 60, chunkZ);

        if (biomeToChance.containsKey(biome)) {
            double chance = biomeToChance.get(biome);
            if (random.nextDouble() < chance * spawnRate.getModifier()) {
                Location location = new Location(world, chunkX, 60, chunkZ);
                if (!DungeonCache.hasNearby(location, 220)) {
                    for (int i = 0; i < PLACEMENT_ATTEMPTS; i++) {
                        int x = chunkX + random.nextInt(15);
                        int z = chunkZ + random.nextInt(15);
                        if (placer.place(world, x, z, themeFactory.select(biome, random), random)) break;
                    }
                }
            }
        }

    }

    public void setSpawnRate(SpawnRate spawnRate) {
        this.spawnRate = spawnRate;
    }

}

