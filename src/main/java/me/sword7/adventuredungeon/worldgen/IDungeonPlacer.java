package me.sword7.adventuredungeon.worldgen;

import me.sword7.adventuredungeon.structure.ITheme;
import org.bukkit.World;

import java.util.Random;

public interface IDungeonPlacer {

    boolean place(World world, int spawnX, int spawnZ, ITheme theme, Random random);

}
