package me.sword7.adventuredungeon.worldgen;

import me.sword7.adventuredungeon.AdventureDungeon;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Generator implements Listener {

    private static Map<UUID, SpawnRate> worldToRate = new HashMap<>();
    private static GeneratorFlatFile generatorFlatFile = new GeneratorFlatFile();

    public Generator() {
        Plugin plugin = AdventureDungeon.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        for (World world : Bukkit.getWorlds()) {
            resetWorld(world);
        }
        load();
    }

    private static void resetWorld(World world) {
        List<BlockPopulator> populators = world.getPopulators();
        for (int i = populators.size() - 1; i >= 0; i--) {
            BlockPopulator populator = populators.get(i);
            if (populator.toString().contains("DungeonPopulator")) {
                populators.remove(i);
            }
        }
    }

    private void load() {
        worldToRate.putAll(generatorFlatFile.fetch());
        for (Map.Entry<UUID, SpawnRate> entry : worldToRate.entrySet()) {
            World world = Bukkit.getWorld(entry.getKey());
            if (world != null && world.getEnvironment() == World.Environment.NORMAL) {
                SpawnRate rate = entry.getValue();
                world.getPopulators().add(new DungeonPopulator(rate));
            }
        }
    }

    public static void save() {
        generatorFlatFile.store(worldToRate);
    }

    public static void shutdown() {
        save();
    }

    public static void setSpawnRate(World world, SpawnRate rate) {
        if (rate != SpawnRate.NONE) {
            worldToRate.put(world.getUID(), rate);
            boolean found = false;
            for (BlockPopulator populator : world.getPopulators()) {
                if (populator.toString().contains("DungeonPopulator")) {
                    ((DungeonPopulator) populator).setSpawnRate(rate);
                    found = true;
                    break;
                }
            }
            if (!found) world.getPopulators().add(new DungeonPopulator(rate));
        } else {
            worldToRate.remove(world.getUID());
            resetWorld(world);
        }
    }

    public static SpawnRate getSpawnRate(World world) {
        UUID worldId = world.getUID();
        if (worldToRate.containsKey(worldId)) {
            return worldToRate.get(worldId);
        } else {
            return SpawnRate.NONE;
        }
    }

    @EventHandler
    public void onWorldLoad(WorldLoadEvent e) {
        World world = e.getWorld();
        resetWorld(world);
        UUID worldId = world.getUID();
        if (worldToRate.containsKey(worldId)) {
            SpawnRate rate = worldToRate.get(worldId);
            world.getPopulators().add(new DungeonPopulator(rate));
        }
    }

}
