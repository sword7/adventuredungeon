package me.sword7.adventuredungeon.worldgen;

import me.sword7.adventuredungeon.util.Util;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GeneratorFlatFile {

    private File file = new File("plugins/AdventureDungeon/data", "dungeonGenerator.yml");

    public void store(Map<UUID, SpawnRate> worldToRate) {
        file.delete();
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        for (Map.Entry<UUID, SpawnRate> entry : worldToRate.entrySet()) {
            config.set(entry.getKey().toString(), entry.getValue().toString());
        }
        Util.save(config, file);
    }

    public Map<UUID, SpawnRate> fetch() {
        Map<UUID, SpawnRate> worldToRate = new HashMap<>();
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        for (String worldIdString : config.getRoot().getKeys(false)) {
            try {
                UUID worldId = UUID.fromString(worldIdString);
                SpawnRate spawnRate = SpawnRate.valueOf(config.getString(worldIdString));
                worldToRate.put(worldId, spawnRate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return worldToRate;

    }


}
