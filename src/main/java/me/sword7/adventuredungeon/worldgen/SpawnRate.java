package me.sword7.adventuredungeon.worldgen;

public enum SpawnRate {

    NONE(0),
    LOW(0.5),
    MED(1),
    HIGH(2),
    ;

    private double modifier;

    SpawnRate(double modifier) {
        this.modifier = modifier;
    }

    public double getModifier() {
        return modifier;
    }
}
