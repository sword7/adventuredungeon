package me.sword7.adventuredungeon.worldgen;

import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.dungeon.DungeonType;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Random;

public class CommandPopulate implements CommandExecutor {

    private static Random random = new Random();
    private DungeonType dungeonType = DungeonType.CRYPT;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Chunk source = player.getLocation().getChunk();
            World world = player.getLocation().getWorld();

            int chunkX = source.getX() * 16;
            int chunkZ = source.getZ() * 16;

            for (int i = 0; i < 3; i++) {
                int x = chunkX + random.nextInt(15);
                int z = chunkZ + random.nextInt(15);
                Biome biome = Version.getValue() < 115 ? world.getBiome(chunkX, chunkZ) : world.getBiome(chunkX, 60, chunkZ);
                if (dungeonType.getPlacer().place(world, x, z, dungeonType.getThemeFactory().select(biome, random), random))
                    break;
            }


        }
        return false;
    }



}
