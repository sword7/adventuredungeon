package me.sword7.adventuredungeon.mob;

import org.bukkit.Color;

public interface ILeatherSkin extends IDungeonMob {

    Color getSkinColor();

    Color getHurtColor();


}
