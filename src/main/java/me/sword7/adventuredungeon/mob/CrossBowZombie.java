package me.sword7.adventuredungeon.mob;

import net.minecraft.server.v1_16_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;

public class CrossBowZombie extends EntityZombie implements ICrossbow {

    public CrossBowZombie(Location location) {
        super(EntityTypes.ZOMBIE, ((CraftWorld) location.getWorld()).getHandle());
        this.setPosition(location.getX(), location.getY(), location.getZ());


        this.goalSelector.a(0, new PathfinderGoalCrossbowAttack<>(this, 1.0, 1.0f));
    }

    @Override
    public void a(EntityLiving entityLiving, float v) {
        super.a(entityLiving, v);
    }

    @Override
    public void b(boolean b) {

    }

    @Override
    public void a(EntityLiving entityLiving, ItemStack itemStack, IProjectile iProjectile, float v) {

    }

    @Override
    public void U_() {

    }
}
