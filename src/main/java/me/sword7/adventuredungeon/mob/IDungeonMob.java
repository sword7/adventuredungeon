package me.sword7.adventuredungeon.mob;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Random;

public interface IDungeonMob {

    void spawn(Location location, Random rand);

    void applyDrops(LivingEntity entity, List<ItemStack> drops, Random rand, int lootingLevel);

}
