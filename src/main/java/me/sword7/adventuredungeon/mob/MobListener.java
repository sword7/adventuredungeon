package me.sword7.adventuredungeon.mob;

import me.sword7.adventuredungeon.AdventureDungeon;
import me.sword7.adventuredungeon.util.Scheduler;
import me.sword7.adventuredungeon.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.Plugin;

import java.util.*;

public class MobListener implements Listener {

    public MobListener() {
        Plugin plugin = AdventureDungeon.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onHurt(EntityDamageEvent e) {
        if (e.getEntity() instanceof LivingEntity) {
            LivingEntity livingEntity = (LivingEntity) e.getEntity();
            EntityEquipment entityEquipment = livingEntity.getEquipment();
            ItemStack head = entityEquipment.getHelmet();
            if (head != null && head.getType() == Material.PLAYER_HEAD) {
                SkullMeta skullMeta = (SkullMeta) head.getItemMeta();
                if (skullMeta.hasOwner()) {
                    UUID headId = skullMeta.getOwningPlayer().getUniqueId();
                    if (MobHead.isMobHead(headId)) {
                        MobHead mobHead = MobHead.getMobHead(headId);
                        doHurt(livingEntity, mobHead);
                    }
                }
            }
        }
    }

    private static Set<UUID> hurtingEntities = new HashSet<>();

    private void doHurt(LivingEntity entity, MobHead head) {
        UUID entityId = entity.getUniqueId();
        if (!hurtingEntities.contains(entityId)) {
            hurtingEntities.add(entityId);
            entity.getEquipment().setHelmet(head.getHeadHurt());

            boolean skinFlash = false;
            Color skinColor = Color.WHITE;
            if (head.getMobType().getDungeonMob() instanceof ILeatherSkin) {
                skinFlash = true;
                ILeatherSkin leatherSkin = (ILeatherSkin) head.getMobType().getDungeonMob();
                skinColor = leatherSkin.getSkinColor();
                colorLeather(entity, leatherSkin.getHurtColor());
            }

            final boolean finalFlash = skinFlash;
            final Color finalColor = skinColor;

            Scheduler.runLater(() -> {
                if (!entity.isDead()) {
                    entity.getEquipment().setHelmet(head.getHead());
                    if (finalFlash) colorLeather(entity, finalColor);
                }
                hurtingEntities.remove(entityId);
            }, 10);

        }
    }

    private void colorLeather(LivingEntity entity, Color color) {
        ItemStack chest = entity.getEquipment().getChestplate();
        ItemMeta chestMeta = chest.getItemMeta();
        if(chestMeta instanceof LeatherArmorMeta){
            LeatherArmorMeta leatherChestMeta = (LeatherArmorMeta) chestMeta;
            leatherChestMeta.setColor(color);
            ItemStack replaceChest = new ItemStack(Material.LEATHER_CHESTPLATE);
            replaceChest.setItemMeta(leatherChestMeta);
            entity.getEquipment().setChestplate(replaceChest);
        }

        ItemStack pants = entity.getEquipment().getLeggings();
        ItemMeta pantsMeta = pants.getItemMeta();
        if(pantsMeta instanceof LeatherArmorMeta){
            LeatherArmorMeta leatherPantsMeta = (LeatherArmorMeta) pantsMeta;
            leatherPantsMeta.setColor(color);
            ItemStack replacementPants = new ItemStack(Material.LEATHER_LEGGINGS);
            replacementPants.setItemMeta(leatherPantsMeta);
            entity.getEquipment().setLeggings(replacementPants);
        }

        ItemStack boots = entity.getEquipment().getBoots();
        ItemMeta bootsMeta = boots.getItemMeta();
        if(bootsMeta instanceof LeatherArmorMeta){
            LeatherArmorMeta leatherBootsMeta = (LeatherArmorMeta) bootsMeta;
            leatherBootsMeta.setColor(color);
            ItemStack replacementBoots = new ItemStack(Material.LEATHER_BOOTS);
            replacementBoots.setItemMeta(leatherBootsMeta);
            entity.getEquipment().setBoots(replacementBoots);
        }

    }

    private static Random rand = new Random();

    @EventHandler
    public void onDeath(EntityDeathEvent e) {
        DungeonMobType dungeonMobType = getMobType(e.getEntity(), e.getDrops());
        if (dungeonMobType != null) {
            dungeonMobType.getDungeonMob().applyDrops(e.getEntity(), e.getDrops(), rand, getLootingLevel(e));
        }
    }


    public DungeonMobType getMobType(LivingEntity entity, List<ItemStack> drops) {
        ItemStack head = entity.getEquipment().getHelmet();
        if (Util.isEmpty(head)) {
            for (ItemStack itemStack : drops) {
                if (itemStack.getType() == Material.PLAYER_HEAD) {
                    head = itemStack;
                }
            }
        }
        if (!Util.isEmpty(head) && head.getType() == Material.PLAYER_HEAD) {
            SkullMeta skullMeta = (SkullMeta) head.getItemMeta();
            UUID headId = skullMeta.hasOwner() ? skullMeta.getOwningPlayer().getUniqueId() : null;
            if (MobHead.isMobHead(headId)) {
                MobHead mobHead = MobHead.getMobHead(headId);
                return mobHead.getMobType();
            }
        }
        return null;
    }

    public int getLootingLevel(EntityDeathEvent e) {
        if (e.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e.getEntity().getLastDamageCause();
            if (event.getDamager() instanceof Player) {
                Player player = (Player) event.getDamager();
                ItemStack mainHand = player.getEquipment().getItemInMainHand();
                if (mainHand != null && mainHand.hasItemMeta()) {
                    ItemMeta meta = mainHand.getItemMeta();
                    return meta.getEnchantLevel(Enchantment.LOOT_BONUS_MOBS);
                }
            }
        }
        return 0;
    }

}
