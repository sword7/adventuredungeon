package me.sword7.adventuredungeon.mob;

import me.sword7.adventuredungeon.util.BlockColor;
import me.sword7.adventuredungeon.util.Util;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Banner;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Skeleton;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.loot.LootTables;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Stalfos implements IDungeonMob {

    private static Color leatherColor = Color.fromRGB(70, 53, 46);

    @Override
    public void spawn(Location location, Random rand) {
        Skeleton skeleton = (Skeleton) location.getWorld().spawnEntity(location, EntityType.SKELETON);
        equip(skeleton, Garment.getRandom(), BlockColor.RED, rand);
    }

    public void spawnColored(Location location, Garment garment, BlockColor blockColor, Random rand) {
        Skeleton skeleton = (Skeleton) location.getWorld().spawnEntity(location, EntityType.SKELETON);
        equip(skeleton, garment, blockColor, rand);
    }

    private void equip(Skeleton skeleton, Garment garment, BlockColor blockColor, Random rand) {

        skeleton.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(25);
        skeleton.setHealth(25);
        skeleton.setLootTable(LootTables.EMPTY.getLootTable());

        EntityEquipment equipment = skeleton.getEquipment();
        equipment.clear();
        ItemStack head = MobHead.getStalfosHead().getHead();
        ;
        equipment.setHelmet(head);
        equipment.setChestplate(selectChest(rand, garment));
        equipment.setLeggings(selectLeggings(rand, garment));
        equipment.setBoots(selectBoots(rand, garment));
        ItemStack mainHand = selectMainHandWeapon(rand);
        equipment.setItemInMainHand(mainHand);
        if (mainHand.getType() != Material.BOW) {
            equipment.setItemInOffHand(selectOffhandWeapon(rand, blockColor));
        }

    }

    public ItemStack selectChest(Random rand, Garment garment) {
        double r = rand.nextDouble();
        ItemStack chestPlate = null;
        if (r < 0.15) {
            chestPlate = new ItemStack(Material.IRON_CHESTPLATE);
        } else if (r < 0.30) {
            chestPlate = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
        } else if (r < 0.5) {
            chestPlate = new ItemStack(Material.LEATHER_CHESTPLATE);
            LeatherArmorMeta meta = (LeatherArmorMeta) chestPlate.getItemMeta();
            meta.setColor(garment.getColor());
            chestPlate.setItemMeta(meta);
        }

        if (chestPlate != null && rand.nextDouble() < 0.1) {
            ItemMeta meta = chestPlate.getItemMeta();
            meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false);
            chestPlate.setItemMeta(meta);
        }

        return chestPlate;
    }

    public ItemStack selectLeggings(Random rand, Garment garment) {
        double r = rand.nextDouble();
        ItemStack leggings = null;
        if (r < 0.1) {
            leggings = new ItemStack(Material.IRON_LEGGINGS);
        } else if (r < 0.2) {
            leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
        } else if (r < 0.4) {
            leggings = new ItemStack(Material.LEATHER_LEGGINGS);
            LeatherArmorMeta meta = (LeatherArmorMeta) leggings.getItemMeta();
            meta.setColor(garment.getColor());
            leggings.setItemMeta(meta);
        } else if (r < 0.5) {
            leggings = new ItemStack(Material.LEATHER_LEGGINGS);
            LeatherArmorMeta meta = (LeatherArmorMeta) leggings.getItemMeta();
            meta.setColor(leatherColor);
            leggings.setItemMeta(meta);
        }

        if (leggings != null && rand.nextDouble() < 0.1) {
            ItemMeta meta = leggings.getItemMeta();
            meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false);
            leggings.setItemMeta(meta);
        }

        return leggings;
    }

    public ItemStack selectBoots(Random rand, Garment garment) {
        double r = rand.nextDouble();
        ItemStack boots = null;
        if (r < 0.3) {
            boots = new ItemStack(Material.IRON_BOOTS);
        } else if (r < 0.5) {
            boots = new ItemStack(Material.LEATHER_BOOTS);
            LeatherArmorMeta meta = (LeatherArmorMeta) boots.getItemMeta();
            meta.setColor(garment.getColor());
            boots.setItemMeta(meta);
        } else if (r < 0.6) {
            boots = new ItemStack(Material.LEATHER_BOOTS);
            LeatherArmorMeta meta = (LeatherArmorMeta) boots.getItemMeta();
            meta.setColor(leatherColor);
            boots.setItemMeta(meta);
        }

        if (boots != null && rand.nextDouble() < 0.1) {
            ItemMeta meta = boots.getItemMeta();
            meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false);
            boots.setItemMeta(meta);
        }

        return boots;
    }

    public ItemStack selectMainHandWeapon(Random rand) {
        double r = rand.nextDouble();
        ItemStack weapon;
        if (r < 0.15) {
            weapon = new ItemStack(Material.IRON_AXE);
            ItemMeta meta = weapon.getItemMeta();
            meta.setCustomModelData(701);
            weapon.setItemMeta(meta);
        } else if (r < 0.30) {
            weapon = new ItemStack(Material.BOW);
        } else {
            weapon = new ItemStack(Material.IRON_SWORD);
            ItemMeta meta = weapon.getItemMeta();
            meta.setCustomModelData(Util.randomInt(701, 705));
            weapon.setItemMeta(meta);
        }

        if (rand.nextDouble() < 0.1) {
            ItemMeta meta = weapon.getItemMeta();
            Enchantment enchantment = weapon.getType() == Material.BOW ? Enchantment.ARROW_DAMAGE : Enchantment.DAMAGE_ALL;
            meta.addEnchant(enchantment, 1, false);
            weapon.setItemMeta(meta);
        }

        return weapon;

    }

    public ItemStack selectOffhandWeapon(Random rand, BlockColor blockColor) {
        double r = rand.nextDouble();
        ItemStack weapon = null;
        if (r < 0.12) {
            weapon = new ItemStack(Material.SHIELD);
            BlockStateMeta shieldMeta = (BlockStateMeta) weapon.getItemMeta();
            Banner banner = (Banner) shieldMeta.getBlockState();
            List<Pattern> patterns = new ArrayList<>();
            patterns.add(new Pattern(blockColor.getDyeColor(), PatternType.BASE));
            r = rand.nextDouble();
            if (r < 0.33) {
                patterns.add(new Pattern(DyeColor.BLACK, PatternType.HALF_VERTICAL));
            } else if (r < 0.66) {
                patterns.add(new Pattern(DyeColor.BLACK, PatternType.STRAIGHT_CROSS));
            }
            banner.setPatterns(patterns);
            banner.update();
            shieldMeta.setBlockState(banner);
            weapon.setItemMeta(shieldMeta);
        } else if (r < 0.13) {
            weapon = new ItemStack(Material.IRON_AXE);
            ItemMeta meta = weapon.getItemMeta();
            meta.setCustomModelData(701);
            weapon.setItemMeta(meta);
        } else if (r < 0.20) {
            weapon = new ItemStack(Material.IRON_SWORD);
            ItemMeta meta = weapon.getItemMeta();
            meta.setCustomModelData(Util.randomInt(701, 705));
            weapon.setItemMeta(meta);
        }

        if (weapon != null && weapon.getType() != Material.SHIELD && rand.nextDouble() < 0.1) {
            ItemMeta meta = weapon.getItemMeta();
            Enchantment enchantment = weapon.getType() == Material.BOW ? Enchantment.ARROW_DAMAGE : Enchantment.DAMAGE_ALL;
            meta.addEnchant(enchantment, 1, false);
            weapon.setItemMeta(meta);
        }

        return weapon;

    }


    @Override
    public void applyDrops(LivingEntity entity, List<ItemStack> drops, Random rand, int lootingLevel) {
        drops.clear();
        drops.add(new ItemStack(Material.BONE, Util.randomInt(0, lootingLevel + 2)));
        if (rand.nextDouble() < 0.1) {
            drops.add(new ItemStack(Material.GOLD_NUGGET, Util.randomInt(1, 5 + lootingLevel)));
        }

        EntityEquipment equipment = entity.getEquipment();
        ItemStack mainHand = equipment.getItemInMainHand();
        if (!Util.isEmpty(mainHand) && mainHand.getType() == Material.BOW) {
            drops.add(new ItemStack(Material.ARROW, Util.randomInt(0, 2 + lootingLevel)));
        }
        double dropChance = 0.05 + lootingLevel * 0.01;
        ApplyEquipmentDrop(equipment.getChestplate(), dropChance, rand, drops);
        ApplyEquipmentDrop(equipment.getLeggings(), dropChance, rand, drops);
        ApplyEquipmentDrop(equipment.getBoots(), dropChance, rand, drops);
        ApplyEquipmentDrop(equipment.getItemInMainHand(), dropChance, rand, drops);
        ApplyEquipmentDrop(equipment.getItemInOffHand(), dropChance, rand, drops);
    }

    private void ApplyEquipmentDrop(ItemStack equipment, double chance, Random rand, List<ItemStack> drops) {
        if (!Util.isEmpty(equipment) && rand.nextDouble() < chance) {
            ItemStack equipmentDrop = equipment;
            Util.applyDurability(equipmentDrop, 0.05, 0.40);
            drops.add(equipmentDrop);
        }
    }

    public enum Garment {
        CYAN(Color.fromRGB(24, 112, 120)),
        GREEN(Color.fromRGB(59, 124, 104)),
        GRAY_PURPLE(Color.fromRGB(126, 118, 142)),
        INDIGO(Color.fromRGB(84, 69, 129)),
        RED(Color.fromRGB(163, 55, 41)),
        BLACK(Color.fromRGB(82, 73, 66)),

        ;

        private Color color;

        Garment(Color color) {
            this.color = color;
        }

        public Color getColor() {
            return color;
        }

        public static Garment getRandom() {
            return values()[(int) (Math.random() * values().length)];
        }

    }


}
