package me.sword7.adventuredungeon.mob;

import org.bukkit.Location;

import java.util.Random;

public interface IBestiary {

    void spawn(DungeonMobType mobType, Location location, Random rand);

    double getAmbientSpawnChance();

    double getOverrideSpawnChance();

    void ambientSpawn(Location location, Random random);

}
