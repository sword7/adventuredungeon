package me.sword7.adventuredungeon.mob;

import me.sword7.adventuredungeon.util.Util;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Banner;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.loot.LootTables;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Dinolfos implements ILeatherSkin {

    //TODO custom entity with ranged if spawn with crossbow!!!!
    //TODO fire breath?

    private static final Color SCALE_COLOR = Color.fromRGB(111, 135, 46);
    private static final Color HURT_COLOR = Color.fromRGB(163, 97, 58);

    @Override
    public void spawn(Location location, Random rand) {
        Zombie zombie = (Zombie) location.getWorld().spawnEntity(location, EntityType.ZOMBIE);
        equip(zombie, rand);
    }

    @Override
    public void applyDrops(LivingEntity entity, List<ItemStack> drops, Random rand, int lootingLevel) {
        drops.clear();
        drops.add(new ItemStack(Material.SCUTE, Util.randomInt(0, lootingLevel + 2)));
        if (rand.nextDouble() < 0.08) {
            drops.add(new ItemStack(Material.IRON_INGOT, 1));
        }

        EntityEquipment equipment = entity.getEquipment();
        ItemStack mainHand = equipment.getItemInMainHand();
        if (!Util.isEmpty(mainHand) && mainHand.getType() == Material.CROSSBOW) {
            drops.add(new ItemStack(Material.ARROW, Util.randomInt(0, 2 + lootingLevel)));
        }
        double dropChance = 0.05 + lootingLevel * 0.01;
        ApplyEquipmentDrop(equipment.getChestplate(), dropChance, rand, drops);
        ApplyEquipmentDrop(equipment.getLeggings(), dropChance, rand, drops);
        ApplyEquipmentDrop(equipment.getBoots(), dropChance, rand, drops);
        ApplyEquipmentDrop(equipment.getItemInMainHand(), dropChance, rand, drops);
        ApplyEquipmentDrop(equipment.getItemInOffHand(), dropChance, rand, drops);
    }

    private void ApplyEquipmentDrop(ItemStack equipment, double chance, Random rand, List<ItemStack> drops) {
        if (!Util.isEmpty(equipment) && !(equipment.getItemMeta() instanceof LeatherArmorMeta) && rand.nextDouble() < chance) {
            ItemStack equipmentDrop = equipment;
            Util.applyDurability(equipmentDrop, 0.15, 0.65);
            drops.add(equipmentDrop);
        }
    }


    private void equip(Zombie zombie, Random rand) {

        zombie.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(27);
        zombie.setHealth(27);
        zombie.setLootTable(LootTables.EMPTY.getLootTable());

        EntityEquipment equipment = zombie.getEquipment();
        equipment.clear();
        ItemStack head = MobHead.DINOLFOS_1.getHead();
        equipment.setHelmet(head);
        equipment.setChestplate(selectChest(rand));
        equipment.setLeggings(selectLeggings(rand));
        equipment.setBoots(selectBoots(rand));
        ItemStack mainHand = selectMainHandWeapon(rand);
        equipment.setItemInMainHand(mainHand);
        if (mainHand != null && mainHand.getType() != Material.CROSSBOW) {
            equipment.setItemInOffHand(selectOffhandWeapon(rand));
        }

    }

    public ItemStack selectChest(Random rand) {
        double r = rand.nextDouble();
        ItemStack chestPlate;
        boolean scale = false;
        if (r < 0.75) {
            chestPlate = new ItemStack(Material.IRON_CHESTPLATE);
        } else {
            chestPlate = new ItemStack(Material.LEATHER_CHESTPLATE);
            LeatherArmorMeta meta = (LeatherArmorMeta) chestPlate.getItemMeta();
            meta.setColor(SCALE_COLOR);
            chestPlate.setItemMeta(meta);
            scale = true;
        }

        if (chestPlate != null && !scale && rand.nextDouble() < 0.025) {
            ItemMeta meta = chestPlate.getItemMeta();
            meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false);
            chestPlate.setItemMeta(meta);
        }

        return chestPlate;
    }

    public ItemStack selectLeggings(Random rand) {
        double r = rand.nextDouble();
        ItemStack leggings;
        boolean scale = false;
        if (r < 0.75) {
            leggings = new ItemStack(Material.IRON_LEGGINGS);
        } else {
            leggings = new ItemStack(Material.LEATHER_LEGGINGS);
            LeatherArmorMeta meta = (LeatherArmorMeta) leggings.getItemMeta();
            meta.setColor(SCALE_COLOR);
            leggings.setItemMeta(meta);
            scale = true;
        }

        if (leggings != null && !scale && rand.nextDouble() < 0.025) {
            ItemMeta meta = leggings.getItemMeta();
            meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false);
            leggings.setItemMeta(meta);
        }

        return leggings;
    }

    public ItemStack selectBoots(Random rand) {
        double r = rand.nextDouble();
        ItemStack boots;
        boolean scale = false;
        if (r < 0.75) {
            boots = new ItemStack(Material.IRON_BOOTS);
        } else {
            boots = new ItemStack(Material.LEATHER_BOOTS);
            LeatherArmorMeta meta = (LeatherArmorMeta) boots.getItemMeta();
            meta.setColor(SCALE_COLOR);
            boots.setItemMeta(meta);
            scale = true;
        }

        if (boots != null && !scale && rand.nextDouble() < 0.025) {
            ItemMeta meta = boots.getItemMeta();
            meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false);
            boots.setItemMeta(meta);
        }

        return boots;
    }

    public ItemStack selectMainHandWeapon(Random rand) {
        double r = rand.nextDouble();
        ItemStack weapon;
        if (r < 0.35) {
            weapon = new ItemStack(Material.IRON_AXE);
            ItemMeta meta = weapon.getItemMeta();
            meta.setCustomModelData(702);
            weapon.setItemMeta(meta);
        } else if (r < 0.5) {
            //weapon = new ItemStack(Material.CROSSBOW);
            weapon = null;
        } else {
            weapon = new ItemStack(Material.IRON_SWORD);
            ItemMeta meta = weapon.getItemMeta();
            meta.setCustomModelData(Util.randomInt(706, 710));
            weapon.setItemMeta(meta);
        }

        if (weapon != null && rand.nextDouble() < 0.025) {
            ItemMeta meta = weapon.getItemMeta();
            Enchantment enchantment = weapon.getType() == Material.CROSSBOW ? Enchantment.PIERCING : Enchantment.DAMAGE_ALL;
            meta.addEnchant(enchantment, 1, false);
            weapon.setItemMeta(meta);
        }

        return weapon;

    }

    public ItemStack selectOffhandWeapon(Random rand) {
        double r = rand.nextDouble();
        ItemStack weapon = null;
        if (r < 0.35) {
            weapon = new ItemStack(Material.SHIELD);
            BlockStateMeta shieldMeta = (BlockStateMeta) weapon.getItemMeta();
            Banner banner = (Banner) shieldMeta.getBlockState();
            List<Pattern> patterns = new ArrayList<>();
            patterns.add(new Pattern(DyeColor.GRAY, PatternType.BASE));
            patterns.add(new Pattern(DyeColor.LIGHT_GRAY, PatternType.FLOWER));
            patterns.add(new Pattern(DyeColor.LIGHT_GRAY, PatternType.STRAIGHT_CROSS));
            patterns.add(new Pattern(DyeColor.BLACK, PatternType.CURLY_BORDER));
            banner.setPatterns(patterns);
            banner.update();
            shieldMeta.setBlockState(banner);
            weapon.setItemMeta(shieldMeta);
        }

        return weapon;

    }


    @Override
    public Color getSkinColor() {
        return SCALE_COLOR;
    }

    @Override
    public Color getHurtColor() {
        return HURT_COLOR;
    }
}
