package me.sword7.adventuredungeon.mob;

public enum DungeonMobType {

    STALFOS(new Stalfos()),
    DINOLFOS(new Dinolfos()),
    //WOLFOS
    //MINOTAUR
    //SLIME INFECTED
    //DARK_NUT
    //MOUND_SKELETON
    //MOBLIN
    //POE
    //GHOST_MARKSMAN
    //GIBDO
    ;

    private IDungeonMob dungeonMob;

    DungeonMobType(IDungeonMob dungeonMob) {
        this.dungeonMob = dungeonMob;
    }

    public IDungeonMob getDungeonMob() {
        return dungeonMob;
    }

}
