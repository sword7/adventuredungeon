package me.sword7.adventuredungeon.mob;

import me.sword7.adventuredungeon.util.HeadUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public enum MobHead {

    STALFOS_1("Stalfos1", DungeonMobType.STALFOS,
            "b876b2bf-f11b-4b2e-9567-5458c1afd01c", "ewogICJ0aW1lc3RhbXAiIDogMTYyNjExMDczMDg0NywKICAicHJvZmlsZUlkIiA6ICI0ZGU4MjQxNjJkZTU0MzU5YWFlMDBmMzQ1ZmMyZTY0MSIsCiAgInByb2ZpbGVOYW1lIiA6ICJOYXRoYW5fS2luZyIsCiAgInNpZ25hdHVyZVJlcXVpcmVkIiA6IHRydWUsCiAgInRleHR1cmVzIiA6IHsKICAgICJTS0lOIiA6IHsKICAgICAgInVybCIgOiAiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9hNjg1MjgyNzlkYmQwYTU3MGIwYTkyNjhhNTc4Y2Y2Y2JmMWY5NDkxNWFjOTI5NDU0NzJjZTI4MTRhNGU4OTcxIiwKICAgICAgIm1ldGFkYXRhIiA6IHsKICAgICAgICAibW9kZWwiIDogInNsaW0iCiAgICAgIH0KICAgIH0KICB9Cn0=",
            "b35f420c-ef8f-45b7-819e-b58ed7a67014", "ewogICJ0aW1lc3RhbXAiIDogMTYyNjExNzUyNDMzOSwKICAicHJvZmlsZUlkIiA6ICI0ZWQ4MjMzNzFhMmU0YmI3YTVlYWJmY2ZmZGE4NDk1NyIsCiAgInByb2ZpbGVOYW1lIiA6ICJGaXJlYnlyZDg4IiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzUzN2JmYzE2MTliZjYzYTY1ZWQyYzJlMzYwMDkwYzZmN2E2ZTMxM2Q1ZmRjNmRlYzE1YmUyODc0MzYyNDU3M2EiLAogICAgICAibWV0YWRhdGEiIDogewogICAgICAgICJtb2RlbCIgOiAic2xpbSIKICAgICAgfQogICAgfQogIH0KfQ=="),
    STALFOS_2("Stalfos2", DungeonMobType.STALFOS,
            "478c0545-54c4-4e75-8645-8acc63a378ca", "ewogICJ0aW1lc3RhbXAiIDogMTYyNjExODEzMDk2NSwKICAicHJvZmlsZUlkIiA6ICJmMGIzYmRkMjEwNDg0Y2VlYjZhNTQyYmZiOGEyNTdiMiIsCiAgInByb2ZpbGVOYW1lIiA6ICJBbm9uaW1ZVFQiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvN2U2MDAyYWJlMTc3MzZmZjY3ZTBjMTk4YzdhN2JkMzcxZTI4ZTA3MDIxYTgzMjYxOWMwZTE4Y2E1ODNiOTQxZiIsCiAgICAgICJtZXRhZGF0YSIgOiB7CiAgICAgICAgIm1vZGVsIiA6ICJzbGltIgogICAgICB9CiAgICB9CiAgfQp9",
            "e42c7ce9-7da0-4039-8e0c-6fa0ecfb16f3", "ewogICJ0aW1lc3RhbXAiIDogMTYyNjExODIxMDM5NiwKICAicHJvZmlsZUlkIiA6ICJhYTZhNDA5NjU4YTk0MDIwYmU3OGQwN2JkMzVlNTg5MyIsCiAgInByb2ZpbGVOYW1lIiA6ICJiejE0IiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2NhODZjMGIwZTA4NjEyMmU5M2Q3MzVmNTBkNGRkMWUxNjBmZDQyMDdlMGI0YTQ0Mjc2MTBlMTYyYzE2YjFhZGUiLAogICAgICAibWV0YWRhdGEiIDogewogICAgICAgICJtb2RlbCIgOiAic2xpbSIKICAgICAgfQogICAgfQogIH0KfQ=="),
    STALFOS_3("Stalfos3", DungeonMobType.STALFOS,
            "511610d4-522d-48fe-8a4f-aa96e7a5372b", "ewogICJ0aW1lc3RhbXAiIDogMTYyNjExODMyODQwNSwKICAicHJvZmlsZUlkIiA6ICI3MmNiMDYyMWU1MTA0MDdjOWRlMDA1OTRmNjAxNTIyZCIsCiAgInByb2ZpbGVOYW1lIiA6ICJNb3M5OTAiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzIyOTViNDBmODdiNmJmODRmMTZkODU4YWQ4ZjZkYjcyMzY2YjBlMmJhZWQ3NGRiN2ExNmY5YWEyNDUzODdkZSIsCiAgICAgICJtZXRhZGF0YSIgOiB7CiAgICAgICAgIm1vZGVsIiA6ICJzbGltIgogICAgICB9CiAgICB9CiAgfQp9",
            "99328788-739b-4118-9317-8e754626b517", "ewogICJ0aW1lc3RhbXAiIDogMTYyNjExODM1OTUzMiwKICAicHJvZmlsZUlkIiA6ICI2NGExOGZiZmQ0YWY0Yzg0YjliN2FjZmNlNDRmMTAzZCIsCiAgInByb2ZpbGVOYW1lIiA6ICJieVJPTkFMX1lUIiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2M0MmY0MTJmMDNiZGYzNmI0MjkwMzIwMWIyN2IwNjNlMjI0NDYxOGY3MmQ5MjUzZDU2OWJmZWNiYWIzNjY3YjAiLAogICAgICAibWV0YWRhdGEiIDogewogICAgICAgICJtb2RlbCIgOiAic2xpbSIKICAgICAgfQogICAgfQogIH0KfQ=="),
    STALFOS_4("Stalfos4", DungeonMobType.STALFOS,
            "e735f524-6a65-4de8-8ed7-a9e16072c5f6", "ewogICJ0aW1lc3RhbXAiIDogMTYyNjExODM4NzE0NywKICAicHJvZmlsZUlkIiA6ICI2MWUwNmYxMjFjNTk0ZDdiYjY5MmQ0ZDg1MDljNDdmOSIsCiAgInByb2ZpbGVOYW1lIiA6ICJJdHpfU2NhcmxldHQ3MDIiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzQzNDQ3MTBmODUzNWU3NjdlOTEzNWYzYWIzNmUzYTY2YTY1NzAzY2NmN2RiNTFmNDBmMDVlZDZhNmZhMmYxZCIsCiAgICAgICJtZXRhZGF0YSIgOiB7CiAgICAgICAgIm1vZGVsIiA6ICJzbGltIgogICAgICB9CiAgICB9CiAgfQp9",
            "a5d52e03-d7f3-4f31-a80c-a17108f8a9e0", "ewogICJ0aW1lc3RhbXAiIDogMTYyNjExODQxMzU0MSwKICAicHJvZmlsZUlkIiA6ICJmNThkZWJkNTlmNTA0MjIyOGY2MDIyMjExZDRjMTQwYyIsCiAgInByb2ZpbGVOYW1lIiA6ICJ1bnZlbnRpdmV0YWxlbnQiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNmU0MjYwNTY4MWMyN2E0ODg4MzQzMTNiMWUyYTc0YWRiZGEyZDU2NjE4OGU2OTgwMDdjYTMwZjYwYmNmNmQ3ZCIsCiAgICAgICJtZXRhZGF0YSIgOiB7CiAgICAgICAgIm1vZGVsIiA6ICJzbGltIgogICAgICB9CiAgICB9CiAgfQp9"),
    STALFOS_5("Stalfos5", DungeonMobType.STALFOS,
            "e8e227cf-5c6d-45ae-8454-a54f2304b0a2", "ewogICJ0aW1lc3RhbXAiIDogMTYyNjExODQzMjU5NSwKICAicHJvZmlsZUlkIiA6ICJiYjdjY2E3MTA0MzQ0NDEyOGQzMDg5ZTEzYmRmYWI1OSIsCiAgInByb2ZpbGVOYW1lIiA6ICJsYXVyZW5jaW8zMDMiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmVlMDY3MjdlM2M2NzdjNGE4NDBjZDFjMzE4MmRiMzQ0MDQwOTkyMTZjZDQwYTU3ZmYyM2RhYjBjYTc4MTdjNiIsCiAgICAgICJtZXRhZGF0YSIgOiB7CiAgICAgICAgIm1vZGVsIiA6ICJzbGltIgogICAgICB9CiAgICB9CiAgfQp9",
            "5e7dfc0e-8bd6-428d-8784-0f513e1d3041", "ewogICJ0aW1lc3RhbXAiIDogMTYyNjExODQ1NjQ2OCwKICAicHJvZmlsZUlkIiA6ICI3MzgyZGRmYmU0ODU0NTVjODI1ZjkwMGY4OGZkMzJmOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJJb3lhbCIsCiAgInNpZ25hdHVyZVJlcXVpcmVkIiA6IHRydWUsCiAgInRleHR1cmVzIiA6IHsKICAgICJTS0lOIiA6IHsKICAgICAgInVybCIgOiAiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8yM2E4NGFhMGE0ODJkOTU1ZTUwNTAyNGM1ZTMyYWJhNjM2YTVhZTEzZThiMDkyNTAyNGMwMGFjZmFiNTk4MDY4IiwKICAgICAgIm1ldGFkYXRhIiA6IHsKICAgICAgICAibW9kZWwiIDogInNsaW0iCiAgICAgIH0KICAgIH0KICB9Cn0="),
    DINOLFOS_1("Dinolfos1", DungeonMobType.DINOLFOS,
            "804d2f41-5ab6-4a0b-8c41-f8013aab2c88", "ewogICJ0aW1lc3RhbXAiIDogMTYyNzMzNzQ5MTk0NywKICAicHJvZmlsZUlkIiA6ICJhNzdkNmQ2YmFjOWE0NzY3YTFhNzU1NjYxOTllYmY5MiIsCiAgInByb2ZpbGVOYW1lIiA6ICIwOEJFRDUiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTEyMjE3MDFmMWRkNWQ3NTI1NDhiZmJhMDlkZjY1NTRiYjk3OTQ1M2FkMjE1MWJlOTU2MzM0ZTc4MzAwOGVkOCIsCiAgICAgICJtZXRhZGF0YSIgOiB7CiAgICAgICAgIm1vZGVsIiA6ICJzbGltIgogICAgICB9CiAgICB9CiAgfQp9",
            "015e6237-4549-425a-ac0b-560e2e5e5c4e", "ewogICJ0aW1lc3RhbXAiIDogMTYyNzMzNzU1Mzg0OCwKICAicHJvZmlsZUlkIiA6ICJiN2ZkYmU2N2NkMDA0NjgzYjlmYTllM2UxNzczODI1NCIsCiAgInByb2ZpbGVOYW1lIiA6ICJDVUNGTDE0IiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2U4YTcwY2I0MzE3MTRjMWYwZTlmN2IzNzA1YmI3YzA1ZWNkMTczYTNjMmYwOTc2ODY5YmEwZjZlZTE1NzdmODAiLAogICAgICAibWV0YWRhdGEiIDogewogICAgICAgICJtb2RlbCIgOiAic2xpbSIKICAgICAgfQogICAgfQogIH0KfQ=="),
    ;


    private UUID headId;
    private ItemStack head;
    private UUID hurtId;
    private ItemStack headHurt;
    private DungeonMobType dungeonMobType;

    MobHead(String profile, DungeonMobType dungeonMobType, String headIdString, String headUrl, String hurtIdString, String hurtUrl) {
        this.dungeonMobType = dungeonMobType;
        headId = UUID.fromString(headIdString);
        head = new ItemStack(Material.PLAYER_HEAD);
        HeadUtil.assignTexture(head, headUrl, profile, headId);

        hurtId = UUID.fromString(hurtIdString);
        headHurt = new ItemStack(Material.PLAYER_HEAD);
        HeadUtil.assignTexture(headHurt, hurtUrl, profile, hurtId);
    }

    private static Map<UUID, MobHead> idToHead = new HashMap<>();

    public static void init() {
        for (MobHead mobHead : values()) {
            idToHead.put(mobHead.headId, mobHead);
            idToHead.put(mobHead.hurtId, mobHead);
        }
    }

    public static boolean isMobHead(UUID id) {
        return idToHead.containsKey(id);
    }

    public static MobHead getMobHead(UUID id) {
        return idToHead.get(id);
    }

    private static MobHead[] stalfosHeads = new MobHead[]{MobHead.STALFOS_1, MobHead.STALFOS_2, MobHead.STALFOS_3, MobHead.STALFOS_4, MobHead.STALFOS_5};

    public static MobHead getStalfosHead() {
        return stalfosHeads[(int) (Math.random() * stalfosHeads.length)];
    }

    public ItemStack getHead() {
        return head.clone();
    }

    public ItemStack getHeadHurt() {
        return headHurt.clone();
    }

    public DungeonMobType getMobType() {
        return dungeonMobType;
    }


}
