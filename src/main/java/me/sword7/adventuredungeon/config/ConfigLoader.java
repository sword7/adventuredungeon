package me.sword7.adventuredungeon.config;

import com.google.common.collect.ImmutableList;
import me.sword7.adventuredungeon.AdventureDungeon;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class ConfigLoader {

    private static final String pluginFolder = "AdventureDungeon";

    private static ImmutableList<String> configs = new ImmutableList.Builder<String>()
            .add("config")
            .build();

    private static ImmutableList<String> languages = new ImmutableList.Builder<String>()
            .add("en")
            .build();

    public static void load() {
        try {
            for (String config : configs) {
                File target = new File("plugins/" + pluginFolder, config + ".yml");
                if (!target.exists()) {
                    load("config/" + config + ".yml", target);
                }
            }
            for (String lang : languages) {
                File target = new File("plugins/" + pluginFolder + "/locale", lang + ".yml");
                if (!target.exists()) {
                    load("locale/" + lang + ".yml", target);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void load(String resource, File file) throws IOException {
        file.getParentFile().mkdirs();
        InputStream in = AdventureDungeon.getPlugin().getResource(resource);
        Files.copy(in, file.toPath());
        in.close();
    }


}
