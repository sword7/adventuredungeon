package me.sword7.adventuredungeon.config;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Language {

    PLUGIN_DESCRIPTION("Plugin - description", "Adds procedurally generated dungeons"),
    PLUGIN_READ_MORE("Plugin - read more", "Read more on the [link]."),
    PLUGIN_WIKI("Plugin - wiki", "Adventure Dungeon wiki"),
    PLUGIN_OPTIONS("Plugin - options", "Options"),
    PLUGIN_AUTHOR("Plugin - author", "Author"),
    PLUGIN_VERSION("Plugin - version", "Version"),

    LABEL_COMMANDS("Label - commands", "Commands"),

    HELP_ADVENTUREDUNGEON_HELP("Help - adventuredungeon help", "show plugin commands"),
    HELP_ADVENTUREDUNGEON_INFO("Help - adventuredungeon info", "get plugin information"),
    HELP_DGEN("Help - dgen", "set dungeon spawn rate"),

    WARN_NOT_PERMITTED("Warn - no permission", "You do not have permission for this command."),
    WARN_UNKNOWN_RATE("Warn - unknown rate", "Unrecognized rate value"),
    WARN_INVALID_WORLD("Warn - invalid world", "Invalid world environment"),

    SUCCESS_WRITE_DEED("Success - write deed", "You wrote %amount% plot deeds."),
    SUCCESS_SET_RATE("Success - set rate", "Spawn rate for %world% set to %rate%"),

    STATUS_CHECK_RATE("Status - check rate", "Spawn rate for %world% is %rate%"),

    ;

    public static void load() {
        File file = new File("plugins/AdventureDungeon/locale", PluginConfig.getLanguageFile() + ".yml");
        if (file.exists()) {
            FileConfiguration config = YamlConfiguration.loadConfiguration(file);
            try {
                for (Language message : Language.values()) {
                    MessageSetting setting = message.getMessageSetting();
                    if (config.contains(setting.getLabel())) {
                        setting.setMessage(config.getString(setting.getLabel(), setting.getMessage()));
                    } else {
                        config.set(setting.getLabel(), setting.getMessage());
                    }
                }
                config.save(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void reload() {
        load();
    }

    public class MessageSetting {

        private String label;
        private String message;

        public MessageSetting(String label, String message) {
            this.label = label;
            this.message = message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getLabel() {
            return label;
        }

        public String getMessage() {
            return message;
        }

    }

    private MessageSetting messageSetting;

    Language(String messageSetting, String messageDefault) {
        this.messageSetting = new MessageSetting(messageSetting, messageDefault);
    }

    public MessageSetting getMessageSetting() {
        return messageSetting;
    }

    @Override
    public String toString() {
        return get();
    }

    private String get() {
        return ChatColor.translateAlternateColorCodes('&', messageSetting.getMessage());
    }

    public String fromWorldAndRate(String world, String rate) {
        return get().replaceAll("%world%", world).replaceAll("%rate%", rate);
    }

    public TextComponent getWithLink(ChatColor baseColor, String name, String link) {
        String string = get();
        TextComponent linkComponent = new TextComponent(name);
        linkComponent.setColor(ChatColor.AQUA.asBungee());
        linkComponent.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, link));
        TextComponent base = new TextComponent("");
        base.setColor(baseColor.asBungee());
        String[] subs = string.split("\\[link\\]");
        for (int i = 0; i < subs.length; i++) {
            TextComponent textComponent = new TextComponent(subs[i]);
            textComponent.setColor(baseColor.asBungee());
            base.addExtra(textComponent);
            if (i < subs.length - 1 || string.endsWith("[link]")) {
                base.addExtra(linkComponent);
            }
        }
        return base;
    }

}
