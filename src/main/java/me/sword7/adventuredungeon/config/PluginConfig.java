package me.sword7.adventuredungeon.config;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class PluginConfig {

    private static String languageFileField = "Language file";
    private static String languageFile = "en";

    public PluginConfig() {
        load();
    }

    private static void load() {
        File file = new File("plugins/AdventureDungeon", "config.yml");
        if (file.exists()) {
            try {
                FileConfiguration config = YamlConfiguration.loadConfiguration(file);
                languageFile = config.getString(languageFileField, languageFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void reload() {
        load();
    }

    public static String getLanguageFile() {
        return languageFile;
    }


}
