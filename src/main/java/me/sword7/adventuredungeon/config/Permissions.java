package me.sword7.adventuredungeon.config;

import org.bukkit.command.CommandSender;

public class Permissions {

    private static final String GEN_PERM = "adventure.gen";

    public static boolean canGenerate(CommandSender sender) {
        return hasPermission(sender, GEN_PERM);
    }


    private static boolean hasPermission(CommandSender sender, String permString) {
        return sender.hasPermission("adventure.*") || sender.hasPermission(permString);
    }


}
