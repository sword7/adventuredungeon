package me.sword7.adventuredungeon.dungeons.lizardbase;

import me.sword7.adventuredungeon.dungeon.container.ContainerType;
import me.sword7.adventuredungeon.lootpool.LootPool;
import me.sword7.adventuredungeon.lootpool.item.ILootItem;

import java.util.Random;

public class LizardBaseLootPool extends LootPool {

    @Override
    public double getContainerDropChance(ContainerType containerType) {
        return 0.33;
    }

    @Override
    public ILootItem selectContainer(ContainerType containerType, Random random) {
        return null;
    }

}
