package me.sword7.adventuredungeon.dungeons.lizardbase;

import me.sword7.adventuredungeon.dungeon.Dungeon;
import me.sword7.adventuredungeon.dungeon.DungeonCache;
import me.sword7.adventuredungeon.generate.IConstructor;
import me.sword7.adventuredungeon.lootpool.LootTag;
import me.sword7.adventuredungeon.mob.DungeonMobType;
import me.sword7.adventuredungeon.util.Util;
import me.sword7.adventuredungeon.util.math.Cardinal;
import me.sword7.adventuredungeon.util.math.IBound;
import me.sword7.adventuredungeon.util.math.Point;
import me.sword7.adventuredungeon.util.math.Point2D;

import java.util.*;

public class LizardBaseConstructor implements IConstructor {

    private UUID worldId;
    private Point entrance;
    private Cardinal direction;
    private List<IBound> bounds;
    private List<Point> chestLocations;
    private List<Point> spawnerLocation;
    private Point center = new Point(0, 0, 0);
    private int radius = 0;

    public LizardBaseConstructor(UUID worldId, Point entrance, Cardinal direction, List<IBound> bounds, List<Point> chestLocations, List<Point> spawnerLocations) {
        this.worldId = worldId;
        this.entrance = entrance;
        this.direction = direction;
        this.bounds = bounds;
        this.chestLocations = chestLocations;
        this.spawnerLocation = spawnerLocations;
    }

    @Override
    public Dungeon construct() {
        UUID id = UUID.randomUUID();
        String name = "The Festering Pit";
        calculateCenterAndRadius(bounds);

        Set<Point2D> gridSet = new HashSet<>();
        for (IBound bound : bounds) {
            gridSet.addAll(Util.getBoundGrids(bound, DungeonCache.DUNGEON_GRID_LENGTH));
        }

        List<Point2D> grids = new ArrayList<>();
        grids.addAll(gridSet);
        gridSet.clear();

        Map<Point, List<LootTag>> chests = new HashMap<>();
        for (Point p : chestLocations) {
            chests.put(p, Collections.EMPTY_LIST);
        }
        chestLocations.clear();

        Map<Point, DungeonMobType> spawners = new HashMap<>();
        for (Point p : spawnerLocation) {
            spawners.put(p, DungeonMobType.DINOLFOS);
        }
        this.spawnerLocation.clear();


        return new LizardBase(id, name, worldId, entrance, direction, center, radius, grids, chests, spawners, new HashSet<>(), bounds);
    }

    private void calculateCenterAndRadius(List<IBound> bounds) {
        if (bounds.size() > 0) {
            IBound fB = bounds.get(0);
            Point min = fB.getMin();
            int minX = min.getX();
            int minY = min.getY();
            int minZ = min.getZ();
            Point max = fB.getMax();
            int maxX = max.getX();
            int maxY = max.getY();
            int maxZ = max.getZ();
            for (int i = 1; i < bounds.size(); i++) {
                IBound b = bounds.get(i);
                min = b.getMin();
                if (min.getX() < minX) minX = min.getX();
                if (min.getY() < minY) minY = min.getY();
                if (min.getZ() < minZ) minZ = min.getZ();
                max = b.getMax();
                if (max.getX() > maxX) maxX = max.getX();
                if (max.getY() > maxY) maxY = max.getY();
                if (max.getZ() > maxZ) maxZ = max.getZ();
            }

            int centerX = minX + ((maxX - minX) / 2);
            int centerY = minY + ((maxY - minY) / 2);
            int centerZ = minZ + ((maxZ - minZ) / 2);

            this.center = new Point(centerX, centerY, centerZ);

            int dimX = maxX - minX + 1;
            int dimY = maxY - minY + 1;
            int dimZ = maxZ - minZ + 1;

            double diagonal = Math.sqrt((dimX * dimX) + (dimY * dimY) + (dimZ * dimZ));
            double radiusDouble = diagonal / 2.0;
            this.radius = (int) Math.ceil(radiusDouble);

        }

    }


}
