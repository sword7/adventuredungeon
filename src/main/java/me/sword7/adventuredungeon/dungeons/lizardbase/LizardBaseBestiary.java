package me.sword7.adventuredungeon.dungeons.lizardbase;

import me.sword7.adventuredungeon.mob.DungeonMobType;
import me.sword7.adventuredungeon.mob.IBestiary;
import org.bukkit.Location;

import java.util.Random;

public class LizardBaseBestiary implements IBestiary {

    @Override
    public void spawn(DungeonMobType mobType, Location location, Random random) {
        mobType.getDungeonMob().spawn(location, random);
    }

    @Override
    public double getAmbientSpawnChance() {
        return 1.0;
    }

    @Override
    public double getOverrideSpawnChance() {
        return 1.0;
    }

    @Override
    public void ambientSpawn(Location location, Random random) {
        DungeonMobType.DINOLFOS.getDungeonMob().spawn(location, random);
    }

}
