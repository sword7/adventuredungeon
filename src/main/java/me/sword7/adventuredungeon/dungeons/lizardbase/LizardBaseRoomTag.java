package me.sword7.adventuredungeon.dungeons.lizardbase;

import me.sword7.adventuredungeon.structure.RoomTag;

public enum LizardBaseRoomTag implements RoomTag {
    ENTRANCE,
}
