package me.sword7.adventuredungeon.dungeons.lizardbase;

import me.sword7.adventuredungeon.dungeon.DungeonTag;
import me.sword7.adventuredungeon.lootpool.LootTag;
import me.sword7.adventuredungeon.lootpool.item.ItemCollection;
import me.sword7.adventuredungeon.lootpool.item.ItemWeightedAmount;
import me.sword7.adventuredungeon.lootpool.loot.FlavoredLoot;
import me.sword7.adventuredungeon.util.weight.WeightedChoice;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Random;

public class LizardBaseLoot extends FlavoredLoot {

    private static ItemCollection leaves;
    private static ItemCollection shrubs;
    private static ItemCollection bricks;
    private static ItemCollection rails;

    public static void init() {

        //LEAVES
        leaves = new ItemCollection();

        ItemWeightedAmount leaf = createWeights(Material.JUNGLE_LEAVES);
        ItemWeightedAmount stick = createWeights(Material.STICK);

        leaves.addItem(new WeightedChoice<>(leaf, 8));
        leaves.addItem(new WeightedChoice<>(stick, 3));

        //SHRUBS
        shrubs = new ItemCollection();

        ItemWeightedAmount coral = createWeights(Material.DEAD_FIRE_CORAL);

        shrubs.addItem(new WeightedChoice<>(coral, 1));

        //BRICKS
        bricks = new ItemCollection();

        ItemWeightedAmount bars = createWeights(Material.IRON_BARS);
        ItemWeightedAmount brick1 = createWeights(Material.BRICK);
        ItemWeightedAmount brick2 = createWeights(Material.NETHER_BRICK);

        bricks.addItem(new WeightedChoice<>(bars, 7));
        bricks.addItem(new WeightedChoice<>(brick1, 2));
        bricks.addItem(new WeightedChoice<>(brick2, 2));

        //RAILS
        rails = new ItemCollection();

        ItemWeightedAmount rail = createWeights(Material.RAIL);

        rails.addItem(new WeightedChoice<>(bars, 7));
        rails.addItem(new WeightedChoice<>(rail, 4));

    }

    private static ItemWeightedAmount createWeights(Material material) {
        ItemWeightedAmount item = new ItemWeightedAmount(new ItemStack(material));
        item.addAmount(1, 15);
        item.addAmount(2, 4);
        item.addAmount(3, 1);
        return item;
    }

    public LizardBaseLoot(ItemCollection treasure) {
        super(treasure);
    }

    public LizardBaseLoot(ItemCollection treasure, double minItem, double maxItem, double minTreasure, double maxTreasure) {
        super(treasure, minItem, maxItem, minTreasure, maxTreasure);
    }

    @Override
    protected ItemCollection selectFlavor(Random rand, List<DungeonTag> dungeonTags, List<LootTag> lootTags) {

        double r = rand.nextDouble();
        if (r < 0.25) {
            return leaves;
        } else if (r < 0.5) {
            return shrubs;
        } else if (r < 0.75) {
            return bricks;
        } else {
            return rails;
        }

    }

}
