package me.sword7.adventuredungeon.dungeons.lizardbase;

import me.sword7.adventuredungeon.dungeons.lizardbase.theme.LizardBaseTheme;
import me.sword7.adventuredungeon.structure.ITheme;
import me.sword7.adventuredungeon.structure.IThemeFactory;
import org.bukkit.block.Biome;

import java.util.Random;

public class LizardBaseThemeFactory implements IThemeFactory {

    @Override
    public ITheme select(Biome biome, Random random) {
        return new LizardBaseTheme();
    }

}
