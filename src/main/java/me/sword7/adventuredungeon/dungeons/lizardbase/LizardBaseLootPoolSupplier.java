package me.sword7.adventuredungeon.dungeons.lizardbase;

import me.sword7.adventuredungeon.dungeons.crypt.CryptLoot;
import me.sword7.adventuredungeon.lootpool.LootPool;
import me.sword7.adventuredungeon.lootpool.LootUtil;
import me.sword7.adventuredungeon.lootpool.item.*;
import me.sword7.adventuredungeon.lootpool.loot.ILoot;
import me.sword7.adventuredungeon.lootpool.loot.LootGroup;
import me.sword7.adventuredungeon.util.weight.WeightedChoice;
import me.sword7.adventuredungeon.util.weight.WeightedRandomizer;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

public class LizardBaseLootPoolSupplier {

    private static LootPool lootPool = new LizardBaseLootPool();

    public static void init() {
        buildPool(lootPool);
    }

    public static LootPool getLootPool() {
        return lootPool;
    }

    private static void buildPool(LootPool pool) {

        //=== ITEM COLLECTIONS ===
        ItemCollection gemsAndMetals = new ItemCollection();
        gemsAndMetals.addItem(LootUtil.DIAMOND, 1);
        gemsAndMetals.addItem(LootUtil.IRON_INGOTS, 9);
        gemsAndMetals.addItem(LootUtil.GOLD_INGOTS, 2);

        ItemCollection ore = new ItemCollection();
        ore.addItem(new ItemRangedAmount(Material.COAL_ORE, 1, 9), 1);
        ore.addItem(new ItemRangedAmount(Material.IRON_ORE, 1, 9), 1);

        ItemCollection anvil = new ItemCollection();
        anvil.addItem(Material.ANVIL, 1);
        anvil.addItem(Material.CHIPPED_ANVIL, 1);
        anvil.addItem(Material.DAMAGED_ANVIL, 1);

        ItemCollection armorItems = getArmorItems();
        ItemCollection weaponItems = getWeapons();

        ItemCollection miscItems = new ItemCollection();
        miscItems.addItem(Material.DISPENSER, 1);
        miscItems.addItem(new ItemRangedAmount(Material.COAL_ORE, 1, 3), 1);
        miscItems.addItem(new ItemRangedAmount(Material.COAL_ORE, 1, 3), 1);
        miscItems.addItem(Material.PISTON, 1);
        miscItems.addItem(Material.IRON_BLOCK, 1);
        miscItems.addItem(Material.TNT, 1);
        miscItems.addItem(Material.OBSIDIAN, 1);
        miscItems.addItem(Material.LADDER, 1);
        miscItems.addItem(new ItemRangedAmount(Material.RAIL, 1, 5), 1);
        miscItems.addItem(Material.COBBLESTONE, 1);
        miscItems.addItem(Material.SMOOTH_STONE, 1);
        miscItems.addItem(Material.POLISHED_ANDESITE, 1);
        miscItems.addItem(Material.BRICKS, 1);
        miscItems.addItem(Material.LEVER, 1);
        miscItems.addItem(Material.STONE_PRESSURE_PLATE, 1);
        miscItems.addItem(Material.ACACIA_TRAPDOOR, 1);
        miscItems.addItem(Material.IRON_BARS, 1);
        miscItems.addItem(Material.STONE_BRICKS, 1);
        miscItems.addItem(Material.CHAIN, 1);
        miscItems.addItem(Material.CHISELED_STONE_BRICKS, 1);
        miscItems.addItem(Material.TRIPWIRE_HOOK, 1);
        miscItems.addItem(anvil, 1);
        miscItems.addItem(Material.LIGHT_WEIGHTED_PRESSURE_PLATE, 1);
        miscItems.addItem(Material.QUARTZ_BRICKS, 1);
        miscItems.addItem(Material.HOPPER, 1);
        miscItems.addItem(new ItemRangedAmount(Material.ACTIVATOR_RAIL, 1, 3), 1);
        miscItems.addItem(Material.DAYLIGHT_DETECTOR, 1);
        miscItems.addItem(Material.DROPPER, 1);
        miscItems.addItem(Material.IRON_TRAPDOOR, 1);
        miscItems.addItem(Material.COAL_BLOCK, 1);
        miscItems.addItem(Material.OBSERVER, 1);
        miscItems.addItem(Material.SLIME_BLOCK, 1);
        miscItems.addItem(Material.LIGHT_GRAY_GLAZED_TERRACOTTA, 1);
        miscItems.addItem(Material.GRAY_GLAZED_TERRACOTTA, 1);
        miscItems.addItem(Material.CYAN_GLAZED_TERRACOTTA, 1);
        miscItems.addItem(Material.TURTLE_EGG, 1);
        miscItems.addItem(Material.DEAD_FIRE_CORAL, 1);
        miscItems.addItem(Material.SCAFFOLDING, 1);
        miscItems.addItem(Material.IRON_DOOR, 1);
        miscItems.addItem(Material.REPEATER, 1);
        miscItems.addItem(Material.COMPARATOR, 1);
        miscItems.addItem(Material.TURTLE_HELMET, 1);
        miscItems.addItem(Material.SCUTE, 1);
        miscItems.addItem(Material.ARROW, 1);
        miscItems.addItem(Material.IRON_PICKAXE, 1);
        miscItems.addItem(Material.IRON_SHOVEL, 1);
        miscItems.addItem(Material.IRON_HOE, 1);
        miscItems.addItem(Material.BLAST_FURNACE, 1);
        miscItems.addItem(Material.SMITHING_TABLE, 1);
        miscItems.addItem(Material.GRINDSTONE, 1);
        miscItems.addItem(Material.STONECUTTER, 1);
        miscItems.addItem(Material.LANTERN, 1);
        miscItems.addItem(Material.FLINT, 1);
        miscItems.addItem(Material.FLINT_AND_STEEL, 1);
        miscItems.addItem(Material.GUNPOWDER, 1);
        miscItems.addItem(new ItemRangedAmount(Material.WHEAT, 1, 5), 1);
        miscItems.addItem(new ItemRangedAmount(Material.WHEAT_SEEDS, 1, 7), 1);
        miscItems.addItem(Material.STICK, 1);
        miscItems.addItem(Material.JUNGLE_LOG, 1);
        miscItems.addItem(Material.BUCKET, 1);
        miscItems.addItem(Material.MINECART, 1);
        miscItems.addItem(Material.REDSTONE, 1);
        miscItems.addItem(Material.PORKCHOP, 1);
        miscItems.addItem(Material.BEEF, 1);
        miscItems.addItem(Material.MUTTON, 1);
        miscItems.addItem(Material.CHICKEN, 1);
        miscItems.addItem(Material.RABBIT, 1);
        miscItems.addItem(Material.LEATHER, 1);
        miscItems.addItem(Material.BRICK, 1);
        miscItems.addItem(Material.CLAY, 1);
        miscItems.addItem(Material.PAPER, 1);
        miscItems.addItem(Material.BOOK, 1);
        miscItems.addItem(Material.FURNACE_MINECART, 1);
        miscItems.addItem(Material.COMPASS, 1);
        miscItems.addItem(Material.BONE, 1);
        miscItems.addItem(Material.SHEARS, 1);
        miscItems.addItem(Material.SPIDER_EYE, 1);
        miscItems.addItem(Material.QUARTZ, 1);
        miscItems.addItem(Material.QUARTZ, 1);
        miscItems.addItem(Material.NETHER_BRICK, 1);
        miscItems.addItem(Material.PRISMARINE_SHARD, 1);
        miscItems.addItem(Material.PRISMARINE_CRYSTALS, 1);
        miscItems.addItem(Material.RABBIT_HIDE, 1);
        miscItems.addItem(Material.RABBIT_FOOT, 1);
        miscItems.addItem(Material.IRON_HORSE_ARMOR, 1);
        miscItems.addItem(Material.LEAD, 1);
        miscItems.addItem(Material.DRAGON_BREATH, 1);
        miscItems.addItem(Material.SHIELD, 1);
        miscItems.addItem(Material.NAME_TAG, 1);
        miscItems.addItem(Material.IRON_INGOT, 1);
        miscItems.addItem(Material.PHANTOM_MEMBRANE, 1);
        miscItems.addItem(Material.CROSSBOW, 1);
        miscItems.addItem(Material.IRON_SWORD, 1);

        WeightedRandomizer<Enchantment> enchants = new WeightedRandomizer<>();
        enchants.add(new WeightedChoice<>(Enchantment.PROTECTION_ENVIRONMENTAL, 1));
        enchants.add(new WeightedChoice<>(Enchantment.THORNS, 1));
        enchants.add(new WeightedChoice<>(Enchantment.DAMAGE_ALL, 1));
        enchants.add(new WeightedChoice<>(Enchantment.DAMAGE_UNDEAD, 1));
        enchants.add(new WeightedChoice<>(Enchantment.LOOT_BONUS_MOBS, 1));
        enchants.add(new WeightedChoice<>(Enchantment.SWEEPING_EDGE, 1));
        enchants.add(new WeightedChoice<>(Enchantment.DIG_SPEED, 1));
        enchants.add(new WeightedChoice<>(Enchantment.DURABILITY, 1));
        enchants.add(new WeightedChoice<>(Enchantment.LOOT_BONUS_BLOCKS, 1));
        enchants.add(new WeightedChoice<>(Enchantment.MULTISHOT, 1));
        enchants.add(new WeightedChoice<>(Enchantment.QUICK_CHARGE, 1));
        enchants.add(new WeightedChoice<>(Enchantment.PIERCING, 1));
        ILootItem enchantedBook = new EnchantedBook(enchants);

        //=== CHEST TYPES ===

        //MAIN
        ItemCollection mainCollection = new ItemCollection();
        mainCollection.addItem(new WeightedChoice<>(gemsAndMetals, 2));
        mainCollection.addItem(new WeightedChoice<>(enchantedBook, 2));
        mainCollection.addItem(new WeightedChoice<>(armorItems, 1));
        mainCollection.addItem(new WeightedChoice<>(weaponItems, 1));
        mainCollection.addItem(new WeightedChoice<>(miscItems, 9));

        //TREASURE
        ItemCollection extrasCollection = new ItemCollection();
        extrasCollection.addItem(new WeightedChoice<>(miscItems, 5));
        extrasCollection.addItem(new WeightedChoice<>(enchantedBook, 2));
        extrasCollection.addItem(new WeightedChoice<>(armorItems, 1));
        extrasCollection.addItem(new WeightedChoice<>(weaponItems, 1));

        ItemCollection metals = new ItemCollection();
        metals.addItem(new WeightedChoice<>(LootUtil.GOLD_INGOTS, 5));
        metals.addItem(new WeightedChoice<>(LootUtil.IRON_INGOTS, 13));

        ItemCollection ironHeavy = new ItemCollection();
        ironHeavy.addItem(new WeightedChoice<>(LootUtil.DIAMOND, 2));
        ironHeavy.addItem(new WeightedChoice<>(LootUtil.GOLD_INGOTS, 4));
        ironHeavy.addItem(new WeightedChoice<>(LootUtil.IRON_INGOTS, 16));

        ItemCollection ironDiamond = new ItemCollection();
        ironDiamond.addItem(new WeightedChoice<>(LootUtil.DIAMOND, 2));
        ironDiamond.addItem(new WeightedChoice<>(LootUtil.IRON_INGOTS, 16));

        ItemCollection treasureCollection = new ItemCollection();
        treasureCollection.addItem(new WeightedChoice<>(gemsAndMetals, 48));
        treasureCollection.addItem(new WeightedChoice<>(extrasCollection, 3));
        ILoot treasure1 = new LizardBaseLoot(treasureCollection, 0.4, 0.98, 0.6, 0.95);

        treasureCollection = new ItemCollection();
        treasureCollection.addItem(new WeightedChoice<>(ironDiamond, 48));
        treasureCollection.addItem(new WeightedChoice<>(extrasCollection, 3));
        ILoot treasure2 = new LizardBaseLoot(treasureCollection, 0.4, 0.98, 0.6, 0.95);

        treasureCollection = new ItemCollection();
        treasureCollection.addItem(new WeightedChoice<>(metals, 48));
        treasureCollection.addItem(new WeightedChoice<>(extrasCollection, 3));
        ILoot treasure3 = new LizardBaseLoot(treasureCollection, 0.4, 0.98, 0.6, 0.95);

        treasureCollection = new ItemCollection();
        treasureCollection.addItem(new WeightedChoice<>(ironHeavy, 48));
        treasureCollection.addItem(new WeightedChoice<>(extrasCollection, 3));
        ILoot treasure4 = new LizardBaseLoot(treasureCollection, 0.4, 0.98, 0.6, 0.95);

        LootGroup treasure = new LootGroup();
        treasure.add(new WeightedChoice<>(treasure1, 1));
        treasure.add(new WeightedChoice<>(treasure2, 1));
        treasure.add(new WeightedChoice<>(treasure3, 1));
        treasure.add(new WeightedChoice<>(treasure4, 1));

        //ARMORY

        ItemCollection armor = new ItemCollection();
        armor.addItem(new WeightedChoice<>(gemsAndMetals, 4));
        armor.addItem(new WeightedChoice<>(enchantedBook, 2));
        armor.addItem(new WeightedChoice<>(weaponItems, 2));
        armor.addItem(new WeightedChoice<>(miscItems, 4));
        armor.addItem(new WeightedChoice<>(armorItems, 30));

        //WEAPONRY

        ItemCollection weapons = new ItemCollection();
        weapons.addItem(new WeightedChoice<>(gemsAndMetals, 4));
        weapons.addItem(new WeightedChoice<>(enchantedBook, 2));
        weapons.addItem(new WeightedChoice<>(armorItems, 2));
        weapons.addItem(new WeightedChoice<>(miscItems, 4));
        weapons.addItem(new WeightedChoice<>(weaponItems, 30));

        //FOOD


        pool.add(new WeightedChoice<>(new LizardBaseLoot(mainCollection), 5));
        pool.add(new WeightedChoice<>(treasure, 2));
        pool.add(new WeightedChoice<>(new LizardBaseLoot(armor, 0.45, 0.75, 0.33, 0.66), 1));
        pool.add(new WeightedChoice<>(new LizardBaseLoot(weapons, 0.45, 0.75, 0.33, 0.66), 1));
    }


    private static ItemCollection getArmorItems() {
        WeightedRandomizer<Enchantment> helmetEnchants = new WeightedRandomizer<>();
        helmetEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_ENVIRONMENTAL, 10));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_PROJECTILE, 4));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.THORNS, 4));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.DURABILITY, 8));

        WeightedRandomizer<Enchantment> middleWareEnchants = new WeightedRandomizer<>();
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_ENVIRONMENTAL, 10));
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_PROJECTILE, 4));
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.THORNS, 4));
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.DURABILITY, 8));

        WeightedRandomizer<Enchantment> bootEnchants = new WeightedRandomizer<>();
        bootEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_ENVIRONMENTAL, 10));
        bootEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_PROJECTILE, 4));
        bootEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_FALL, 8));
        bootEnchants.add(new WeightedChoice<>(Enchantment.DURABILITY, 8));
        bootEnchants.add(new WeightedChoice<>(Enchantment.THORNS, 4));
        bootEnchants.add(new WeightedChoice<>(Enchantment.DEPTH_STRIDER, 2));

        ItemCollection enchantedDiamondArmor = new ItemCollection();
        enchantedDiamondArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.DIAMOND_HELMET, helmetEnchants), 1));
        enchantedDiamondArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.DIAMOND_CHESTPLATE, middleWareEnchants), 1));
        enchantedDiamondArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.DIAMOND_LEGGINGS, middleWareEnchants), 1));
        enchantedDiamondArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.DIAMOND_BOOTS, bootEnchants), 1));

        ItemCollection diamondArmor = new ItemCollection();
        diamondArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.DIAMOND_HELMET), 1));
        diamondArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.DIAMOND_CHESTPLATE), 1));
        diamondArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.DIAMOND_LEGGINGS), 1));
        diamondArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.DIAMOND_BOOTS), 1));

        ItemCollection enchantedIronArmor = new ItemCollection();
        enchantedIronArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.IRON_HELMET, helmetEnchants), 1));
        enchantedIronArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.IRON_CHESTPLATE, middleWareEnchants), 1));
        enchantedIronArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.IRON_LEGGINGS, middleWareEnchants), 1));
        enchantedIronArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.IRON_BOOTS, bootEnchants), 1));

        ItemCollection ironArmor = new ItemCollection();
        ironArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.IRON_HELMET), 1));
        ironArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.IRON_CHESTPLATE), 1));
        ironArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.IRON_LEGGINGS), 1));
        ironArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.IRON_BOOTS), 1));

        ItemCollection enchantedChainArmor = new ItemCollection();
        enchantedChainArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.CHAINMAIL_HELMET, helmetEnchants), 1));
        enchantedChainArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.CHAINMAIL_CHESTPLATE, middleWareEnchants), 1));
        enchantedChainArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.CHAINMAIL_LEGGINGS, middleWareEnchants), 1));
        enchantedChainArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.CHAINMAIL_BOOTS, bootEnchants), 1));

        ItemCollection chainArmor = new ItemCollection();
        chainArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.CHAINMAIL_HELMET), 1));
        chainArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.CHAINMAIL_CHESTPLATE), 1));
        chainArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.CHAINMAIL_LEGGINGS), 1));
        chainArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.CHAINMAIL_BOOTS), 1));

        ItemCollection armorItems = new ItemCollection();
        armorItems.addItem(new WeightedChoice<>(enchantedDiamondArmor, 1));
        armorItems.addItem(new WeightedChoice<>(diamondArmor, 3));
        armorItems.addItem(new WeightedChoice<>(enchantedIronArmor, 2));
        armorItems.addItem(new WeightedChoice<>(ironArmor, 6));
        armorItems.addItem(new WeightedChoice<>(enchantedChainArmor, 1));
        armorItems.addItem(new WeightedChoice<>(chainArmor, 3));

        return armorItems;
    }


    private static ItemCollection getWeapons() {

        WeightedRandomizer<Enchantment> swordEnchantments = new WeightedRandomizer<>();
        swordEnchantments.add(new WeightedChoice<>(Enchantment.DAMAGE_ALL, 10));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.DURABILITY, 6));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.DAMAGE_UNDEAD, 4));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.KNOCKBACK, 4));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.LOOT_BONUS_MOBS, 4));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.SWEEPING_EDGE, 4));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.FIRE_ASPECT, 4));

        WeightedRandomizer<Enchantment> axeEnchantments = new WeightedRandomizer<>();
        axeEnchantments.add(new WeightedChoice<>(Enchantment.DAMAGE_ALL, 10));
        axeEnchantments.add(new WeightedChoice<>(Enchantment.DURABILITY, 6));
        axeEnchantments.add(new WeightedChoice<>(Enchantment.DAMAGE_UNDEAD, 4));

        WeightedRandomizer<Enchantment> crossBowEnchantments = new WeightedRandomizer<>();
        crossBowEnchantments.add(new WeightedChoice<>(Enchantment.PIERCING, 6));
        crossBowEnchantments.add(new WeightedChoice<>(Enchantment.MULTISHOT, 4));
        crossBowEnchantments.add(new WeightedChoice<>(Enchantment.QUICK_CHARGE, 4));
        crossBowEnchantments.add(new WeightedChoice<>(Enchantment.DURABILITY, 4));

        LootItem enchantedDiamondSword = new EnchantedItem(Material.DIAMOND_SWORD, swordEnchantments);
        LootItem diamondSword = new ItemSingleton(Material.DIAMOND_SWORD);

        LootItem enchantedDiamondAxe = new EnchantedItem(Material.DIAMOND_AXE, axeEnchantments);
        LootItem diamondAxe = new ItemSingleton(Material.DIAMOND_AXE);

        LootItem enchantedIronSword = new EnchantedItem(Material.IRON_SWORD, swordEnchantments);
        LootItem ironSword = new ItemSingleton(Material.IRON_SWORD);

        LootItem enchantedIronAxe = new EnchantedItem(Material.IRON_AXE, axeEnchantments);
        LootItem ironAxe = new ItemSingleton(Material.IRON_AXE);

        LootItem enchantedCrossBow = new EnchantedItem(Material.CROSSBOW, crossBowEnchantments);
        LootItem crossBow = new ItemSingleton(Material.CROSSBOW);

        ItemCollection weaponItems = new ItemCollection();
        weaponItems.addItem(new WeightedChoice<>(enchantedDiamondSword, 3));
        weaponItems.addItem(new WeightedChoice<>(diamondSword, 9));
        weaponItems.addItem(new WeightedChoice<>(enchantedDiamondAxe, 1));
        weaponItems.addItem(new WeightedChoice<>(diamondAxe, 3));


        weaponItems.addItem(new WeightedChoice<>(enchantedIronSword, 9));
        weaponItems.addItem(new WeightedChoice<>(ironSword, 27));
        weaponItems.addItem(new WeightedChoice<>(enchantedIronAxe, 3));
        weaponItems.addItem(new WeightedChoice<>(ironAxe, 9));

        weaponItems.addItem(new WeightedChoice<>(enchantedCrossBow, 4));
        weaponItems.addItem(new WeightedChoice<>(crossBow, 12));
        weaponItems.addItem(new WeightedChoice<>(LootUtil.ARROWS, 32));

        //sword/axe type
        //bow/arrow type
        //mix

        return weaponItems;
    }


    private static ItemCollection getPotionArrows() {
        return new ItemCollection();
    }

    private static ItemCollection getPotions() {
        return new ItemCollection();
    }

    private static ItemCollection getScrolls() {
        //books
        //paper
        //maps
        //banner patterns
        //enchanted books
        return new ItemCollection();
    }

    private static ItemCollection getStorage() {
        //food
        //**different flavor
        //books/scrolls
        //potions
        //misc items
        return new ItemCollection();
    }

    private static ItemCollection getCeremony() {
        //redstone torches
        //banners
        //pattens
        //gems
        //gold
        //armor/weapons
        //seeds
        //carpets
        //remains
        return new ItemCollection();
    }


}
