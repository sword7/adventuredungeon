package me.sword7.adventuredungeon.dungeons.lizardbase;

import me.sword7.adventuredungeon.dungeon.DungeonType;
import me.sword7.adventuredungeon.structure.Decoration;
import me.sword7.adventuredungeon.structure.IFrame;
import me.sword7.adventuredungeon.structure.StructFlatFile;
import me.sword7.adventuredungeon.structure.Structure;
import me.sword7.adventuredungeon.util.math.Cardinal;
import me.sword7.adventuredungeon.util.math.Point;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public enum LizardBaseFrame implements IFrame {

    sN,
    sNE,
    sNS,
    sNEW,
    sNSEW,
    hNE,
    hNS,
    hNEW,
    hNSEW,
    rSW,
    rNS,
    rNSE,
    rNSSE,
    rNEW,
    stSE,
    stNSEEW,

    ;

    private Point dimensions;
    private int doors;
    private Structure structure;
    private Map<Point, Set<Cardinal>> gridCoordToEntrances = new HashMap<>();
    private Decoration[] decorations;

    public static void init() {

        for (LizardBaseFrame frame : values()) {
            String frameString = frame.toString();
            if (frameString.startsWith("s") || frameString.startsWith("h")) {
                frame.dimensions = new Point(1, 1, 1);
            } else if (frameString.startsWith("r")) {
                frame.dimensions = new Point(2, 1, 1);
            }
            frame.doors = (int) frameString.chars().filter(c -> Character.isUpperCase(c)).count();
            frame.structure = StructFlatFile.fetch("lizardbase/frame", frameString);
            frame.decorations = Decoration.load(frame);

        }

        sN.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N));
        sNE.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.E));
        hNE.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.E));
        sNS.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S));
        hNS.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S));
        sNEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.E, Cardinal.W));
        hNEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.E, Cardinal.W));
        sNSEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S, Cardinal.E, Cardinal.W));
        hNSEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S, Cardinal.E, Cardinal.W));

        rSW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.W));
        rSW.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.S));
        rNS.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N));
        rNS.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.S));
        rNSE.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S));
        rNSE.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.E));
        rNSSE.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S));
        rNSSE.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.E, Cardinal.S));
        rNEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.W));
        rNEW.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.E));

        stSE.dimensions = new Point(1, 2, 1);
        stSE.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.E));
        stSE.gridCoordToEntrances.put(new Point(0, 1, 0), Cardinal.buildSet(Cardinal.S));

        stNSEEW.dimensions = new Point(1, 3, 1);
        stNSEEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N));
        stNSEEW.gridCoordToEntrances.put(new Point(0, 1, 0), Cardinal.buildSet(Cardinal.E, Cardinal.W));
        stNSEEW.gridCoordToEntrances.put(new Point(0, 2, 0), Cardinal.buildSet(Cardinal.S, Cardinal.E));


    }

    @Override
    public Point getDimensions() {
        return dimensions;
    }

    @Override
    public int getDoors() {
        return doors;
    }

    @Override
    public Structure getStructure() {
        return structure;
    }

    @Override
    public Map<Point, Set<Cardinal>> getGridCoordToEntrances() {
        return gridCoordToEntrances;
    }

    @Override
    public Decoration selectDecoration(Random random) {
        return decorations.length > 0 ? decorations[(int) (Math.random() * decorations.length)] : null;
    }

    @Override
    public DungeonType getDungeonType() {
        return DungeonType.LIZARD_BASE;
    }


}
