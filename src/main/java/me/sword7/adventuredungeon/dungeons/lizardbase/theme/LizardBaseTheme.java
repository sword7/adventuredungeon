package me.sword7.adventuredungeon.dungeons.lizardbase.theme;

import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.structure.Theme;
import me.sword7.adventuredungeon.structure.block.DungeonMaterial;
import me.sword7.adventuredungeon.util.Util;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.block.data.Directional;

import java.util.ArrayList;
import java.util.List;

public class LizardBaseTheme extends Theme {

    private Color color;
    private double secondaryWallChance = Util.randomDouble(0.1, 0.33);
    private double obbyFloorChance = Util.randomDouble(0.1, 0.75);
    private SecondaryWall secondaryWall;
    private List<SecondaryWall> rareWalls = new ArrayList<>();

    public LizardBaseTheme() {
        super();
        init();
    }

    private void init() {

        double colorRoll = random.nextDouble();
        if (Version.getValue() >= 116) {
            if (colorRoll < 0.1) {
                color = Color.BLUE;
            } else if (colorRoll < 0.2) {
                color = Color.ORANGE;
            } else if (colorRoll < 0.55) {
                color = Color.RED;
            } else {
                color = Color.GREEN;
            }
        } else {
            if (colorRoll < 0.5) {
                color = Color.BLUE;
            } else {
                color = Color.ORANGE;
            }
        }

        List<SecondaryWall> altWalls = new ArrayList<>();
        altWalls.add(SecondaryWall.BRICK);
        altWalls.add(SecondaryWall.QUARTZ);
        if (color == Color.GREEN) altWalls.add(SecondaryWall.PRIS);

        secondaryWall = altWalls.get(random.nextInt(altWalls.size()));
        rareWalls.addAll(altWalls);
        rareWalls.remove(secondaryWall);

        let(DungeonMaterial.PRIMARY, Material.STONE_BRICKS);
        let(DungeonMaterial.PRIMARY_STAIR, Material.STONE_BRICK_STAIRS);
        let(DungeonMaterial.PRIMARY_SLAB, Material.STONE_BRICK_SLAB);
        let(DungeonMaterial.PRIMARY_WALL, Material.STONE_BRICK_WALL);

        if (color == Color.GREEN) {
            let(DungeonMaterial.HIGHLIGHT, Material.LIME_TERRACOTTA);
            let(DungeonMaterial.TERTIARY, Material.DARK_PRISMARINE);
            let(DungeonMaterial.TERTIARY_STAIR, Material.DARK_PRISMARINE_STAIRS);
            let(DungeonMaterial.TERTIARY_SLAB, Material.DARK_PRISMARINE_SLAB);
            let(DungeonMaterial.WOOD, Material.STRIPPED_WARPED_STEM);
            let(DungeonMaterial.WOOD_FENCE, Material.WARPED_FENCE);
            let(DungeonMaterial.WOOD_SLAB, Material.WARPED_SLAB);
            let(DungeonMaterial.WOOD_STAIR, Material.WARPED_STAIRS);
            let(DungeonMaterial.WOOD_TRAPDOOR, Material.WARPED_TRAPDOOR);
            let(DungeonMaterial.WOOD_BUTTON, Material.WARPED_BUTTON);
            let(DungeonMaterial.FANCY, Material.GREEN_GLAZED_TERRACOTTA);
            let(DungeonMaterial.ROD, Material.GREEN_STAINED_GLASS_PANE);
            let(DungeonMaterial.BANNER_1, Material.GREEN_WALL_BANNER);
            let(DungeonMaterial.CARPET_MAIN_1, Material.GREEN_CARPET);
            let(DungeonMaterial.CARPET_MAIN_2, Material.LIME_CARPET);
            let(DungeonMaterial.CARPET_DARK_1, Material.GRAY_CARPET);
            let(DungeonMaterial.CARPET_LIGHT_1, Material.LIGHT_GRAY_CARPET);
        } else if (color == Color.RED) {
            let(DungeonMaterial.HIGHLIGHT, Material.RED_TERRACOTTA);
            let(DungeonMaterial.TERTIARY, Material.NETHER_BRICKS);
            let(DungeonMaterial.TERTIARY_STAIR, Material.NETHER_BRICK_STAIRS);
            let(DungeonMaterial.TERTIARY_SLAB, Material.NETHER_BRICK_SLAB);
            let(DungeonMaterial.WOOD, Material.STRIPPED_CRIMSON_STEM);
            let(DungeonMaterial.WOOD_FENCE, Material.CRIMSON_FENCE);
            let(DungeonMaterial.WOOD_SLAB, Material.CRIMSON_SLAB);
            let(DungeonMaterial.WOOD_STAIR, Material.CRIMSON_STAIRS);
            let(DungeonMaterial.WOOD_TRAPDOOR, Material.CRIMSON_TRAPDOOR);
            let(DungeonMaterial.WOOD_BUTTON, Material.CRIMSON_BUTTON);
            let(DungeonMaterial.FANCY, Material.BLACK_GLAZED_TERRACOTTA);
            let(DungeonMaterial.ROD, Material.RED_STAINED_GLASS_PANE);
            let(DungeonMaterial.BANNER_1, Material.RED_WALL_BANNER);
            let(DungeonMaterial.CARPET_MAIN_1, Material.BLACK_CARPET);
            let(DungeonMaterial.CARPET_MAIN_2, Material.RED_CARPET);
            let(DungeonMaterial.CARPET_DARK_1, Material.GRAY_CARPET);
            let(DungeonMaterial.CARPET_LIGHT_1, Material.LIGHT_GRAY_CARPET);
        } else if (color == Color.BLUE) {
            let(DungeonMaterial.HIGHLIGHT, Material.CYAN_TERRACOTTA);
            let(DungeonMaterial.TERTIARY, Material.SANDSTONE);
            let(DungeonMaterial.TERTIARY_STAIR, Material.SANDSTONE_STAIRS);
            let(DungeonMaterial.TERTIARY_SLAB, Material.SANDSTONE_SLAB);
            let(DungeonMaterial.WOOD, Material.STRIPPED_BIRCH_LOG);
            let(DungeonMaterial.WOOD_FENCE, Material.BIRCH_FENCE);
            let(DungeonMaterial.WOOD_SLAB, Material.BIRCH_SLAB);
            let(DungeonMaterial.WOOD_STAIR, Material.BIRCH_STAIRS);
            let(DungeonMaterial.WOOD_TRAPDOOR, Material.BIRCH_TRAPDOOR);
            let(DungeonMaterial.WOOD_BUTTON, Material.BIRCH_BUTTON);
            let(DungeonMaterial.FANCY, Material.CYAN_GLAZED_TERRACOTTA);
            let(DungeonMaterial.ROD, Material.LIGHT_BLUE_STAINED_GLASS_PANE);
            let(DungeonMaterial.BANNER_1, Material.CYAN_WALL_BANNER);
            let(DungeonMaterial.CARPET_MAIN_1, Material.CYAN_CARPET);
            let(DungeonMaterial.CARPET_MAIN_2, Material.LIGHT_BLUE_CARPET);
            let(DungeonMaterial.CARPET_DARK_1, Material.GRAY_CARPET);
            let(DungeonMaterial.CARPET_LIGHT_1, Material.LIGHT_GRAY_CARPET);
        } else if (color == Color.ORANGE) {
            let(DungeonMaterial.HIGHLIGHT, Material.ORANGE_TERRACOTTA);
            let(DungeonMaterial.TERTIARY, Material.RED_SANDSTONE);
            let(DungeonMaterial.TERTIARY_STAIR, Material.RED_SANDSTONE_STAIRS);
            let(DungeonMaterial.TERTIARY_SLAB, Material.RED_SANDSTONE_SLAB);
            let(DungeonMaterial.WOOD, Material.STRIPPED_ACACIA_LOG);
            let(DungeonMaterial.WOOD_FENCE, Material.ACACIA_FENCE);
            let(DungeonMaterial.WOOD_SLAB, Material.ACACIA_SLAB);
            let(DungeonMaterial.WOOD_STAIR, Material.ACACIA_STAIRS);
            let(DungeonMaterial.WOOD_TRAPDOOR, Material.ACACIA_TRAPDOOR);
            let(DungeonMaterial.WOOD_BUTTON, Material.ACACIA_BUTTON);
            let(DungeonMaterial.FANCY, Material.BROWN_GLAZED_TERRACOTTA);
            let(DungeonMaterial.ROD, Material.CYAN_STAINED_GLASS_PANE);
            let(DungeonMaterial.BANNER_1, Material.GRAY_WALL_BANNER);
            let(DungeonMaterial.CARPET_MAIN_1, Material.BLACK_CARPET);
            let(DungeonMaterial.CARPET_MAIN_2, Material.LIGHT_GRAY_CARPET);
            let(DungeonMaterial.CARPET_DARK_1, Material.GRAY_CARPET);
            let(DungeonMaterial.CARPET_LIGHT_1, Material.LIGHT_GRAY_CARPET);
        }

        let(DungeonMaterial.SECONDARY, Material.POLISHED_ANDESITE);
        let(DungeonMaterial.SECONDARY_STAIR, Material.POLISHED_ANDESITE_STAIRS);
        let(DungeonMaterial.SECONDARY_SLAB, Material.POLISHED_ANDESITE_SLAB);

        let(DungeonMaterial.QUATERNARY, Material.COBBLESTONE);
        let(DungeonMaterial.QUATERNARY_STAIR, Material.COBBLESTONE_STAIRS);
        let(DungeonMaterial.QUATERNARY_SLAB, Material.COBBLESTONE_SLAB);
        let(DungeonMaterial.QUATERNARY_WALL, Material.COBBLESTONE_WALL);

        let(DungeonMaterial.FLOOR, Material.SMOOTH_STONE);
        let(DungeonMaterial.FLOOR_SLAB, Material.SMOOTH_STONE_SLAB);
        let(DungeonMaterial.FLOOR_STAIR, Material.STONE_BRICK_STAIRS);


        let(DungeonMaterial.DETAIL, Material.CHISELED_STONE_BRICKS);
        let(DungeonMaterial.SHRUB, Material.DEAD_FIRE_CORAL);

        let(DungeonMaterial.CHEST, Material.CHEST);
        let(DungeonMaterial.CEILING, Material.POLISHED_DIORITE);

        //TODO shulker box colors (red + cyan)

    }


    @Override
    public void beginStruct() {
        super.beginStruct();
        double wallRoll = random.nextDouble();

        SecondaryWall wall = null;

        if (wallRoll < secondaryWallChance) {
            wall = secondaryWall;
        } else if (wallRoll < secondaryWallChance + 0.03) {
            wall = rareWalls.get(random.nextInt(rareWalls.size()));
        }

        if (wall != null) {
            if (wall == SecondaryWall.BRICK) {
                override(DungeonMaterial.PRIMARY, Material.BRICKS);
                override(DungeonMaterial.PRIMARY_STAIR, Material.BRICK_STAIRS);
            } else if (wall == SecondaryWall.QUARTZ) {
                override(DungeonMaterial.PRIMARY, Material.QUARTZ_BRICKS);
                override(DungeonMaterial.PRIMARY_STAIR, Material.QUARTZ_STAIRS);
            } else if (wall == SecondaryWall.PRIS) {
                override(DungeonMaterial.PRIMARY, Material.PRISMARINE_BRICKS);
                override(DungeonMaterial.PRIMARY_STAIR, Material.PRISMARINE_BRICK_STAIRS);
            }
        }

        if (random.nextDouble() < obbyFloorChance) {
            override(DungeonMaterial.FLOOR, Material.OBSIDIAN);
        }
    }


    @Override
    protected void makeBanner(Block block, Directional directional, DungeonMaterial type) {
        DyeColor dyeColor = DyeColor.LIME;
        Material material = Material.GREEN_WALL_BANNER;
        if (color == Color.RED) {
            dyeColor = DyeColor.BLACK;
            material = Material.RED_WALL_BANNER;
        } else if (color == Color.BLUE) {
            dyeColor = DyeColor.GRAY;
            material = Material.CYAN_WALL_BANNER;
        } else if (color == Color.ORANGE) {
            dyeColor = DyeColor.LIGHT_GRAY;
            material = Material.GRAY_WALL_BANNER;
        }
        Directional blockData = (Directional) material.createBlockData();
        blockData.setFacing(directional.getFacing());
        block.setBlockData(blockData);
        Banner banner = (Banner) block.getState();

        banner.addPattern(new Pattern(dyeColor, PatternType.CURLY_BORDER));
        banner.addPattern(new Pattern(dyeColor, PatternType.CIRCLE_MIDDLE));

        banner.update();
    }

    private enum Color {
        GREEN,
        RED,
        ORANGE,
        BLUE,
    }

    public enum SecondaryWall {
        QUARTZ,
        BRICK,
        PRIS,
    }

}
