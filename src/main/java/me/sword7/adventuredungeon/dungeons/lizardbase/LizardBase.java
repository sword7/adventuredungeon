package me.sword7.adventuredungeon.dungeons.lizardbase;

import me.sword7.adventuredungeon.dungeon.Dungeon;
import me.sword7.adventuredungeon.dungeon.DungeonType;
import me.sword7.adventuredungeon.lootpool.LootTag;
import me.sword7.adventuredungeon.mob.DungeonMobType;
import me.sword7.adventuredungeon.util.math.Cardinal;
import me.sword7.adventuredungeon.util.math.IBound;
import me.sword7.adventuredungeon.util.math.Point;
import me.sword7.adventuredungeon.util.math.Point2D;

import java.util.*;

public class LizardBase extends Dungeon {

    public LizardBase(UUID id, String name, UUID worldId, Point entrance, Cardinal direction, Point center, int radius, List<Point2D> grids, Map<Point, List<LootTag>> chestLocations, Map<Point, DungeonMobType> spawnerLocations, Set<UUID> visitors, List<IBound> bounds) {
        super(id, name, DungeonType.LIZARD_BASE, worldId, entrance, direction, center, radius, grids, chestLocations, spawnerLocations, visitors, bounds, Collections.emptyList(), LizardBaseLootPoolSupplier.getLootPool(), new LizardBaseBestiary());
    }


}