package me.sword7.adventuredungeon.dungeons.lizardbase;

import com.google.common.collect.ImmutableList;
import me.sword7.adventuredungeon.dungeon.Dungeon;
import me.sword7.adventuredungeon.dungeon.DungeonCache;
import me.sword7.adventuredungeon.dungeons.crypt.CryptRoomTag;
import me.sword7.adventuredungeon.dungeons.lizardbase.theme.LizardBaseTheme;
import me.sword7.adventuredungeon.generate.GridBlueprint;
import me.sword7.adventuredungeon.generate.GridGenerator;
import me.sword7.adventuredungeon.generate.Room;
import me.sword7.adventuredungeon.generate.RoomData;
import me.sword7.adventuredungeon.structure.ITheme;
import me.sword7.adventuredungeon.structure.StructFlatFile;
import me.sword7.adventuredungeon.structure.Structure;
import me.sword7.adventuredungeon.util.LeafBlower;
import me.sword7.adventuredungeon.util.MaterialUtil;
import me.sword7.adventuredungeon.util.Util;
import me.sword7.adventuredungeon.util.math.*;
import me.sword7.adventuredungeon.worldgen.IDungeonPlacer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.Waterlogged;

import java.util.*;

public class LizardBasePlacer implements IDungeonPlacer {

    private static final Point GRID_SCALE = new Point(11, 10, 11);
    private static final int Y_RANGE = 2;
    private static final Structure MOUTH_STRUCT = StructFlatFile.fetch("lizardbase/entrance", "lizard_entrance_1_mouth");
    private static final Structure CAVE_STRUCT = StructFlatFile.fetch("lizardbase/entrance", "lizard_entrance_1_cave");
    private static final Structure FALL_STRUCT = StructFlatFile.fetch("lizardbase/entrance", "lizard_entrance_1_fall");
    private static List<Point> entPoints = new ImmutableList.Builder<Point>()
            .add(new Point(2, 2, 0)).add(new Point(3, 2, 0)).add(new Point(4, 2, 0)).build();

    @Override
    public boolean place(World world, int spawnX, int spawnZ, ITheme theme, Random random) {

        Rotation rot = Rotation.fromInt(Util.randomInt(0, 3));
        Cardinal entDir = rot.rotateCardinal(Cardinal.N);

        Structure structure = MOUTH_STRUCT.clone();
        Point originalDim = structure.getDimensions();
        structure.rotate(rot);
        Point dim = structure.getDimensions();

        List<Point> rotatedEntPoints = new ArrayList<>();
        for (Point p : entPoints) {
            rotatedEntPoints.add(rot.rotatePoint(p, originalDim.getX(), originalDim.getZ()));
        }

        int groundY = selectGroundY(world, spawnX, spawnZ, dim, entDir);
        boolean entranceClear = checkEntranceClear(world, spawnX, spawnZ, groundY, rotatedEntPoints, entDir);


        if (groundY > 0 && groundY <= 90 && entranceClear) {

            int mouthY = groundY - 1;

            //clear trees
            LeafBlower.clearTrees(world, new Point(spawnX, mouthY, spawnZ), new Point(spawnX + dim.getX(), mouthY + dim.getY(), spawnZ + dim.getZ())); //TODO FIX THIS

            //main structure
            theme.beginTask(Collections.singletonList(LizardBaseRoomTag.ENTRANCE));
            structure.draw(new Location(world, spawnX, mouthY, spawnZ), theme);
            theme.endTask();
            Point entMin = new Point(spawnX, mouthY, spawnZ);
            Point entMax = entMin.clone().add(new Point(dim.getX() - 1, dim.getY() - 1, dim.getZ() - 1));
            IBound entBound = new BoundRect(entMin, entMax);

            //decorations
            //spawnDecor(world, random, spawnX, spawnZ, dim, rot, originalDim, groundY);

            //cave
            Point cMin = new Point(2, -7, -6);
            Point cMax = new Point(8, -1, 6);
            cMin = rot.rotatePoint(cMin, originalDim.getX(), originalDim.getZ());
            cMax = rot.rotatePoint(cMax, originalDim.getX(), originalDim.getZ());
            Point.order(cMin, cMax);
            Structure cave = CAVE_STRUCT.clone();
            cave.rotate(rot);
            theme.beginTask(Collections.singletonList(LizardBaseRoomTag.ENTRANCE));
            cave.draw(new Location(world, cMin.getX() + spawnX, cMin.getY() + mouthY, cMin.getZ() + spawnZ), theme);
            theme.endTask();
            Point caveDim = cave.getDimensions();
            Point caveMin = new Point(cMin.getX() + spawnX, cMin.getY() + mouthY, cMin.getZ() + spawnZ);
            Point caveMax = caveMin.clone().add(caveDim.getX() - 1, caveDim.getY() - 1, caveDim.getZ() - 1);
            IBound caveBound = new BoundRect(caveMin, caveMax);

            //fall
            cMin = new Point(2, -31, -6);
            cMax = new Point(8, -8, 0);
            cMin = rot.rotatePoint(cMin, originalDim.getX(), originalDim.getZ());
            cMax = rot.rotatePoint(cMax, originalDim.getX(), originalDim.getZ());
            Point.order(cMin, cMax);
            Structure fall = FALL_STRUCT.clone();
            fall.rotate(rot);
            fall.draw(new Location(world, cMin.getX() + spawnX, cMin.getY() + mouthY, cMin.getZ() + spawnZ), theme);
            Point fallDim = cave.getDimensions();
            Point fallMin = new Point(cMin.getX() + spawnX, cMin.getY() + mouthY, cMin.getZ() + spawnZ);
            Point fallMax = caveMin.clone().add(fallDim.getX() - 1, fallDim.getY() - 1, fallDim.getZ() - 1);
            IBound fallBound = new BoundRect(fallMin, fallMax);


            //first room
            cMin = new Point(0, -33, 1);
            cMax = new Point(10, -24, 11);
            cMin = rot.rotatePoint(cMin, originalDim.getX(), originalDim.getZ());
            cMax = rot.rotatePoint(cMax, originalDim.getX(), originalDim.getZ());
            Point.order(cMin, cMax);

            List<IBound> bounds = new ArrayList<>();
            bounds.add(entBound);
            bounds.add(caveBound);
            bounds.add(fallBound);
            List<Point> chests = new ArrayList<>();
            List<Point> spawners = new ArrayList<>();

            Location dLoc = new Location(world, cMin.getX() + spawnX, cMin.getY() + mouthY, cMin.getZ() + spawnZ);
            GridGenerator generator = new GridGenerator(dLoc, LizardBaseFrame.values(), GRID_SCALE, entDir);
            GridBlueprint blueprint = (GridBlueprint) generator.generate();
            for (Room room : blueprint.getRooms()) {
                RoomData data = room.draw(dLoc, theme, GRID_SCALE);
                chests.addAll(data.getChestLocations());
                data.getChestLocations().clear();
                spawners.addAll(data.getSpawnerLocation());
                data.getSpawnerLocation().clear();
                bounds.add(room.getBounds(dLoc, GRID_SCALE));
            }

            //ent point
            Point ent = new Point(5, 2, 8);
            ent = rot.rotatePoint(ent, originalDim.getX(), originalDim.getZ());
            ent = ent.add(spawnX, mouthY, spawnZ);

            Dungeon dungeon = new LizardBaseConstructor(world.getUID(), ent, entDir, bounds, chests, spawners).construct();
            DungeonCache.registerDungeon(dungeon);

            return true;

        }

        return false;

    }

    private int selectGroundY(World world, int spawnX, int spawnZ, Point dim, Cardinal entDir) {
        Set<Cardinal> waterBodies = new HashSet<>();

        int minY = getHighestGroundHeightAt(world, spawnX, spawnZ);
        int maxY = minY;

        for (int x = spawnX; x < spawnX + dim.getX(); x++) {
            int y = getHighestGroundHeightAt(world, x, spawnZ);
            if (y > maxY) maxY = y;
            if (y < minY) minY = y;
            if (maxY - minY > Y_RANGE) {
                return -1;
            }
            Material bt = world.getBlockAt(x, y, spawnZ).getType();
            Block b1 = world.getBlockAt(x, y + 1, spawnZ);
            if (isLiquid(b1)) waterBodies.add(Cardinal.N);
            if (!MaterialUtil.NATURAL_GROUND.contains(bt)) return -1;
        }
        for (int x = spawnX; x < spawnX + dim.getX(); x++) {
            int y = getHighestGroundHeightAt(world, x, spawnZ + dim.getZ());
            if (y > maxY) maxY = y;
            if (y < minY) minY = y;
            if (maxY - minY > Y_RANGE) {
                return -1;
            }
            Material bt = world.getBlockAt(x, y, spawnZ + dim.getZ()).getType();
            Block b1 = world.getBlockAt(x, y + 1, spawnZ + dim.getZ());
            if (isLiquid(b1)) waterBodies.add(Cardinal.S);
            if (!MaterialUtil.NATURAL_GROUND.contains(bt)) return -1;
        }
        for (int z = spawnZ; z < spawnZ + dim.getZ(); z++) {
            int y = getHighestGroundHeightAt(world, spawnX, z);
            if (y > maxY) maxY = y;
            if (y < minY) minY = y;
            if (maxY - minY > Y_RANGE) {
                return -1;
            }

            Material bt = world.getBlockAt(spawnX, y, z).getType();
            Block b1 = world.getBlockAt(spawnX, y + 1, z);
            if (isLiquid(b1)) waterBodies.add(Cardinal.W);
            if (!MaterialUtil.NATURAL_GROUND.contains(bt)) return -1;
        }
        for (int z = spawnZ; z < spawnZ + dim.getZ(); z++) {
            int y = getHighestGroundHeightAt(world, spawnX + dim.getX(), z);
            if (y > maxY) maxY = y;
            if (y < minY) minY = y;
            if (maxY - minY > Y_RANGE) {
                return -1;
            }
            Material bt = world.getBlockAt(spawnX + dim.getX(), y, spawnZ).getType();
            Block b1 = world.getBlockAt(spawnX + dim.getX(), y + 1, spawnZ);
            if (isLiquid(b1)) waterBodies.add(Cardinal.E);
            if (!MaterialUtil.NATURAL_GROUND.contains(bt)) return -1;
        }

        if ((waterBodies.contains(Cardinal.N) && waterBodies.contains(Cardinal.S)) ||
                (waterBodies.contains(Cardinal.W) && waterBodies.contains(Cardinal.E)) ||
                waterBodies.contains(entDir)) {
            return -1;
        }

        return minY;
    }


    private boolean isLiquid(Block b) {
        if (MaterialUtil.WATER.contains(b.getType())) return true;
        if (b.getBlockData() instanceof Waterlogged && ((Waterlogged) b.getBlockData()).isWaterlogged()) return true;
        return false;
    }

    private boolean checkEntranceClear(World world, int spawnX, int spawnZ, int groundY, List<Point> entPoints, Cardinal dir) {

        for (Point p : entPoints) {
            for (int d = 1; d < 17; d++) {
                int x = spawnX + p.getX() + (dir.getDirection().getX() * d);
                int z = spawnZ + p.getZ() + (dir.getDirection().getZ() * d);
                int y = getHighestGroundHeightAt(world, x, z);
                if (d <= 2) {
                    Block block = world.getBlockAt(x, y + 1, z);
                    if (block.isLiquid()) {
                        return false;
                    } else if (y > groundY || y < groundY - 1) {
                        return false;
                    }
                } else if (d <= 9) {
                    if (y > groundY) {
                        return false;
                    }
                } else {
                    if (y > groundY + 1) {
                        return false;
                    }
                }
            }
        }

        return true;
    }


    private int getHighestGroundHeightAt(World world, int x, int z) {
        Block block = world.getHighestBlockAt(x, z);
        while (!block.getType().isSolid() || MaterialUtil.WOOD.contains(block.getType()) || MaterialUtil.LEAVES.contains(block.getType()) || MaterialUtil.WATER.contains(block.getType())) {
            block = world.getBlockAt(x, block.getY() - 1, z);
        }
        return block.getY();
    }


}
