package me.sword7.adventuredungeon.dungeons.crypt.theme;

import me.sword7.adventuredungeon.structure.block.BlockSingleton;
import me.sword7.adventuredungeon.structure.block.BlockWeightedJumble;
import me.sword7.adventuredungeon.structure.block.DungeonMaterial;
import me.sword7.adventuredungeon.util.BlockColor;
import org.bukkit.Material;

public class CoralTheme extends CryptTheme {

    public CoralTheme(BlockColor blockColor) {
        super(blockColor);
        init();
    }

    private void init() {

        secondaryWall = new BlockWeightedJumble().add(Material.POLISHED_DIORITE, 15).add(Material.DIORITE, 2).add(Material.DEAD_BRAIN_CORAL_BLOCK, 2);
        rareWall1 = new BlockWeightedJumble().add(Material.PRISMARINE, 17).add(Material.DEAD_BRAIN_CORAL_BLOCK, 2);
        rareWall2 = new BlockWeightedJumble().add(Material.PRISMARINE_BRICKS, 17).add(Material.DEAD_BRAIN_CORAL_BLOCK, 2);
        smoothFloor = new BlockWeightedJumble().add(Material.SMOOTH_STONE, 17).add(Material.DEAD_FIRE_CORAL_BLOCK, 2);
        checker = selectChecker(new BlockSingleton(Material.DEAD_FIRE_CORAL_BLOCK), 0.1, blockColor);

        let(DungeonMaterial.PRIMARY, Material.DEAD_BRAIN_CORAL_BLOCK);
        let(DungeonMaterial.CEILING, new BlockWeightedJumble().add(Material.DEAD_BUBBLE_CORAL_BLOCK, 17).add(Material.STONE, 1).add(Material.ANDESITE, 1));
        let(DungeonMaterial.FLOOR, new BlockWeightedJumble().add(Material.DEAD_FIRE_CORAL_BLOCK, 17).add(Material.STONE, 1).add(Material.ANDESITE, 1));

        let(DungeonMaterial.ROOF, Material.DEAD_BUBBLE_CORAL_BLOCK);
        let(DungeonMaterial.ROOF_STAIR, Material.DEAD_BUBBLE_CORAL_BLOCK);
        let(DungeonMaterial.ROOF_SLAB, Material.DEAD_BUBBLE_CORAL_BLOCK);

    }


    @Override
    protected void doSecondaryWallRoofOverride() {
        //do nothing
    }


}
