package me.sword7.adventuredungeon.dungeons.crypt;

import com.google.common.collect.ImmutableList;
import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.dungeon.Dungeon;
import me.sword7.adventuredungeon.dungeon.DungeonCache;
import me.sword7.adventuredungeon.dungeons.crypt.theme.CryptTheme;
import me.sword7.adventuredungeon.generate.GridBlueprint;
import me.sword7.adventuredungeon.generate.GridGenerator;
import me.sword7.adventuredungeon.generate.Room;
import me.sword7.adventuredungeon.generate.RoomData;
import me.sword7.adventuredungeon.structure.ITheme;
import me.sword7.adventuredungeon.structure.StructFlatFile;
import me.sword7.adventuredungeon.structure.Structure;
import me.sword7.adventuredungeon.structure.block.DungeonBlock;
import me.sword7.adventuredungeon.structure.block.DungeonMaterial;
import me.sword7.adventuredungeon.structure.block.ThemeBlock;
import me.sword7.adventuredungeon.util.LeafBlower;
import me.sword7.adventuredungeon.util.MaterialUtil;
import me.sword7.adventuredungeon.util.Util;
import me.sword7.adventuredungeon.util.math.*;
import me.sword7.adventuredungeon.worldgen.IDungeonPlacer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.FaceAttachable;
import org.bukkit.block.data.Waterlogged;
import org.bukkit.block.data.type.Leaves;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.block.data.type.Switch;
import org.bukkit.util.Vector;

import java.util.*;

public class CryptPlacer implements IDungeonPlacer {


    private static final Point GRID_SCALE = new Point(9, 6, 9);
    private static final int Y_RANGE = 4;
    private static Structure building = StructFlatFile.fetch("crypt/entrance", "crypt_entrance_1");
    private static Structure stairs = StructFlatFile.fetch("crypt/entrance", "crypt_entrance_1_stairs");
    private static List<Point> entPoints = new ImmutableList.Builder<Point>()
            .add(new Point(7, 0, 1)).add(new Point(8, 0, 1)).add(new Point(9, 0, 1)).build();

    @Override
    public boolean place(World world, int spawnX, int spawnZ, ITheme theme, Random random) {

        Rotation rot = Rotation.fromInt(Util.randomInt(0, 3));
        Cardinal entDir = rot.rotateCardinal(Cardinal.N);
        Structure structure = building.clone();
        Point originalDim = structure.getDimensions();
        structure.rotate(rot);
        Point dim = structure.getDimensions();

        List<Point> rotatedEntPoints = new ArrayList<>();
        for (Point p : entPoints) {
            rotatedEntPoints.add(rot.rotatePoint(p, originalDim.getX(), originalDim.getZ()));
        }

        int groundY = selectGroundY(world, spawnX, spawnZ, dim, entDir);
        boolean entranceClear = checkEntranceClear(world, spawnX, spawnZ, groundY, rotatedEntPoints, entDir);
        boolean basementCovered = true;

        if (groundY > 0 && groundY <= 90 && entranceClear && basementCovered) {


            //clear trees
            LeafBlower.clearTrees(world, new Point(spawnX, groundY + 1, spawnZ), new Point(spawnX + dim.getX(), groundY + 1 + dim.getY(), spawnZ + dim.getZ()));

            //clear trees in front
            Point frontMin = rot.rotatePoint(new Point(6, 0, 0), originalDim.getX(), originalDim.getZ());
            frontMin = frontMin.add(spawnX, groundY + 1, spawnZ);
            Cardinal runDir = entDir.next();
            Point frontMax = frontMin.add((runDir.getDirection().getX() * 4) + (entDir.getDirection().getX() * 7), dim.getY(), (runDir.getDirection().getZ() * 4) + (entDir.getDirection().getZ() * 7));
            Point.order(frontMin, frontMax);
            LeafBlower.clearTrees(world, frontMin, frontMax);

            //overhang
            ThemeBlock themeBlock = new ThemeBlock(DungeonMaterial.HIGHLIGHT);
            DungeonBlock[][][] blocks = structure.getBlocks();
            for (int x = spawnX; x < spawnX + dim.getX(); x++) {
                for (int z = spawnZ; z < spawnZ + dim.getZ(); z++) {
                    int blockY = getHighestGroundHeightAt(world, x, z);
                    if (blockY < groundY) {
                        DungeonBlock dungeonBlock = blocks[x - spawnX][0][z - spawnZ];
                        if (dungeonBlock != null && !(dungeonBlock instanceof ThemeBlock && ((ThemeBlock) dungeonBlock).getDungeonMaterial() == DungeonMaterial.VOID)) {
                            for (int y = blockY + 1; y <= groundY; y++) {
                                theme.setBlock(world.getBlockAt(x, y, z), themeBlock);
                            }
                        }
                    }
                }
            }

            //main structure
            theme.beginTask(Collections.singletonList(CryptRoomTag.ENTRANCE));
            structure.draw(new Location(world, spawnX, groundY + 1, spawnZ), theme);
            theme.endTask();
            Point entMin = new Point(spawnX, groundY + 1, spawnZ);
            Point entMax = entMin.clone().add(new Point(dim.getX() - 1, dim.getY() - 1, dim.getZ() - 1));
            IBound entBound = new BoundRect(entMin, entMax);

            //stairs to entrance
            doEntranceStairs(world, spawnX, spawnZ, groundY, rotatedEntPoints, entDir, theme);

            //decorations
            spawnDecor(world, random, spawnX, spawnZ, dim, rot, originalDim, groundY);

            //stairs
            Point cMin = new Point(6, -15, 7);
            Point cMax = new Point(10, -1, 23);
            cMin = rot.rotatePoint(cMin, originalDim.getX(), originalDim.getZ());
            cMax = rot.rotatePoint(cMax, originalDim.getX(), originalDim.getZ());
            Point.order(cMin, cMax);
            Structure stairs = this.stairs.clone();
            stairs.rotate(rot);
            stairs.draw(new Location(world, cMin.getX() + spawnX, cMin.getY() + groundY + 1, cMin.getZ() + spawnZ), theme);
            Point stairDim = stairs.getDimensions();
            Point stairMin = new Point(cMin.getX() + spawnX, cMin.getY() + groundY + 1, cMin.getZ() + spawnZ);
            Point stairMax = stairMin.clone().add(stairDim.getX() - 1, stairDim.getY() - 1, stairDim.getZ() - 1);
            IBound stairBound = new BoundRect(stairMin, stairMax);

            //first room
            cMin = new Point(4, -15, 24);
            cMax = new Point(12, -10, 32);
            cMin = rot.rotatePoint(cMin, originalDim.getX(), originalDim.getZ());
            cMax = rot.rotatePoint(cMax, originalDim.getX(), originalDim.getZ());
            Point.order(cMin, cMax);

            List<IBound> bounds = new ArrayList<>();
            bounds.add(entBound);
            bounds.add(stairBound);
            List<Point> chests = new ArrayList<>();
            List<Point> spawners = new ArrayList<>();

            Location dLoc = new Location(world, cMin.getX() + spawnX, cMin.getY() + groundY + 1, cMin.getZ() + spawnZ);
            GridGenerator generator = new GridGenerator(dLoc, CryptFrame.values(), GRID_SCALE, entDir);
            GridBlueprint blueprint = (GridBlueprint) generator.generate();
            for (Room room : blueprint.getRooms()) {
                RoomData data = room.draw(dLoc, theme, GRID_SCALE);
                chests.addAll(data.getChestLocations());
                data.getChestLocations().clear();
                spawners.addAll(data.getSpawnerLocation());
                data.getSpawnerLocation().clear();
                bounds.add(room.getBounds(dLoc, GRID_SCALE));
            }

            //ent point
            Point ent = new Point(8, 1, 6);
            ent = rot.rotatePoint(ent, originalDim.getX(), originalDim.getZ());
            ent = ent.add(spawnX, groundY + 1, spawnZ);

            Dungeon dungeon = new CryptConstructor(world.getUID(), ent, entDir, bounds, chests, spawners, ((CryptTheme) theme).getBlockColor()).construct();
            DungeonCache.registerDungeon(dungeon);

            return true;
        }

        return false;
    }

    private int selectGroundY(World world, int spawnX, int spawnZ, Point dim, Cardinal entDir) {
        Set<Cardinal> waterBodies = new HashSet<>();

        int minY = getHighestGroundHeightAt(world, spawnX, spawnZ);
        int maxY = minY;

        for (int x = spawnX; x < spawnX + dim.getX(); x++) {
            int y = getHighestGroundHeightAt(world, x, spawnZ);
            if (y > maxY) maxY = y;
            if (y < minY) minY = y;
            if (maxY - minY > Y_RANGE) {
                return -1;
            }
            Material bt = world.getBlockAt(x, y, spawnZ).getType();
            Block b1 = world.getBlockAt(x, y + 1, spawnZ);
            if (isLiquid(b1)) waterBodies.add(Cardinal.N);
            if (!MaterialUtil.NATURAL_GROUND.contains(bt)) return -1;
        }
        for (int x = spawnX; x < spawnX + dim.getX(); x++) {
            int y = getHighestGroundHeightAt(world, x, spawnZ + dim.getZ());
            if (y > maxY) maxY = y;
            if (y < minY) minY = y;
            if (maxY - minY > Y_RANGE) {
                return -1;
            }
            Material bt = world.getBlockAt(x, y, spawnZ + dim.getZ()).getType();
            Block b1 = world.getBlockAt(x, y + 1, spawnZ + dim.getZ());
            if (isLiquid(b1)) waterBodies.add(Cardinal.S);
            if (!MaterialUtil.NATURAL_GROUND.contains(bt)) return -1;
        }
        for (int z = spawnZ; z < spawnZ + dim.getZ(); z++) {
            int y = getHighestGroundHeightAt(world, spawnX, z);
            if (y > maxY) maxY = y;
            if (y < minY) minY = y;
            if (maxY - minY > Y_RANGE) {
                return -1;
            }

            Material bt = world.getBlockAt(spawnX, y, z).getType();
            Block b1 = world.getBlockAt(spawnX, y + 1, z);
            if (isLiquid(b1)) waterBodies.add(Cardinal.W);
            if (!MaterialUtil.NATURAL_GROUND.contains(bt)) return -1;
        }
        for (int z = spawnZ; z < spawnZ + dim.getZ(); z++) {
            int y = getHighestGroundHeightAt(world, spawnX + dim.getX(), z);
            if (y > maxY) maxY = y;
            if (y < minY) minY = y;
            if (maxY - minY > Y_RANGE) {
                return -1;
            }
            Material bt = world.getBlockAt(spawnX + dim.getX(), y, spawnZ).getType();
            Block b1 = world.getBlockAt(spawnX + dim.getX(), y + 1, spawnZ);
            if (isLiquid(b1)) waterBodies.add(Cardinal.E);
            if (!MaterialUtil.NATURAL_GROUND.contains(bt)) return -1;
        }

        if ((waterBodies.contains(Cardinal.N) && waterBodies.contains(Cardinal.S)) ||
                (waterBodies.contains(Cardinal.W) && waterBodies.contains(Cardinal.E)) ||
                waterBodies.contains(entDir)) {
            return -1;
        }

        return maxY;
    }

    private boolean checkEntranceClear(World world, int spawnX, int spawnZ, int groundY, List<Point> entPoints, Cardinal dir) {

        for (Point p : entPoints) {
            for (int d = 1; d < 17; d++) {
                int x = spawnX + p.getX() + (dir.getDirection().getX() * d);
                int z = spawnZ + p.getZ() + (dir.getDirection().getZ() * d);
                int y = getHighestGroundHeightAt(world, x, z);
                if (d <= 2) {
                    Block block = world.getBlockAt(x, y + 1, z);
                    if (block.isLiquid()) {
                        return false;
                    } else if (y > groundY || y < groundY - 1) {
                        return false;
                    }
                } else if (d <= 9) {
                    if (y > groundY) {
                        return false;
                    }
                } else {
                    if (y > groundY + 1) {
                        return false;
                    }
                }
            }
        }

        return true;
    }


    private boolean doEntranceStairs(World world, int spawnX, int spawnZ, int groundY, List<Point> entPoints, Cardinal dir, ITheme theme) {

        for (Point p : entPoints) {
            int x = spawnX + p.getX() + dir.getDirection().getX();
            int y = groundY;
            int z = spawnZ + p.getZ() + dir.getDirection().getZ();
            Block block = world.getBlockAt(x, y, z);
            if (!block.getType().isSolid() || MaterialUtil.WOOD.contains(block.getType()) || MaterialUtil.LEAVES.contains(block.getType())) {
                block.setType(Material.COBBLESTONE_STAIRS);
                Stairs stairs = (Stairs) block.getBlockData();
                stairs.setFacing(dir.asFace().getOppositeFace());
                theme.setBlock(block, new ThemeBlock(DungeonMaterial.HIGHLIGHT_STAIR, stairs));
            }
        }

        return true;
    }

    private int getHighestGroundHeightAt(World world, int x, int z) {
        Block block = world.getHighestBlockAt(x, z);
        while (!block.getType().isSolid() || MaterialUtil.WOOD.contains(block.getType()) || MaterialUtil.LEAVES.contains(block.getType()) || MaterialUtil.WATER.contains(block.getType())) {
            block = world.getBlockAt(x, block.getY() - 1, z);
        }
        return block.getY();
    }

    private void spawnDecor(World world, Random random, int spawnX, int spawnZ, Point dim, Rotation rot, Point originalDim, int groundY) {

        int centerX = spawnX + (dim.getX() / 2);
        if (dim.getX() % 2 != 0) centerX++;
        int centerZ = spawnZ + (dim.getZ() / 2);
        if (dim.getZ() % 2 != 0) centerZ++;
        Vector center = new Vector(centerX, 0, centerZ);
        int radius = 15;
        for (int x = -radius; x < radius; x++) {
            for (int z = -radius; z < radius; z++) {
                Vector pos = center.clone().add(new Vector(x, 0, z));
                double d = center.distance(pos);
                if (d <= radius + 0.5) {

                    Block block = world.getBlockAt(pos.getBlockX(), getHighestGroundHeightAt(world, pos.getBlockX(), pos.getBlockZ()), pos.getBlockZ());

                    if (!isLiquid(block.getRelative(0, 1, 0))) {

                        clearFlowers(block);

                        if (block.getType() == Material.GRASS_BLOCK || block.getType() == Material.PODZOL) {
                            double r = random.nextDouble();
                            boolean stone = false;
                            boolean soulSand = false;
                            if (r < 0.3) {
                                block.setType(Material.COARSE_DIRT);
                                if (random.nextDouble() < 0.1) {
                                    Block above = block.getRelative(0, 1, 0);
                                    if (above.getType() == Material.AIR) {
                                        above.setType(Material.DEAD_BUSH);
                                    }
                                }
                            } else if (r < 0.37) {
                                clearGrowth(block);
                                block.setType(Material.ANDESITE);
                                stone = true;
                            } else if (r < 0.45) {
                                clearGrowth(block);
                                block.setType(Material.STONE);
                                stone = true;
                            } else if (r < 0.5) {
                                clearGrowth(block);
                                block.setType(Material.SOUL_SAND);
                                soulSand = true;
                            }

                            if (stone) {
                                if (random.nextDouble() < 0.1) {
                                    Block above = block.getRelative(0, 1, 0);
                                    if (above.getType() == Material.AIR) {
                                        above.setType(Material.STONE_BUTTON);
                                        Directional data = (Directional) above.getBlockData();
                                        data.setFacing(Cardinal.getRandom().asFace());
                                        above.setBlockData(data);
                                        if (Version.getValue() >= 116) {
                                            FaceAttachable attached = (FaceAttachable) above.getBlockData();
                                            attached.setAttachedFace(FaceAttachable.AttachedFace.FLOOR);
                                            above.setBlockData(attached);
                                        } else {
                                            Switch switchData = (Switch) above.getBlockData();
                                            switchData.setFace(Switch.Face.FLOOR);
                                            above.setBlockData(switchData);
                                        }
                                    }
                                }
                            }

                            if (!stone && !soulSand) {
                                r = random.nextDouble();
                                if (r < 0.1) {
                                    Block above = block.getRelative(0, 1, 0);
                                    if (above.getType() == Material.AIR) {
                                        above.setType(Material.FERN);
                                    }
                                } else if (r < 0.105) {
                                    Block above = block.getRelative(0, 1, 0);
                                    Block twoAbove = block.getRelative(0, 2, 0);
                                    if (above.getType() == Material.AIR && twoAbove.getType() == Material.AIR) {
                                        setBisected(above, Material.TALL_GRASS, Bisected.Half.BOTTOM);
                                        setBisected(twoAbove, Material.TALL_GRASS, Bisected.Half.TOP);
                                    }
                                } else if (r < 0.115) {
                                    Block above = block.getRelative(0, 1, 0);
                                    Block twoAbove = block.getRelative(0, 2, 0);
                                    if (above.getType() == Material.AIR && twoAbove.getType() == Material.AIR) {
                                        setBisected(above, Material.LARGE_FERN, Bisected.Half.BOTTOM);
                                        setBisected(twoAbove, Material.LARGE_FERN, Bisected.Half.TOP);
                                    }
                                } else if (r < 0.125) {
                                    Block above = block.getRelative(0, 1, 0);
                                    if (above.getType() == Material.AIR) {
                                        setLeaf(above);
                                    }
                                }
                            }

                        }

                        if (d > 13) {
                            if (random.nextDouble() < 0.05) {
                                clearGrowth(block);
                                block.getRelative(0, 1, 0).setType(Material.COBBLESTONE_WALL);
                            }
                        } else if (d > 11) {
                            if (random.nextDouble() < 0.02) {
                                clearGrowth(block);
                                block.getRelative(0, 1, 0).setType(Material.COBBLESTONE_WALL);
                            }
                        }

                    }
                }
            }
        }

        //tall ferm/grass/bush close
        List<Point> bushPoints = new ArrayList<>();
        bushPoints.add(new Point(3, 0, 1));
        bushPoints.add(new Point(4, 0, 1));
        bushPoints.add(new Point(5, 0, 1));
        bushPoints.add(new Point(11, 0, 1));
        bushPoints.add(new Point(12, 0, 1));
        bushPoints.add(new Point(13, 0, 1));

        bushPoints.add(new Point(1, 0, 6));
        bushPoints.add(new Point(1, 0, 7));
        bushPoints.add(new Point(1, 0, 8));
        bushPoints.add(new Point(1, 0, 12));
        bushPoints.add(new Point(1, 0, 13));
        bushPoints.add(new Point(1, 0, 14));

        bushPoints.add(new Point(15, 0, 6));
        bushPoints.add(new Point(15, 0, 7));
        bushPoints.add(new Point(15, 0, 8));
        bushPoints.add(new Point(15, 0, 12));
        bushPoints.add(new Point(15, 0, 13));
        bushPoints.add(new Point(15, 0, 14));

        bushPoints.add(new Point(3, 0, 19));
        bushPoints.add(new Point(4, 0, 19));
        bushPoints.add(new Point(5, 0, 19));
        bushPoints.add(new Point(6, 0, 19));
        bushPoints.add(new Point(7, 0, 19));
        bushPoints.add(new Point(8, 0, 19));
        bushPoints.add(new Point(9, 0, 19));
        bushPoints.add(new Point(10, 0, 19));
        bushPoints.add(new Point(11, 0, 19));
        bushPoints.add(new Point(12, 0, 19));
        bushPoints.add(new Point(13, 0, 19));

        List<Point> rotatedBushPoints = new ArrayList<>();
        for (Point p : bushPoints) {
            rotatedBushPoints.add(rot.rotatePoint(p, originalDim.getX(), originalDim.getZ()));
        }

        for (Point p : rotatedBushPoints) {
            int x = p.getX() + spawnX;
            int z = p.getZ() + spawnZ;
            Block groundBlock = world.getBlockAt(x, getHighestGroundHeightAt(world, x, z), z);
            Material groundType = groundBlock.getType();
            if (groundType == Material.DIRT || groundType == Material.GRASS_BLOCK || groundType == Material.PODZOL || groundType == Material.COARSE_DIRT) {
                int maxHeight = 0;
                Block above = groundBlock.getRelative(0, 1, 0);
                while (!above.getType().isSolid() && above.getLocation().getBlockY() <= groundY + 2 && maxHeight < 3) {
                    above = above.getRelative(0, 1, 0);
                    maxHeight++;
                }

                double r = random.nextDouble();
                if (r < 0.7) {
                    if (maxHeight >= 1) {
                        int height = Util.randomInt(1, maxHeight);
                        for (int y = 1; y <= height; y++) {
                            Block bush = world.getBlockAt(x, y + groundBlock.getY(), z);
                            setLeaf(bush);
                        }
                    }
                } else if (r < 0.8) {
                    if (maxHeight >= 2) {
                        setBisected(groundBlock.getRelative(0, 1, 0), Material.LARGE_FERN, Bisected.Half.BOTTOM);
                        setBisected(groundBlock.getRelative(0, 2, 0), Material.LARGE_FERN, Bisected.Half.TOP);
                    }
                } else if (r < 0.9) {
                    if (maxHeight >= 2) {
                        if (maxHeight >= 2) {
                            setBisected(groundBlock.getRelative(0, 1, 0), Material.TALL_GRASS, Bisected.Half.BOTTOM);
                            setBisected(groundBlock.getRelative(0, 2, 0), Material.TALL_GRASS, Bisected.Half.TOP);
                        }
                    }
                }

            }
        }

        //leaves/vines around 5 sides

        //N side
        Point cMin = new Point(3, 1, 2);
        Point cMax = new Point(13, 5, 2);
        setBush(cMin, cMax, world, spawnX, spawnZ, groundY, random, rot, originalDim);

        List<Point> leafPoints = new ArrayList<>();
        leafPoints.add(new Point(10, 6, 2));
        leafPoints.add(new Point(9, 6, 2));
        leafPoints.add(new Point(8, 6, 2));
        leafPoints.add(new Point(7, 6, 2));
        leafPoints.add(new Point(6, 6, 2));
        leafPoints.add(new Point(9, 7, 2));
        leafPoints.add(new Point(8, 7, 2));
        leafPoints.add(new Point(7, 7, 2));
        leafPoints.add(new Point(10, 8, 2));
        leafPoints.add(new Point(6, 8, 2));
        leafPoints.add(new Point(9, 9, 2));
        leafPoints.add(new Point(7, 9, 2));

        setBush(leafPoints, world, spawnX, spawnZ, groundY, random, rot, originalDim);

        //E side

        cMin = new Point(2, 1, 2);
        cMax = new Point(2, 5, 14);
        setBush(cMin, cMax, world, spawnX, spawnZ, groundY, random, rot, originalDim);


        //S side

        cMin = new Point(3, 2, 18);
        cMax = new Point(13, 5, 18);
        setBush(cMin, cMax, world, spawnX, spawnZ, groundY, random, rot, originalDim);

        leafPoints = new ArrayList<>();
        leafPoints.add(new Point(5, 7, 18));
        leafPoints.add(new Point(11, 7, 18));
        leafPoints.add(new Point(6, 8, 18));
        leafPoints.add(new Point(10, 8, 18));
        leafPoints.add(new Point(7, 9, 18));
        leafPoints.add(new Point(9, 9, 18));

        setBush(leafPoints, world, spawnX, spawnZ, groundY, random, rot, originalDim);


        //W side

        cMin = new Point(14, 1, 2);
        cMax = new Point(14, 5, 14);
        setBush(cMin, cMax, world, spawnX, spawnZ, groundY, random, rot, originalDim);

        //U side

        cMin = new Point(4, 0, 3);
        cMax = new Point(12, 0, 17);

        cMin = rot.rotatePoint(cMin, originalDim.getX(), originalDim.getZ());
        cMax = rot.rotatePoint(cMax, originalDim.getX(), originalDim.getZ());
        Point.order(cMin, cMax);
        for (int x = cMin.getX(); x <= cMax.getX(); x++) {
            for (int z = cMin.getZ(); z <= cMax.getZ(); z++) {
                Block b = world.getBlockAt(spawnX + x, getHighestGroundHeightAt(world, spawnX + x, spawnZ + z) + 1, spawnZ + z);
                if (b.getType() == Material.AIR) {
                    if (random.nextDouble() < 0.15) {
                        setLeaf(b);
                    }
                }
            }
        }

        leafPoints = new ArrayList<>();
        leafPoints.add(new Point(3, 7, 3));
        leafPoints.add(new Point(3, 7, 17));
        leafPoints.add(new Point(13, 7, 3));
        leafPoints.add(new Point(13, 7, 17));


        leafPoints.add(new Point(3, 6, 5));
        leafPoints.add(new Point(3, 6, 6));
        leafPoints.add(new Point(3, 6, 7));
        leafPoints.add(new Point(3, 6, 8));
        leafPoints.add(new Point(3, 6, 9));
        leafPoints.add(new Point(3, 6, 11));
        leafPoints.add(new Point(3, 6, 12));
        leafPoints.add(new Point(3, 6, 13));
        leafPoints.add(new Point(3, 6, 14));
        leafPoints.add(new Point(3, 6, 15));

        leafPoints.add(new Point(13, 6, 5));
        leafPoints.add(new Point(13, 6, 6));
        leafPoints.add(new Point(13, 6, 7));
        leafPoints.add(new Point(13, 6, 8));
        leafPoints.add(new Point(13, 6, 9));
        leafPoints.add(new Point(13, 6, 11));
        leafPoints.add(new Point(13, 6, 12));
        leafPoints.add(new Point(13, 6, 13));
        leafPoints.add(new Point(13, 6, 14));
        leafPoints.add(new Point(13, 6, 15));

        setBush(leafPoints, world, spawnX, spawnZ, groundY, random, rot, originalDim);


    }

    private void setBush(Point cMin, Point cMax, World world, int spawnX, int spawnZ, int groundY, Random random, Rotation rot, Point originalDim) {

        cMin = rot.rotatePoint(cMin, originalDim.getX(), originalDim.getZ());
        cMax = rot.rotatePoint(cMax, originalDim.getX(), originalDim.getZ());
        Point.order(cMin, cMax);
        for (int x = cMin.getX(); x <= cMax.getX(); x++) {
            for (int y = cMin.getY(); y <= cMax.getY(); y++) {
                for (int z = cMin.getZ(); z <= cMax.getZ(); z++) {
                    Block b = world.getBlockAt(spawnX + x, groundY + 1 + y, spawnZ + z);
                    if (b.getType() == Material.AIR) {
                        if (random.nextDouble() < 0.15) {
                            setLeaf(b);
                        }
                    }
                }
            }
        }
    }

    private void setBush(List<Point> points, World world, int spawnX, int spawnZ, int groundY, Random random, Rotation rot, Point originalDim) {

        List<Point> rotatedPoints = new ArrayList<>();
        for (Point p : points) {
            rotatedPoints.add(rot.rotatePoint(p, originalDim.getX(), originalDim.getZ()));
        }

        for (Point p : rotatedPoints) {
            int x = p.getX();
            int y = p.getY();
            int z = p.getZ();
            Block b = world.getBlockAt(spawnX + x, groundY + 1 + y, spawnZ + z);
            if (b.getType() == Material.AIR) {
                if (random.nextDouble() < 0.15) {
                    setLeaf(b);
                }
            }
        }

    }


    private void setLeaf(Block block) {
        block.setType(Material.SPRUCE_LEAVES);
        Leaves leaves = (Leaves) block.getBlockData();
        leaves.setPersistent(true);
        block.setBlockData(leaves);
    }

    private boolean isLiquid(Block b) {
        if (MaterialUtil.WATER.contains(b.getType())) return true;
        if (b.getBlockData() instanceof Waterlogged && ((Waterlogged) b.getBlockData()).isWaterlogged()) return true;
        return false;
    }

    private void setBisected(Block block, Material type, Bisected.Half half) {
        block.setType(type, false);
        Bisected data = (Bisected) block.getBlockData();
        data.setHalf(half);
        block.setBlockData(data);
    }


    //TODO double high blocks no drops
    private void clearGrowth(Block block) {
        Block above = block.getRelative(0, 1, 0);
        while (MaterialUtil.GROWTH.contains(above.getType())) {
            above.setType(Material.AIR);
            above = above.getRelative(0, 1, 0);
        }
    }

    private void clearFlowers(Block block) {
        Block above = block.getRelative(0, 1, 0);
        while (MaterialUtil.FLOWERS.contains(above.getType())) {
            above.setType(Material.AIR);
            above = above.getRelative(0, 1, 0);
        }
    }


}
