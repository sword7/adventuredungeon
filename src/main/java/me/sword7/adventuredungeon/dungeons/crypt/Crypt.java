package me.sword7.adventuredungeon.dungeons.crypt;

import me.sword7.adventuredungeon.dungeon.Dungeon;
import me.sword7.adventuredungeon.dungeon.DungeonTag;
import me.sword7.adventuredungeon.dungeon.DungeonType;
import me.sword7.adventuredungeon.lootpool.LootTag;
import me.sword7.adventuredungeon.mob.DungeonMobType;
import me.sword7.adventuredungeon.mob.Stalfos;
import me.sword7.adventuredungeon.util.BlockColor;
import me.sword7.adventuredungeon.util.math.Cardinal;
import me.sword7.adventuredungeon.util.math.IBound;
import me.sword7.adventuredungeon.util.math.Point;
import me.sword7.adventuredungeon.util.math.Point2D;

import java.util.*;

public class Crypt extends Dungeon {

    private Stalfos.Garment garment;
    private BlockColor blockColor;

    public Crypt(UUID id, String name, UUID worldId, Point entrance, Cardinal direction, Point center, int radius, List<Point2D> grids, Map<Point, List<LootTag>> chestLocations, Map<Point, DungeonMobType> spawnerLocations, Set<UUID> visitors, List<IBound> bounds, Stalfos.Garment garment, BlockColor blockColor) {
        super(id, name, DungeonType.CRYPT, worldId, entrance, direction, center, radius, grids, chestLocations, spawnerLocations, visitors, bounds, Collections.singletonList(CryptTag.fromColorBlock(blockColor)), CryptLootPoolSupplier.getLootPool(), new CryptBestiary(blockColor, garment));
        this.garment = garment;
        this.blockColor = blockColor;
    }

    public Stalfos.Garment getGarment() {
        return garment;
    }

    public BlockColor getColor() {
        return blockColor;
    }


    public enum CryptTag implements DungeonTag {
        RED,
        GREEN,
        BLUE,
        PURPLE,

        ;

        public static CryptTag fromColorBlock(BlockColor color) {
            switch (color) {
                case GREEN:
                    return GREEN;
                case BLUE:
                    return BLUE;
                case PURPLE:
                    return PURPLE;
                default:
                    return RED;
            }
        }

    }


}
