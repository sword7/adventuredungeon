package me.sword7.adventuredungeon.dungeons.crypt;

import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.dungeons.crypt.theme.BlackBrickTheme;
import me.sword7.adventuredungeon.dungeons.crypt.theme.CoralTheme;
import me.sword7.adventuredungeon.dungeons.crypt.theme.CryptTheme;
import me.sword7.adventuredungeon.structure.ITheme;
import me.sword7.adventuredungeon.structure.IThemeFactory;
import me.sword7.adventuredungeon.util.BlockColor;
import org.bukkit.block.Biome;

import java.util.Random;

public class CryptThemeFactory implements IThemeFactory {

    @Override
    public ITheme select(Biome biome, Random random) {

        BlockColor color = BlockColor.RED;
        double r = random.nextDouble();
        if (r < 0.15) {
            color = BlockColor.PURPLE;
        } else if (r < 0.30) {
            color = BlockColor.BLUE;
        } else if (r < 0.45) {
            color = BlockColor.GREEN;
        }

        if (biome == Biome.BEACH) {
            return new CoralTheme(color);
        } else {
            double roll = random.nextDouble();
            if (roll < 0.01) {
                return new CoralTheme(color);
            } else if (roll < 0.13) {
                return Version.getValue() >= 116 ? new BlackBrickTheme(color) : new CryptTheme(color);
            } else {
                return new CryptTheme(color);
            }
        }
    }


}
