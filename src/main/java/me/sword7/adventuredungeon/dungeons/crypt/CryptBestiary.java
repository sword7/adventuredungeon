package me.sword7.adventuredungeon.dungeons.crypt;

import me.sword7.adventuredungeon.mob.DungeonMobType;
import me.sword7.adventuredungeon.mob.IBestiary;
import me.sword7.adventuredungeon.mob.Stalfos;
import me.sword7.adventuredungeon.util.BlockColor;
import org.bukkit.Location;

import java.util.Random;

public class CryptBestiary implements IBestiary {

    private BlockColor blockColor;
    private Stalfos.Garment garment;

    public CryptBestiary(BlockColor blockColor, Stalfos.Garment garment) {
        this.blockColor = blockColor;
        this.garment = garment;
    }

    @Override
    public void spawn(DungeonMobType mobType, Location location, Random random) {
        if (mobType == DungeonMobType.STALFOS) {
            Stalfos stalfos = (Stalfos) mobType.getDungeonMob();
            stalfos.spawnColored(location, garment, blockColor, random);
        } else {
            mobType.getDungeonMob().spawn(location, random);
        }
    }

    @Override
    public double getAmbientSpawnChance() {
        return 1.0;
    }

    @Override
    public double getOverrideSpawnChance() {
        return 0.66;
    }

    @Override
    public void ambientSpawn(Location location, Random random) {
        Stalfos stalfos = (Stalfos) DungeonMobType.STALFOS.getDungeonMob();
        stalfos.spawnColored(location, garment, blockColor, random);
    }

}
