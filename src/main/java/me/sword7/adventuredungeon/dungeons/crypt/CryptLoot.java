package me.sword7.adventuredungeon.dungeons.crypt;

import me.sword7.adventuredungeon.dungeon.DungeonTag;
import me.sword7.adventuredungeon.lootpool.LootTag;
import me.sword7.adventuredungeon.lootpool.item.ItemCollection;
import me.sword7.adventuredungeon.lootpool.item.ItemSingleton;
import me.sword7.adventuredungeon.lootpool.item.ItemWeightedAmount;
import me.sword7.adventuredungeon.lootpool.loot.FlavoredLoot;
import me.sword7.adventuredungeon.util.weight.WeightedChoice;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Random;

public class CryptLoot extends FlavoredLoot {

    private static ItemCollection remains;
    private static ItemCollection garmentsRed;
    private static ItemCollection garmentsBlue;
    private static ItemCollection garmentsPurple;
    private static ItemCollection garmentsGreen;
    private static ItemCollection caveIn;
    private static ItemCollection web;
    //cerminony (candles)

    public static void init() {
        //REMAINS
        remains = new ItemCollection();

        ItemWeightedAmount bones = createWeights(Material.BONE);
        ItemWeightedAmount flesh = createWeights(Material.ROTTEN_FLESH);
        ItemSingleton skull = new ItemSingleton(new ItemStack(Material.SKELETON_SKULL));

        remains.addItem(new WeightedChoice<>(bones, 20));
        remains.addItem(new WeightedChoice<>(skull, 1));
        remains.addItem(new WeightedChoice<>(flesh, 5));


        //CAVE IN
        caveIn = new ItemCollection();

        ItemWeightedAmount dirt = createWeights(Material.DIRT);
        ItemWeightedAmount coarseDirt = createWeights(Material.COARSE_DIRT);
        ItemWeightedAmount andesite = createWeights(Material.ANDESITE);
        ItemWeightedAmount stone = createWeights(Material.STONE);

        caveIn.addItem(new WeightedChoice<>(dirt, 6));
        caveIn.addItem(new WeightedChoice<>(coarseDirt, 6));
        caveIn.addItem(new WeightedChoice<>(andesite, 5));
        caveIn.addItem(new WeightedChoice<>(stone, 5));
        caveIn.addItem(new WeightedChoice<>(bones, 5));
        caveIn.addItem(new WeightedChoice<>(flesh, 1));

        //GARMENTS
        ItemCollection garments = new ItemCollection();

        ItemWeightedAmount carpetDark1 = createWeights(Material.GRAY_CARPET);
        ItemWeightedAmount carpetDark2 = createWeights(Material.BLACK_CARPET);
        ItemWeightedAmount carpetLight = createWeights(Material.LIGHT_GRAY_CARPET);

        garments.addItem(new WeightedChoice<>(carpetDark1, 4));
        garments.addItem(new WeightedChoice<>(carpetDark2, 4));
        garments.addItem(new WeightedChoice<>(carpetLight, 2));
        garments.addItem(new WeightedChoice<>(bones, 2));
        garments.addItem(new WeightedChoice<>(flesh, 1));

        garmentsRed = garments.clone();
        garmentsRed.addItem(new WeightedChoice<>(createWeights(Material.RED_CARPET), 6));
        garmentsGreen = garments.clone();
        garmentsGreen.addItem(new WeightedChoice<>(createWeights(Material.GREEN_CARPET), 6));
        garmentsBlue = garments.clone();
        garmentsBlue.addItem(new WeightedChoice<>(createWeights(Material.BLUE_CARPET), 6));
        garmentsPurple = garments.clone();
        garmentsPurple.addItem(new WeightedChoice<>(createWeights(Material.PURPLE_CARPET), 6));

        //WEB
        web = new ItemCollection();

        ItemWeightedAmount string = createWeights(Material.STRING);
        ItemSingleton cobweb = new ItemSingleton(new ItemStack(Material.COBWEB));

        web.addItem(new WeightedChoice<>(string, 7));
        web.addItem(new WeightedChoice<>(cobweb, 2));
        web.addItem(new WeightedChoice<>(bones, 2));
        web.addItem(new WeightedChoice<>(flesh, 1));

    }

    private static ItemWeightedAmount createWeights(Material material) {
        ItemWeightedAmount item = new ItemWeightedAmount(new ItemStack(material));
        item.addAmount(1, 15);
        item.addAmount(2, 4);
        item.addAmount(3, 1);
        return item;
    }

    public CryptLoot(ItemCollection treasure) {
        super(treasure);
    }

    public CryptLoot(ItemCollection treasure, double minItem, double maxItem, double minTreasure, double maxTreasure) {
        super(treasure, minItem, maxItem, minTreasure, maxTreasure);
    }

    @Override
    protected ItemCollection selectFlavor(Random rand, List<DungeonTag> dungeonTags, List<LootTag> lootTags) {

        double r = rand.nextDouble();
        if (r < 0.22) {
            return caveIn;
        } else if (r < 0.44) {
            if (dungeonTags.contains(Crypt.CryptTag.GREEN)) {
                return garmentsGreen;
            } else if (dungeonTags.contains(Crypt.CryptTag.BLUE)) {
                return garmentsBlue;
            } else if (dungeonTags.contains(Crypt.CryptTag.PURPLE)) {
                return garmentsPurple;
            } else {
                return garmentsRed;
            }
        } else if (r < 0.66) {
            return web;
        } else {
            return remains;
        }

    }

}
