package me.sword7.adventuredungeon.dungeons.crypt;

import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.lootpool.LootPool;
import me.sword7.adventuredungeon.lootpool.LootUtil;
import me.sword7.adventuredungeon.lootpool.item.*;
import me.sword7.adventuredungeon.lootpool.loot.ILoot;
import me.sword7.adventuredungeon.lootpool.loot.LootGroup;
import me.sword7.adventuredungeon.util.weight.WeightedChoice;
import me.sword7.adventuredungeon.util.weight.WeightedRandomizer;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

public class CryptLootPoolSupplier {

    private static LootPool lootPool = new CryptLootPool();

    public static void init() {
        buildPool(lootPool);
    }

    public static LootPool getLootPool() {
        return lootPool;
    }

    private static void buildPool(LootPool pool) {

        //=== ITEM COLLECTIONS ===
        ItemCollection gemsAndMetals = new ItemCollection();
        gemsAndMetals.addItem(LootUtil.DIAMOND, 2);
        gemsAndMetals.addItem(LootUtil.EMERALDS, 3);
        gemsAndMetals.addItem(LootUtil.GOLD_INGOTS, 7);
        gemsAndMetals.addItem(LootUtil.IRON_INGOTS, 7);

        ItemCollection armorItems = getArmorItems();
        ItemCollection weaponItems = getWeapons();

        ItemCollection miscItems = new ItemCollection();
        miscItems.addItem(Material.FEATHER, 1);
        miscItems.addItem(Material.BOWL, 1);
        miscItems.addItem(Material.ARROW, 1);
        miscItems.addItem(Material.FLINT, 1);
        miscItems.addItem(Material.STICK, 1);
        miscItems.addItem(Material.STRING, 1);
        miscItems.addItem(Material.ROTTEN_FLESH, 1);
        miscItems.addItem(Material.SPIDER_EYE, 1);
        miscItems.addItem(new ItemRangedAmount(Material.WHEAT, 1, 6), 1);
        miscItems.addItem(Material.REDSTONE, 1);
        miscItems.addItem(Material.GUNPOWDER, 1);
        miscItems.addItem(Material.BUCKET, 1);
        miscItems.addItem(Material.PAINTING, 1);
        miscItems.addItem(Material.IRON_NUGGET, 1);
        miscItems.addItem(Material.GOLD_NUGGET, 1);
        miscItems.addItem(Material.LEATHER, 1);
        miscItems.addItem(Material.PAPER, 1);
        miscItems.addItem(Material.SLIME_BALL, 1);
        miscItems.addItem(Material.LAPIS_LAZULI, 1);
        miscItems.addItem(Material.GOLDEN_CARROT, 1);
        miscItems.addItem(Material.SKELETON_SKULL, 1);
        miscItems.addItem(Material.MAP, 1);
        miscItems.addItem(Material.ITEM_FRAME, 1);
        miscItems.addItem(Material.BONE, 1);
        miscItems.addItem(Material.FIRE_CHARGE, 1);
        miscItems.addItem(Material.ENDER_PEARL, 1);
        miscItems.addItem(Material.GLASS_BOTTLE, 1);
        miscItems.addItem(Material.SUGAR, 1);
        miscItems.addItem(LootUtil.SEEDS, 1);
        miscItems.addItem(Material.BOOK, 1);
        miscItems.addItem(Material.FLINT_AND_STEEL, 1);
        miscItems.addItem(Material.IRON_HORSE_ARMOR, 1);
        miscItems.addItem(Material.NAME_TAG, 1);
        miscItems.addItem(Material.SADDLE, 1);
        miscItems.addItem(Material.LEAD, 1);
        miscItems.addItem(Material.SKULL_BANNER_PATTERN, 1);

        WeightedRandomizer<Enchantment> enchants = new WeightedRandomizer<>();
        enchants.add(new WeightedChoice<>(Enchantment.PROTECTION_ENVIRONMENTAL, 3));
        enchants.add(new WeightedChoice<>(Enchantment.PROTECTION_PROJECTILE, 2));
        enchants.add(new WeightedChoice<>(Enchantment.PROTECTION_FIRE, 2));
        enchants.add(new WeightedChoice<>(Enchantment.PROTECTION_FALL, 3));
        enchants.add(new WeightedChoice<>(Enchantment.THORNS, 3));
        if (Version.getValue() >= 116) enchants.add(new WeightedChoice<>(Enchantment.SOUL_SPEED, 3));
        enchants.add(new WeightedChoice<>(Enchantment.DAMAGE_ALL, 3));
        enchants.add(new WeightedChoice<>(Enchantment.DAMAGE_UNDEAD, 3));
        enchants.add(new WeightedChoice<>(Enchantment.KNOCKBACK, 3));
        enchants.add(new WeightedChoice<>(Enchantment.ARROW_KNOCKBACK, 3));
        enchants.add(new WeightedChoice<>(Enchantment.LOOT_BONUS_MOBS, 3));
        enchants.add(new WeightedChoice<>(Enchantment.SWEEPING_EDGE, 3));
        enchants.add(new WeightedChoice<>(Enchantment.DIG_SPEED, 3));
        enchants.add(new WeightedChoice<>(Enchantment.SILK_TOUCH, 3));
        enchants.add(new WeightedChoice<>(Enchantment.DURABILITY, 3));
        enchants.add(new WeightedChoice<>(Enchantment.ARROW_DAMAGE, 3));
        enchants.add(new WeightedChoice<>(Enchantment.LOOT_BONUS_BLOCKS, 3));
        enchants.add(new WeightedChoice<>(Enchantment.ARROW_FIRE, 3));
        enchants.add(new WeightedChoice<>(Enchantment.ARROW_INFINITE, 3));
        enchants.add(new WeightedChoice<>(Enchantment.MENDING, 3));
        enchants.add(new WeightedChoice<>(Enchantment.VANISHING_CURSE, 1));
        enchants.add(new WeightedChoice<>(Enchantment.BINDING_CURSE, 1));
        ILootItem enchantedBook = new EnchantedBook(enchants);

        //=== CHEST TYPES ===

        //MAIN
        ItemCollection mainCollection = new ItemCollection();
        mainCollection.addItem(new WeightedChoice<>(gemsAndMetals, 6));
        mainCollection.addItem(new WeightedChoice<>(enchantedBook, 2));
        mainCollection.addItem(new WeightedChoice<>(armorItems, 1));
        mainCollection.addItem(new WeightedChoice<>(weaponItems, 1));
        mainCollection.addItem(new WeightedChoice<>(miscItems, 8));

        //TREASURE

        ItemCollection extrasCollection = new ItemCollection();
        extrasCollection.addItem(new WeightedChoice<>(miscItems, 5));
        extrasCollection.addItem(new WeightedChoice<>(enchantedBook, 2));
        extrasCollection.addItem(new WeightedChoice<>(armorItems, 1));
        extrasCollection.addItem(new WeightedChoice<>(weaponItems, 1));

        ItemCollection goldHeavy = new ItemCollection();
        goldHeavy.addItem(new WeightedChoice<>(LootUtil.DIAMOND, 2));
        goldHeavy.addItem(new WeightedChoice<>(LootUtil.EMERALDS, 3));
        goldHeavy.addItem(new WeightedChoice<>(LootUtil.GOLD_INGOTS, 16));
        goldHeavy.addItem(new WeightedChoice<>(LootUtil.IRON_INGOTS, 4));

        ItemCollection emeraldHeavy = new ItemCollection();
        emeraldHeavy.addItem(new WeightedChoice<>(LootUtil.DIAMOND, 1));
        emeraldHeavy.addItem(new WeightedChoice<>(LootUtil.EMERALDS, 17));
        emeraldHeavy.addItem(new WeightedChoice<>(LootUtil.IRON_INGOTS, 5));

        ItemCollection metals = new ItemCollection();
        metals.addItem(new WeightedChoice<>(LootUtil.GOLD_INGOTS, 13));
        metals.addItem(new WeightedChoice<>(LootUtil.IRON_INGOTS, 8));

        ItemCollection ironHeavy = new ItemCollection();
        ironHeavy.addItem(new WeightedChoice<>(LootUtil.DIAMOND, 2));
        ironHeavy.addItem(new WeightedChoice<>(LootUtil.EMERALDS, 3));
        ironHeavy.addItem(new WeightedChoice<>(LootUtil.GOLD_INGOTS, 4));
        ironHeavy.addItem(new WeightedChoice<>(LootUtil.IRON_INGOTS, 16));

        ItemCollection treasureCollection = new ItemCollection();
        treasureCollection.addItem(new WeightedChoice<>(gemsAndMetals, 48));
        treasureCollection.addItem(new WeightedChoice<>(extrasCollection, 3));
        ILoot treasure1 = new CryptLoot(treasureCollection, 0.4, 0.98, 0.6, 0.95);

        treasureCollection = new ItemCollection();
        treasureCollection.addItem(new WeightedChoice<>(goldHeavy, 48));
        treasureCollection.addItem(new WeightedChoice<>(extrasCollection, 3));
        ILoot treasure2 = new CryptLoot(treasureCollection, 0.4, 0.98, 0.6, 0.95);

        treasureCollection = new ItemCollection();
        treasureCollection.addItem(new WeightedChoice<>(emeraldHeavy, 48));
        treasureCollection.addItem(new WeightedChoice<>(extrasCollection, 3));
        ILoot treasure3 = new CryptLoot(treasureCollection, 0.4, 0.98, 0.6, 0.95);

        treasureCollection = new ItemCollection();
        treasureCollection.addItem(new WeightedChoice<>(metals, 48));
        treasureCollection.addItem(new WeightedChoice<>(extrasCollection, 3));
        ILoot treasure4 = new CryptLoot(treasureCollection, 0.4, 0.98, 0.6, 0.95);

        treasureCollection = new ItemCollection();
        treasureCollection.addItem(new WeightedChoice<>(ironHeavy, 48));
        treasureCollection.addItem(new WeightedChoice<>(extrasCollection, 3));
        ILoot treasure5 = new CryptLoot(treasureCollection, 0.4, 0.98, 0.6, 0.95);

        LootGroup treasure = new LootGroup();
        treasure.add(new WeightedChoice<>(treasure1, 1));
        treasure.add(new WeightedChoice<>(treasure2, 1));
        treasure.add(new WeightedChoice<>(treasure3, 1));
        treasure.add(new WeightedChoice<>(treasure4, 1));
        treasure.add(new WeightedChoice<>(treasure5, 1));

        //ARMORY

        ItemCollection armor = new ItemCollection();
        armor.addItem(new WeightedChoice<>(gemsAndMetals, 4));
        armor.addItem(new WeightedChoice<>(enchantedBook, 2));
        armor.addItem(new WeightedChoice<>(weaponItems, 2));
        armor.addItem(new WeightedChoice<>(miscItems, 4));
        armor.addItem(new WeightedChoice<>(armorItems, 30));

        //WEAPONRY

        ItemCollection weapons = new ItemCollection();
        weapons.addItem(new WeightedChoice<>(gemsAndMetals, 4));
        weapons.addItem(new WeightedChoice<>(enchantedBook, 2));
        weapons.addItem(new WeightedChoice<>(armorItems, 2));
        weapons.addItem(new WeightedChoice<>(miscItems, 4));
        weapons.addItem(new WeightedChoice<>(weaponItems, 30));

        //SCROLLS

        //SUPPLY

        //URN CHEST


        pool.add(new WeightedChoice<>(new CryptLoot(mainCollection), 5));
        pool.add(new WeightedChoice<>(treasure, 2));
        pool.add(new WeightedChoice<>(new CryptLoot(armor, 0.45, 0.75, 0.33, 0.66), 1));
        pool.add(new WeightedChoice<>(new CryptLoot(weapons, 0.45, 0.75, 0.33, 0.66), 1));
    }


    private static ItemCollection getArmorItems() {
        WeightedRandomizer<Enchantment> helmetEnchants = new WeightedRandomizer<>();
        helmetEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_ENVIRONMENTAL, 10));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_PROJECTILE, 4));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_FIRE, 4));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.THORNS, 4));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.DURABILITY, 8));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.MENDING, 2));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.VANISHING_CURSE, 1));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.BINDING_CURSE, 1));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.WATER_WORKER, 4));
        helmetEnchants.add(new WeightedChoice<>(Enchantment.OXYGEN, 4));

        WeightedRandomizer<Enchantment> middleWareEnchants = new WeightedRandomizer<>();
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_ENVIRONMENTAL, 10));
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_PROJECTILE, 4));
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_FIRE, 4));
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.THORNS, 4));
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.DURABILITY, 8));
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.MENDING, 2));
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.VANISHING_CURSE, 1));
        middleWareEnchants.add(new WeightedChoice<>(Enchantment.BINDING_CURSE, 1));

        WeightedRandomizer<Enchantment> bootEnchants = new WeightedRandomizer<>();
        bootEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_ENVIRONMENTAL, 10));
        bootEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_PROJECTILE, 4));
        bootEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_FIRE, 4));
        bootEnchants.add(new WeightedChoice<>(Enchantment.PROTECTION_FALL, 8));
        bootEnchants.add(new WeightedChoice<>(Enchantment.DURABILITY, 8));
        bootEnchants.add(new WeightedChoice<>(Enchantment.THORNS, 4));
        if (Version.getValue() >= 116) bootEnchants.add(new WeightedChoice<>(Enchantment.SOUL_SPEED, 4));
        bootEnchants.add(new WeightedChoice<>(Enchantment.FROST_WALKER, 2));
        bootEnchants.add(new WeightedChoice<>(Enchantment.DEPTH_STRIDER, 2));
        bootEnchants.add(new WeightedChoice<>(Enchantment.MENDING, 2));
        bootEnchants.add(new WeightedChoice<>(Enchantment.VANISHING_CURSE, 1));
        bootEnchants.add(new WeightedChoice<>(Enchantment.BINDING_CURSE, 1));

        ItemCollection enchantedDiamondArmor = new ItemCollection();
        enchantedDiamondArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.DIAMOND_HELMET, helmetEnchants), 1));
        enchantedDiamondArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.DIAMOND_CHESTPLATE, middleWareEnchants), 1));
        enchantedDiamondArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.DIAMOND_LEGGINGS, middleWareEnchants), 1));
        enchantedDiamondArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.DIAMOND_BOOTS, bootEnchants), 1));

        ItemCollection diamondArmor = new ItemCollection();
        diamondArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.DIAMOND_HELMET), 1));
        diamondArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.DIAMOND_CHESTPLATE), 1));
        diamondArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.DIAMOND_LEGGINGS), 1));
        diamondArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.DIAMOND_BOOTS), 1));

        ItemCollection enchantedIronArmor = new ItemCollection();
        enchantedIronArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.IRON_HELMET, helmetEnchants), 1));
        enchantedIronArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.IRON_CHESTPLATE, middleWareEnchants), 1));
        enchantedIronArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.IRON_LEGGINGS, middleWareEnchants), 1));
        enchantedIronArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.IRON_BOOTS, bootEnchants), 1));

        ItemCollection ironArmor = new ItemCollection();
        ironArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.IRON_HELMET), 1));
        ironArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.IRON_CHESTPLATE), 1));
        ironArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.IRON_LEGGINGS), 1));
        ironArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.IRON_BOOTS), 1));

        ItemCollection enchantedChainArmor = new ItemCollection();
        enchantedChainArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.CHAINMAIL_HELMET, helmetEnchants), 1));
        enchantedChainArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.CHAINMAIL_CHESTPLATE, middleWareEnchants), 1));
        enchantedChainArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.CHAINMAIL_LEGGINGS, middleWareEnchants), 1));
        enchantedChainArmor.addItem(new WeightedChoice<>(new EnchantedItem(Material.CHAINMAIL_BOOTS, bootEnchants), 1));

        ItemCollection chainArmor = new ItemCollection();
        chainArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.CHAINMAIL_HELMET), 1));
        chainArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.CHAINMAIL_CHESTPLATE), 1));
        chainArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.CHAINMAIL_LEGGINGS), 1));
        chainArmor.addItem(new WeightedChoice<>(new ItemSingleton(Material.CHAINMAIL_BOOTS), 1));

        ItemCollection armorItems = new ItemCollection();
        armorItems.addItem(new WeightedChoice<>(enchantedDiamondArmor, 1));
        armorItems.addItem(new WeightedChoice<>(diamondArmor, 3));
        armorItems.addItem(new WeightedChoice<>(enchantedIronArmor, 2));
        armorItems.addItem(new WeightedChoice<>(ironArmor, 6));
        armorItems.addItem(new WeightedChoice<>(enchantedChainArmor, 1));
        armorItems.addItem(new WeightedChoice<>(chainArmor, 3));

        return armorItems;
    }


    private static ItemCollection getWeapons() {

        WeightedRandomizer<Enchantment> swordEnchantments = new WeightedRandomizer<>();
        swordEnchantments.add(new WeightedChoice<>(Enchantment.DAMAGE_ALL, 10));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.DURABILITY, 6));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.DAMAGE_UNDEAD, 4));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.KNOCKBACK, 4));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.LOOT_BONUS_MOBS, 4));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.SWEEPING_EDGE, 4));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.FIRE_ASPECT, 4));
        swordEnchantments.add(new WeightedChoice<>(Enchantment.MENDING, 1));

        WeightedRandomizer<Enchantment> axeEnchantments = new WeightedRandomizer<>();
        axeEnchantments.add(new WeightedChoice<>(Enchantment.DAMAGE_ALL, 10));
        axeEnchantments.add(new WeightedChoice<>(Enchantment.DURABILITY, 6));
        axeEnchantments.add(new WeightedChoice<>(Enchantment.DAMAGE_UNDEAD, 4));
        axeEnchantments.add(new WeightedChoice<>(Enchantment.MENDING, 1));

        WeightedRandomizer<Enchantment> bowEnchantments = new WeightedRandomizer<>();
        bowEnchantments.add(new WeightedChoice<>(Enchantment.ARROW_DAMAGE, 6));
        bowEnchantments.add(new WeightedChoice<>(Enchantment.ARROW_KNOCKBACK, 4));
        bowEnchantments.add(new WeightedChoice<>(Enchantment.ARROW_FIRE, 4));
        bowEnchantments.add(new WeightedChoice<>(Enchantment.ARROW_INFINITE, 4));
        bowEnchantments.add(new WeightedChoice<>(Enchantment.DURABILITY, 4));
        bowEnchantments.add(new WeightedChoice<>(Enchantment.MENDING, 1));

        LootItem enchantedDiamondSword = new EnchantedItem(Material.DIAMOND_SWORD, swordEnchantments);
        LootItem diamondSword = new ItemSingleton(Material.DIAMOND_SWORD);

        LootItem enchantedDiamondAxe = new EnchantedItem(Material.DIAMOND_AXE, axeEnchantments);
        LootItem diamondAxe = new ItemSingleton(Material.DIAMOND_AXE);

        LootItem enchantedIronSword = new EnchantedItem(Material.IRON_SWORD, swordEnchantments);
        LootItem ironSword = new ItemSingleton(Material.IRON_SWORD);

        LootItem enchantedIronAxe = new EnchantedItem(Material.IRON_AXE, axeEnchantments);
        LootItem ironAxe = new ItemSingleton(Material.IRON_AXE);

        LootItem enchantedBow = new EnchantedItem(Material.BOW, bowEnchantments);
        LootItem bow = new ItemSingleton(Material.BOW);

        ItemCollection weaponItems = new ItemCollection();
        weaponItems.addItem(new WeightedChoice<>(enchantedDiamondSword, 3));
        weaponItems.addItem(new WeightedChoice<>(diamondSword, 9));
        weaponItems.addItem(new WeightedChoice<>(enchantedDiamondAxe, 1));
        weaponItems.addItem(new WeightedChoice<>(diamondAxe, 3));


        weaponItems.addItem(new WeightedChoice<>(enchantedIronSword, 9));
        weaponItems.addItem(new WeightedChoice<>(ironSword, 27));
        weaponItems.addItem(new WeightedChoice<>(enchantedIronAxe, 3));
        weaponItems.addItem(new WeightedChoice<>(ironAxe, 9));

        weaponItems.addItem(new WeightedChoice<>(enchantedBow, 4));
        weaponItems.addItem(new WeightedChoice<>(bow, 12));
        weaponItems.addItem(new WeightedChoice<>(LootUtil.ARROWS, 32));

        //sword/axe type
        //bow/arrow type
        //mix

        return weaponItems;
    }


    private static ItemCollection getPotionArrows() {
        return new ItemCollection();
    }

    private static ItemCollection getPotions() {
        return new ItemCollection();
    }

    private static ItemCollection getScrolls() {
        //books
        //paper
        //maps
        //banner patterns
        //enchanted books
        return new ItemCollection();
    }

    private static ItemCollection getStorage() {
        //food
        //**different flavor
        //books/scrolls
        //potions
        //misc items
        return new ItemCollection();
    }

    private static ItemCollection getCeremony() {
        //redstone torches
        //banners
        //pattens
        //gems
        //gold
        //armor/weapons
        //seeds
        //carpets
        //remains
        return new ItemCollection();
    }


}
