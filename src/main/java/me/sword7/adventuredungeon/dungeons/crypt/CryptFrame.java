package me.sword7.adventuredungeon.dungeons.crypt;

import me.sword7.adventuredungeon.dungeon.DungeonType;
import me.sword7.adventuredungeon.structure.Decoration;
import me.sword7.adventuredungeon.structure.IFrame;
import me.sword7.adventuredungeon.structure.StructFlatFile;
import me.sword7.adventuredungeon.structure.Structure;
import me.sword7.adventuredungeon.util.math.Cardinal;
import me.sword7.adventuredungeon.util.math.Point;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public enum CryptFrame implements IFrame {

    sN,
    sNE,
    sNS,
    sNEW,
    sNSEW,
    nNE,
    nNS,
    nNEW,
    nNSEW,
    hNE,
    hNS,
    hNEW,
    hNSEW,
    rN,
    rNNSEW,
    rNNW,
    rNS,
    rSE,
    lEEW,
    lSE1,
    lSE2,
    lSW,
    lW,
    bsN,
    bsNE,
    bsSW,
    bsSEW,
    bsNSSW,
    stNN,
    stNS,
    ;

    private Point dimensions;
    private int doors;
    private Structure structure;
    private Map<Point, Set<Cardinal>> gridCoordToEntrances = new HashMap<>();
    private Decoration[] decorations;

    public static void init() {

        for (CryptFrame frame : values()) {
            String frameString = frame.toString();
            if (frameString.startsWith("st")) {
                frame.dimensions = new Point(1, 2, 1);
            } else if (frameString.startsWith("r")) {
                frame.dimensions = new Point(2, 1, 1);
            } else if (frameString.startsWith("bs") || frameString.startsWith("l")) {
                frame.dimensions = new Point(2, 1, 2);
            } else {
                frame.dimensions = new Point(1, 1, 1);
            }
            frame.doors = (int) frameString.chars().filter(c -> Character.isUpperCase(c)).count();
            frame.structure = StructFlatFile.fetch("crypt/frame", frameString);
            frame.decorations = Decoration.load(frame);

        }

        sN.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N));
        sNE.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.E));
        hNE.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.E));
        nNE.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.E));
        sNS.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S));
        hNS.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S));
        nNS.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S));
        sNEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.E, Cardinal.W));
        hNEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.E, Cardinal.W));
        nNEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.E, Cardinal.W));
        sNSEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S, Cardinal.E, Cardinal.W));
        hNSEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S, Cardinal.E, Cardinal.W));
        nNSEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S, Cardinal.E, Cardinal.W));

        rSE.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.S));
        rSE.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.E));

        rN.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet());
        rN.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.N));

        rNNW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.W));
        rNNW.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.N));

        rNS.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.S));
        rNS.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.N));

        rNNSEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.W));
        rNNSEW.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.N, Cardinal.S, Cardinal.E));

        lSW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.W));
        lSW.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.S));
        lSW.gridCoordToEntrances.put(new Point(0, 0, 1), Cardinal.buildSet());

        lSE1.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet());
        lSE1.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.S, Cardinal.E));
        lSE1.gridCoordToEntrances.put(new Point(0, 0, 1), Cardinal.buildSet());

        lEEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet());
        lEEW.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.E));
        lEEW.gridCoordToEntrances.put(new Point(0, 0, 1), Cardinal.buildSet(Cardinal.E, Cardinal.W));

        lSE2.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet());
        lSE2.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.E));
        lSE2.gridCoordToEntrances.put(new Point(0, 0, 1), Cardinal.buildSet(Cardinal.S));

        lW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.W));
        lW.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet());
        lW.gridCoordToEntrances.put(new Point(0, 0, 1), Cardinal.buildSet());

        bsN.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet());
        bsN.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.N));
        bsN.gridCoordToEntrances.put(new Point(0, 0, 1), Cardinal.buildSet());
        bsN.gridCoordToEntrances.put(new Point(1, 0, 1), Cardinal.buildSet());

        bsSW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.W));
        bsSW.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet());
        bsSW.gridCoordToEntrances.put(new Point(0, 0, 1), Cardinal.buildSet());
        bsSW.gridCoordToEntrances.put(new Point(1, 0, 1), Cardinal.buildSet(Cardinal.S));

        bsNE.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N));
        bsNE.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.E));
        bsNE.gridCoordToEntrances.put(new Point(0, 0, 1), Cardinal.buildSet());
        bsNE.gridCoordToEntrances.put(new Point(1, 0, 1), Cardinal.buildSet());

        bsSEW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet());
        bsSEW.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.E));
        bsSEW.gridCoordToEntrances.put(new Point(0, 0, 1), Cardinal.buildSet(Cardinal.W));
        bsSEW.gridCoordToEntrances.put(new Point(1, 0, 1), Cardinal.buildSet(Cardinal.S));

        bsNSSW.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.W));
        bsNSSW.gridCoordToEntrances.put(new Point(1, 0, 0), Cardinal.buildSet(Cardinal.N));
        bsNSSW.gridCoordToEntrances.put(new Point(0, 0, 1), Cardinal.buildSet(Cardinal.S));
        bsNSSW.gridCoordToEntrances.put(new Point(1, 0, 1), Cardinal.buildSet(Cardinal.S));

        stNN.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.N));
        stNN.gridCoordToEntrances.put(new Point(0, 1, 0), Cardinal.buildSet(Cardinal.N));

        stNS.gridCoordToEntrances.put(new Point(0, 0, 0), Cardinal.buildSet(Cardinal.S));
        stNS.gridCoordToEntrances.put(new Point(0, 1, 0), Cardinal.buildSet(Cardinal.N));

    }


    @Override
    public Point getDimensions() {
        return dimensions;
    }

    @Override
    public int getDoors() {
        return doors;
    }

    @Override
    public Structure getStructure() {
        return structure;
    }

    @Override
    public Map<Point, Set<Cardinal>> getGridCoordToEntrances() {
        return gridCoordToEntrances;
    }

    @Override
    public Decoration selectDecoration(Random random) {
        return decorations.length > 0 ? decorations[(int) (Math.random() * decorations.length)] : null;
    }

    @Override
    public DungeonType getDungeonType() {
        return DungeonType.CRYPT;
    }


}
