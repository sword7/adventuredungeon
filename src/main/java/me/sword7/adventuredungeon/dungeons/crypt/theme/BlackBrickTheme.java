package me.sword7.adventuredungeon.dungeons.crypt.theme;

import me.sword7.adventuredungeon.structure.block.BlockWeightedJumble;
import me.sword7.adventuredungeon.structure.block.DungeonMaterial;
import me.sword7.adventuredungeon.util.BlockColor;
import org.bukkit.Material;

public class BlackBrickTheme extends CryptTheme {

    public BlackBrickTheme(BlockColor blockColor) {
        super(blockColor);
        init();
    }

    private void init() {
        secondaryWall = new BlockWeightedJumble().add(Material.POLISHED_GRANITE, 15).add(Material.GRANITE, 2).add(Material.BRICKS, 2);
        rareWall1 = new BlockWeightedJumble().add(Material.OBSIDIAN, 17).add(Material.BRICKS, 2);
        rareWall2 = new BlockWeightedJumble().add(Material.RED_NETHER_BRICKS, 17).add(Material.BRICKS, 2);

        let(DungeonMaterial.PRIMARY, Material.BRICKS);
        let(DungeonMaterial.PRIMARY_STAIR, Material.BRICK_STAIRS);
        let(DungeonMaterial.PRIMARY_SLAB, Material.BRICK_SLAB);
        let(DungeonMaterial.PRIMARY_WALL, Material.BRICK_WALL);

        let(DungeonMaterial.HIGHLIGHT, Material.POLISHED_BLACKSTONE);
        let(DungeonMaterial.HIGHLIGHT_STAIR, Material.POLISHED_BLACKSTONE_STAIRS);
        let(DungeonMaterial.HIGHLIGHT_SLAB, Material.POLISHED_BLACKSTONE_SLAB);

        if (random.nextDouble() < 0.66) {
            let(DungeonMaterial.DETAIL, Material.CHISELED_POLISHED_BLACKSTONE);
        } else {
            let(DungeonMaterial.DETAIL, Material.CHISELED_STONE_BRICKS);
        }
        let(DungeonMaterial.CRACKED_WALL, Material.CRACKED_POLISHED_BLACKSTONE_BRICKS);
        let(DungeonMaterial.CEILING, Material.BLACKSTONE);

        let(DungeonMaterial.FLOOR_OUTSIDE, Material.POLISHED_BLACKSTONE);
        let(DungeonMaterial.FLOOR_OUTSIDE_STAIR, Material.POLISHED_BLACKSTONE_STAIRS);
        let(DungeonMaterial.FLOOR_OUTSIDE_SLAB, Material.POLISHED_BLACKSTONE_SLAB);

        double roofRoll = random.nextDouble();
        if (roofRoll < 0.33) {
            let(DungeonMaterial.ROOF, new BlockWeightedJumble().add(Material.STONE_BRICKS, 9).add(Material.CRACKED_STONE_BRICKS, 1).add(Material.MOSSY_STONE_BRICKS, 2));
            let(DungeonMaterial.ROOF_STAIR, new BlockWeightedJumble().add(Material.STONE_BRICK_STAIRS, 10).add(Material.MOSSY_STONE_BRICK_STAIRS, 2));
            let(DungeonMaterial.ROOF_SLAB, new BlockWeightedJumble().add(Material.STONE_BRICK_SLAB, 10).add(Material.MOSSY_STONE_BRICK_SLAB, 2));
        } else if (roofRoll < 0.66) {
            let(DungeonMaterial.ROOF, new BlockWeightedJumble().add(Material.COBBLESTONE, 10).add(Material.MOSSY_COBBLESTONE, 2));
            let(DungeonMaterial.ROOF_STAIR, new BlockWeightedJumble().add(Material.COBBLESTONE_STAIRS, 10).add(Material.MOSSY_COBBLESTONE_STAIRS, 2));
            let(DungeonMaterial.ROOF_SLAB, new BlockWeightedJumble().add(Material.COBBLESTONE_SLAB, 10).add(Material.MOSSY_COBBLESTONE_SLAB, 2));
        } else {
            let(DungeonMaterial.ROOF, Material.NETHER_BRICKS);
            let(DungeonMaterial.ROOF_STAIR, Material.NETHER_BRICK_STAIRS);
            let(DungeonMaterial.ROOF_SLAB, Material.NETHER_BRICK_SLAB);
        }

    }

    @Override
    protected void doSecondaryWallRoofOverride() {
        //do nothing
    }


}
