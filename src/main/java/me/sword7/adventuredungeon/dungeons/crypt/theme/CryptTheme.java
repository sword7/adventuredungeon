package me.sword7.adventuredungeon.dungeons.crypt.theme;

import me.sword7.adventuredungeon.dungeons.crypt.CryptRoomTag;
import me.sword7.adventuredungeon.structure.Theme;
import me.sword7.adventuredungeon.structure.block.*;
import me.sword7.adventuredungeon.util.BlockColor;
import me.sword7.adventuredungeon.util.Pair;
import me.sword7.adventuredungeon.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.block.data.Directional;

public class CryptTheme extends Theme {

    protected BlockColor blockColor;

    private double secondaryWallChance = Util.randomDouble(0.1, 0.33);
    private double smoothFloorChance = Util.randomDouble(0.2, 0.85);
    private double checkerFloorChance = Util.randomDouble(0.05, 0.33);

    protected IBlockFactory smoothFloor = new BlockWeightedJumble().add(Material.SMOOTH_STONE, 17).add(Material.STONE, 1).add(Material.ANDESITE, 1);
    protected IBlockFactory secondaryWall = new BlockWeightedJumble().add(Material.BRICKS, 17).add(Material.STONE_BRICKS, 1).add(Material.CRACKED_STONE_BRICKS, 1);
    protected IBlockFactory rareWall1 = new BlockWeightedJumble().add(Material.OBSIDIAN, 27).add(Material.STONE_BRICKS, 1).add(Material.CRACKED_STONE_BRICKS, 1);
    protected IBlockFactory rareWall2 = new BlockWeightedJumble().add(Material.DARK_PRISMARINE, 27).add(Material.STONE_BRICKS, 1).add(Material.CRACKED_STONE_BRICKS, 1);
    protected Checker checker;

    public CryptTheme(BlockColor blockColor) {
        super();
        this.blockColor = blockColor;
        this.checker = selectChecker(new BlockWeightedJumble().add(Material.STONE, 1).add(Material.ANDESITE, 1), 0.1, blockColor);
        init();
    }

    private void init() {

        let(DungeonMaterial.PRIMARY, new BlockWeightedJumble()
                .add(Material.STONE_BRICKS, 7)
                .add(Material.CRACKED_STONE_BRICKS, 1));
        let(DungeonMaterial.PRIMARY_STAIR, Material.STONE_BRICK_STAIRS);
        let(DungeonMaterial.PRIMARY_SLAB, Material.STONE_BRICK_SLAB);
        let(DungeonMaterial.PRIMARY_WALL, Material.STONE_BRICK_WALL);

        let(DungeonMaterial.SECONDARY, Material.COBBLESTONE);
        let(DungeonMaterial.SECONDARY_STAIR, Material.COBBLESTONE_STAIRS);
        let(DungeonMaterial.SECONDARY_SLAB, Material.COBBLESTONE_SLAB);
        let(DungeonMaterial.SECONDARY_WALL, Material.COBBLESTONE_WALL);

        boolean rustic = random.nextDouble() < 0.25;

        let(DungeonMaterial.HIGHLIGHT, Material.POLISHED_ANDESITE);
        let(DungeonMaterial.HIGHLIGHT_STAIR, Material.POLISHED_ANDESITE_STAIRS);
        Material highlightSlab = rustic ? Material.COBBLESTONE_SLAB : Material.POLISHED_ANDESITE_SLAB;
        let(DungeonMaterial.HIGHLIGHT_SLAB, highlightSlab);

        let(DungeonMaterial.DETAIL, Material.CHISELED_STONE_BRICKS);
        let(DungeonMaterial.CRACKED_WALL, Material.CRACKED_STONE_BRICKS);
        if (!rustic && random.nextDouble() < 0.25) {
            let(DungeonMaterial.CEILING, new BlockWeightedJumble().add(Material.COBBLESTONE, 10)
                    .add(Material.STONE, 1)
                    .add(Material.ANDESITE, 1));
        } else {
            let(DungeonMaterial.CEILING, new BlockWeightedJumble()
                    .add(Material.POLISHED_ANDESITE, 9)
                    .add(Material.POLISHED_ANDESITE_STAIRS, 1)
                    .add(Material.STONE, 1)
                    .add(Material.ANDESITE, 1));
        }

        let(DungeonMaterial.FLOOR, new BlockWeightedJumble()
                .add(Material.STONE, 9)
                .add(Material.ANDESITE, 9)
                .add(Material.STONE_STAIRS, 1)
                .add(Material.ANDESITE_STAIRS, 1));
        let(DungeonMaterial.FLOOR_STAIR_BLOCK, new BlockWeightedJumble()
                .add(Material.STONE, 1)
                .add(Material.ANDESITE, 1));
        let(DungeonMaterial.FLOOR_STAIR, new BlockJumble().add(Material.STONE_STAIRS).add(Material.ANDESITE_STAIRS));
        let(DungeonMaterial.FLOOR_SLAB, new BlockJumble().add(Material.STONE_SLAB).add(Material.ANDESITE_SLAB));

        let(DungeonMaterial.FLOOR_OUTSIDE, Material.POLISHED_ANDESITE);
        let(DungeonMaterial.FLOOR_OUTSIDE_STAIR_BLOCK, Material.POLISHED_ANDESITE);
        let(DungeonMaterial.FLOOR_OUTSIDE_STAIR, Material.POLISHED_ANDESITE_STAIRS);
        let(DungeonMaterial.FLOOR_OUTSIDE_SLAB, Material.POLISHED_ANDESITE_SLAB);

        double roofRoll = random.nextDouble();
        if (roofRoll < 0.4) {
            boolean bricks = random.nextBoolean();
            Material roofMaterial = bricks ? Material.BRICKS : Material.DARK_PRISMARINE;
            IBlockFactory roof = new BlockWeightedJumble().add(roofMaterial, 16).add(Material.COBBLESTONE, 3).add(Material.MOSSY_COBBLESTONE, 1);
            let(DungeonMaterial.ROOF, roof);
            Material roofStairMaterial = bricks ? Material.BRICK_STAIRS : Material.DARK_PRISMARINE_STAIRS;
            IBlockFactory roofStair = new BlockWeightedJumble().add(roofStairMaterial, 16).add(Material.COBBLESTONE_STAIRS, 3).add(Material.MOSSY_COBBLESTONE_STAIRS, 1);
            let(DungeonMaterial.ROOF_STAIR, roofStair);
            Material roofSlabMaterial = bricks ? Material.BRICK_SLAB : Material.DARK_PRISMARINE_SLAB;
            IBlockFactory roofSlab = new BlockWeightedJumble().add(roofSlabMaterial, 16).add(Material.COBBLESTONE_SLAB, 3).add(Material.MOSSY_COBBLESTONE_SLAB, 1);
            let(DungeonMaterial.ROOF_SLAB, roofSlab);
        } else {
            let(DungeonMaterial.ROOF, Material.NETHER_BRICKS);
            let(DungeonMaterial.ROOF_STAIR, Material.NETHER_BRICK_STAIRS);
            let(DungeonMaterial.ROOF_SLAB, Material.NETHER_BRICK_SLAB);
        }

        let(DungeonMaterial.BANNER_1, blockColor.getBanner());
        let(DungeonMaterial.BANNER_2, Material.BLACK_WALL_BANNER);

        let(DungeonMaterial.CARPET_LIGHT_1, Material.LIGHT_GRAY_CARPET);
        let(DungeonMaterial.CARPET_MAIN_1, blockColor.getCarpet());
        let(DungeonMaterial.CARPET_DARK_1, Material.GRAY_CARPET);
        let(DungeonMaterial.CARPET_DARK_2, Material.BLACK_CARPET);

        let(DungeonMaterial.WINDOW_1, Material.BLACK_STAINED_GLASS_PANE);
        let(DungeonMaterial.WINDOW_2, Material.IRON_BARS);

    }

    public BlockColor getBlockColor() {
        return blockColor;
    }


    @Override
    public void beginStruct() {
        super.beginStruct();
        double wallRoll = random.nextDouble();
        if (wallRoll < secondaryWallChance) {
            override(DungeonMaterial.PRIMARY, secondaryWall);
            doSecondaryWallRoofOverride();
        } else if (!tags.contains(CryptRoomTag.ENTRANCE)) {
            if (wallRoll < secondaryWallChance + 0.015) {
                override(DungeonMaterial.PRIMARY, rareWall1);
            } else if (wallRoll < secondaryWallChance + 0.03) {
                override(DungeonMaterial.PRIMARY, rareWall2);
            }
        }

        if (random.nextDouble() < checkerFloorChance) {
            override(DungeonMaterial.FLOOR, checker);
        } else if (random.nextDouble() < smoothFloorChance) {
            override(DungeonMaterial.FLOOR, smoothFloor);
        }
    }

    protected void doSecondaryWallRoofOverride() {
        override(DungeonMaterial.ROOF, Material.NETHER_BRICKS);
        override(DungeonMaterial.ROOF_STAIR, Material.NETHER_BRICK_STAIRS);
        override(DungeonMaterial.ROOF_SLAB, Material.NETHER_BRICK_SLAB);
    }

    @Override
    protected void makeBanner(Block block, Directional directional, DungeonMaterial type) {
        DyeColor dyeColor = blockColor.getDyeColor();
        boolean patternOne = random.nextBoolean();
        if (type == DungeonMaterial.BANNER_1) {
            Material material = patternOne ? blockColor.getBanner() : Material.BLACK_WALL_BANNER;
            Directional blockData = (Directional) material.createBlockData();
            blockData.setFacing(directional.getFacing());
            block.setBlockData(blockData);
            Banner banner = (Banner) block.getState();
            if (patternOne) {
                banner.addPattern(new Pattern(DyeColor.BLACK, PatternType.STRAIGHT_CROSS));
                banner.addPattern(new Pattern(DyeColor.BLACK, PatternType.CIRCLE_MIDDLE));
            } else {
                banner.addPattern(new Pattern(dyeColor, PatternType.TRIANGLE_TOP));
                banner.addPattern(new Pattern(dyeColor, PatternType.STRIPE_SMALL));
                banner.addPattern(new Pattern(dyeColor, PatternType.STRIPE_CENTER));
            }
            banner.update();
        } else if (type == DungeonMaterial.BANNER_2) {
            Material material = patternOne ? Material.BLACK_WALL_BANNER : blockColor.getBanner();
            Directional blockData = (Directional) material.createBlockData();
            blockData.setFacing(directional.getFacing());
            block.setBlockData(blockData);
            Banner banner = (Banner) block.getState();
            if (patternOne) {
                banner.addPattern(new Pattern(dyeColor, PatternType.STRAIGHT_CROSS));
                banner.addPattern(new Pattern(dyeColor, PatternType.CIRCLE_MIDDLE));
            } else {
                banner.addPattern(new Pattern(DyeColor.BLACK, PatternType.TRIANGLE_TOP));
                banner.addPattern(new Pattern(DyeColor.BLACK, PatternType.STRIPE_SMALL));
                banner.addPattern(new Pattern(DyeColor.BLACK, PatternType.STRIPE_CENTER));
            }
            banner.update();
        }
    }


    private static Pair<Material>[] redCheckers = new Pair[]{
            new Pair<>(Material.RED_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.BLACK_TERRACOTTA, Material.RED_TERRACOTTA),
            new Pair<>(Material.GREEN_TERRACOTTA, Material.RED_TERRACOTTA),
            new Pair<>(Material.RED_TERRACOTTA, Material.CYAN_TERRACOTTA),

    };

    private static Pair<Material>[] greenCheckers = new Pair[]{
            new Pair<>(Material.LIME_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.BLACK_TERRACOTTA, Material.GREEN_TERRACOTTA),
            new Pair<>(Material.LIME_TERRACOTTA, Material.YELLOW_TERRACOTTA),
            new Pair<>(Material.GREEN_TERRACOTTA, Material.MAGENTA_TERRACOTTA),
            new Pair<>(Material.GREEN_TERRACOTTA, Material.RED_TERRACOTTA),
            new Pair<>(Material.CYAN_TERRACOTTA, Material.LIME_TERRACOTTA),

    };

    private static Pair<Material>[] blueCheckers = new Pair[]{
            new Pair<>(Material.BLUE_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.LIGHT_BLUE_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.BLACK_TERRACOTTA, Material.BLUE_TERRACOTTA),
            new Pair<>(Material.PURPLE_TERRACOTTA, Material.BLUE_TERRACOTTA),
            new Pair<>(Material.BLUE_TERRACOTTA, Material.CYAN_TERRACOTTA),
    };

    private static Pair<Material>[] purpleCheckers = new Pair[]{
            new Pair<>(Material.BLACK_TERRACOTTA, Material.PURPLE_TERRACOTTA),
            new Pair<>(Material.PURPLE_TERRACOTTA, Material.BLUE_TERRACOTTA),
            new Pair<>(Material.PURPLE_TERRACOTTA, Material.YELLOW_TERRACOTTA),
            new Pair<>(Material.MAGENTA_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.PURPLE_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.GREEN_TERRACOTTA, Material.MAGENTA_TERRACOTTA),
    };

    private static Pair<Material>[] coloredCheckers = new Pair[]{

            new Pair<>(Material.RED_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.BLACK_TERRACOTTA, Material.RED_TERRACOTTA),
            new Pair<>(Material.RED_TERRACOTTA, Material.CYAN_TERRACOTTA),

            new Pair<>(Material.LIME_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.BLACK_TERRACOTTA, Material.GREEN_TERRACOTTA),
            new Pair<>(Material.LIME_TERRACOTTA, Material.YELLOW_TERRACOTTA),
            new Pair<>(Material.GREEN_TERRACOTTA, Material.MAGENTA_TERRACOTTA),
            new Pair<>(Material.GREEN_TERRACOTTA, Material.RED_TERRACOTTA),
            new Pair<>(Material.CYAN_TERRACOTTA, Material.LIME_TERRACOTTA),

            new Pair<>(Material.BLUE_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.LIGHT_BLUE_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.BLACK_TERRACOTTA, Material.BLUE_TERRACOTTA),
            new Pair<>(Material.BLUE_TERRACOTTA, Material.CYAN_TERRACOTTA),

            new Pair<>(Material.PURPLE_TERRACOTTA, Material.BLUE_TERRACOTTA),
            new Pair<>(Material.BLACK_TERRACOTTA, Material.PURPLE_TERRACOTTA),
            new Pair<>(Material.PURPLE_TERRACOTTA, Material.YELLOW_TERRACOTTA),
            new Pair<>(Material.MAGENTA_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.PURPLE_TERRACOTTA, Material.WHITE_TERRACOTTA),
    };

    private static Pair<Material>[] neutralCheckers = new Pair[]{
            new Pair<>(Material.BLACK_TERRACOTTA, Material.WHITE_TERRACOTTA),
            new Pair<>(Material.BLACK_TERRACOTTA, Material.TERRACOTTA),
            new Pair<>(Material.TERRACOTTA, Material.YELLOW_TERRACOTTA),
            new Pair<>(Material.BROWN_TERRACOTTA, Material.ORANGE_TERRACOTTA),
            new Pair<>(Material.GRAY_TERRACOTTA, Material.CYAN_TERRACOTTA),
            new Pair<>(Material.GRAY_TERRACOTTA, Material.LIGHT_GRAY_TERRACOTTA),
    };

    protected static Checker selectChecker(IBlockFactory secondary, double secondaryChance, BlockColor blockColor) {
        Pair<Material>[] checkers;
        double roll = random.nextDouble();
        if (roll < 0.4) {
            if (blockColor == BlockColor.RED) {
                checkers = redCheckers;
            } else if (blockColor == BlockColor.GREEN) {
                checkers = greenCheckers;
            } else if (blockColor == BlockColor.BLUE) {
                checkers = blueCheckers;
            } else if (blockColor == BlockColor.PURPLE) {
                checkers = purpleCheckers;
            } else {
                checkers = neutralCheckers;
            }
        } else if (roll < 0.7) {
            checkers = coloredCheckers;
        } else {
            checkers = neutralCheckers;
        }
        Pair<Material> pair = checkers[random.nextInt(checkers.length)];
        return new Checker(pair.p1, pair.p2, secondary, secondaryChance);
    }

    /*

    Floors:
    -stone/andesite
    -stone/andesite/corse dirt/dirt/soulsand (dead bush/stones(buttons))
    -mostly dirt/grass
    -smooth stone
    -checkers (dead bush)

    task end: (gradient/to rooms surrounding)

    - (infested) cobwebs
    - (ruined) caveins(hanging stone)/cracks/holes/dirt/dead bush/pebbles
    - (flooded)
    - (heat) lava floor patch/lava flood
    - (horror) bones -> blood -> nether [deeper dependent]
    - (overgrown) dirt floor/grass -> mossy -> plants/mushrooms -> nether [closer to surface + forest biome dependent]
    - (snowed in) snow/ice -> snow piles [snowy biome dependent]


     */


}
