package me.sword7.adventuredungeon.dungeons.crypt;

import me.sword7.adventuredungeon.dungeon.container.ContainerType;
import me.sword7.adventuredungeon.lootpool.LootPool;
import me.sword7.adventuredungeon.lootpool.LootUtil;
import me.sword7.adventuredungeon.lootpool.item.*;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;

import java.util.Random;

public class CryptLootPool extends LootPool {

    private static ItemCollection barrelCollection = new ItemCollection();
    private static ItemCollection crateCollection = new ItemCollection();
    private static ItemCollection potCollection = new ItemCollection();
    private static ItemCollection skullCollection = new ItemCollection();

    public static void init() {

        ItemWeightedAmount crateArrows = new ItemWeightedAmount(Material.ARROW);
        crateArrows.addAmount(5, 1);
        crateArrows.addAmount(10, 1);

        ItemWeightedAmount cratePotatoes = new ItemWeightedAmount(Material.POTATO);
        cratePotatoes.addAmount(1, 1);
        cratePotatoes.addAmount(2, 1);
        cratePotatoes.addAmount(5, 1);

        ItemWeightedAmount crateCarrots = new ItemWeightedAmount(Material.CARROT);
        crateCarrots.addAmount(1, 1);
        crateCarrots.addAmount(2, 1);
        crateCarrots.addAmount(5, 1);

        ItemWeightedAmount crateFish = new ItemWeightedAmount(Material.COOKED_COD);
        crateFish.addAmount(1, 1);
        crateFish.addAmount(2, 1);
        crateFish.addAmount(5, 1);

        ItemWeightedAmount crateBread = new ItemWeightedAmount(Material.BREAD);
        crateBread.addAmount(1, 1);
        crateBread.addAmount(2, 1);
        crateBread.addAmount(5, 1);

        barrelCollection.addItem(crateArrows, 3);
        barrelCollection.addItem(crateCarrots, 2);
        barrelCollection.addItem(cratePotatoes, 2);
        barrelCollection.addItem(crateFish, 2);
        barrelCollection.addItem(crateBread, 2);
        barrelCollection.addItem(Material.EMERALD, 2);
        barrelCollection.addItem(Material.IRON_INGOT, 2);
        barrelCollection.addItem(Material.GOLD_INGOT, 2);
        barrelCollection.addItem(Material.DIAMOND, 1);
        barrelCollection.addItem(LootUtil.HEART, 4);
        barrelCollection.addItem(new EntityItem(EntityType.SLIME), 1);
        barrelCollection.addItem(new EntityItem(EntityType.SILVERFISH), 1);
        barrelCollection.addItem(new EntityItem(EntityType.BAT), 1);
        barrelCollection.addItem(new EntityItem(EntityType.VEX), 1);

        crateCollection.addItem(crateArrows, 3);
        crateCollection.addItem(crateCarrots, 2);
        crateCollection.addItem(cratePotatoes, 2);
        crateCollection.addItem(crateFish, 2);
        crateCollection.addItem(crateBread, 2);
        crateCollection.addItem(Material.EMERALD, 2);
        crateCollection.addItem(Material.IRON_INGOT, 2);
        crateCollection.addItem(Material.GOLD_INGOT, 2);
        crateCollection.addItem(Material.DIAMOND, 1);
        crateCollection.addItem(LootUtil.HEART, 4);
        crateCollection.addItem(new EntityItem(EntityType.BAT), 1);
        crateCollection.addItem(new EntityItem(EntityType.VEX), 1);

        ItemWeightedAmount potArrows = new ItemWeightedAmount(Material.ARROW);
        potArrows.addAmount(1, 1);
        potArrows.addAmount(2, 1);
        potArrows.addAmount(5, 1);

        ItemCollection seedCollection = new ItemCollection();
        seedCollection.addItem(Material.MELON_SEEDS, 2);
        seedCollection.addItem(Material.PUMPKIN_SEEDS, 2);
        seedCollection.addItem(Material.WHEAT_SEEDS, 2);
        seedCollection.addItem(Material.BEETROOT_SEEDS, 1);

        potCollection.addItem(Material.EMERALD, 2);
        potCollection.addItem(new ItemRangedAmount(Material.GOLD_NUGGET, 1, 3), 2);
        potCollection.addItem(LootUtil.HEART, 4);
        potCollection.addItem(new ItemRangedAmount(Material.IRON_NUGGET, 1, 3), 2);
        potCollection.addItem(Material.DIAMOND, 1);
        potCollection.addItem(potArrows, 2);
        potCollection.addItem(seedCollection, 2);
        potCollection.addItem(new EntityItem(EntityType.VEX), 1);
        potCollection.addItem(new EntityItem(EntityType.BAT), 1);

        skullCollection.addItem(Material.BONE_MEAL, 4);
        skullCollection.addItem(Material.ARROW, 2);
        skullCollection.addItem(LootUtil.HEART, 4);
        skullCollection.addItem(new ItemRangedAmount(Material.GOLD_NUGGET, 1, 3), 2);
        skullCollection.addItem(Material.EMERALD, 2);
        skullCollection.addItem(new ItemRangedAmount(Material.IRON_NUGGET, 1, 3), 2);
        skullCollection.addItem(Material.DIAMOND, 1);
        skullCollection.addItem(new EntityItem(EntityType.VEX), 1);
        skullCollection.addItem(new EntityItem(EntityType.BAT), 1);
        skullCollection.addItem(new EntityItem(EntityType.SILVERFISH), 1);
    }

    @Override
    public double getContainerDropChance(ContainerType containerType) {
        return 0.33;
    }

    @Override
    public ILootItem selectContainer(ContainerType containerType, Random random) {
        if (containerType == ContainerType.SKULL) {
            return skullCollection.select(random);
        } else if (containerType == ContainerType.POT) {
            return potCollection.select(random);
        } else if (containerType == ContainerType.BARREL) {
            return barrelCollection.select(random);
        } else if (containerType == ContainerType.CRATE) {
            return crateCollection.select(random);
        } else {
            return null;
        }

    }

}
