package me.sword7.adventuredungeon.dungeons.crypt;

import me.sword7.adventuredungeon.lootpool.LootTag;

public enum CryptLootTag implements LootTag {

    HIDEOUT,
    GRAVE,
    CEREMONY,

}
