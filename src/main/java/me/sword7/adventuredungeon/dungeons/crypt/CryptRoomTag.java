package me.sword7.adventuredungeon.dungeons.crypt;

import me.sword7.adventuredungeon.structure.RoomTag;
import me.sword7.adventuredungeon.util.Tag;

public enum CryptRoomTag implements RoomTag {
    ENTRANCE,
}
