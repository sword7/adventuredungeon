package me.sword7.adventuredungeon.structure;

import org.bukkit.block.Biome;

import java.util.Random;

public interface IThemeFactory {

    ITheme select(Biome biome, Random random);

}
