package me.sword7.adventuredungeon.structure.block;

import org.bukkit.block.data.BlockData;

public class MinecraftBlock implements DungeonBlock {

    private BlockData blockData;

    public MinecraftBlock(BlockData blockData) {
        this.blockData = blockData;
    }

    public BlockData getBlockData() {
        return blockData;
    }

    @Override
    public DungeonBlock clone() {
        return new MinecraftBlock(blockData.clone());
    }
}
