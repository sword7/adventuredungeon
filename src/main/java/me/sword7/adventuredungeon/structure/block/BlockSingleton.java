package me.sword7.adventuredungeon.structure.block;

import org.bukkit.Location;
import org.bukkit.Material;

import java.util.Random;

public class BlockSingleton implements IBlockFactory {

    private Material material;

    public BlockSingleton(Material material) {
        this.material = material;
    }

    @Override
    public Material select(Random r, Location location) {
        return material;
    }
}
