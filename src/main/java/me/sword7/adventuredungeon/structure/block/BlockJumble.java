package me.sword7.adventuredungeon.structure.block;

import org.bukkit.Location;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BlockJumble implements IBlockFactory {

    private List<Material> materials = new ArrayList<>();

    public BlockJumble() {

    }

    public BlockJumble(List<Material> materials) {
        this.materials = materials;
    }

    public BlockJumble add(Material material) {
        if (!materials.contains(material)) {
            materials.add(material);
        }
        return this;
    }

    public void remove(Material material) {
        materials.remove(material);
    }

    @Override
    public Material select(Random r, Location location) {
        return materials.get(r.nextInt(materials.size()));
    }
}
