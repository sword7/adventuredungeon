package me.sword7.adventuredungeon.structure.block;

public interface DungeonBlock {

    DungeonBlock clone();

}
