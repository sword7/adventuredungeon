package me.sword7.adventuredungeon.structure.block;

import com.google.common.collect.ImmutableSet;
import org.bukkit.Material;

import java.util.Set;

public enum DungeonMaterial {

    PRIMARY,
    SECONDARY,
    TERTIARY,
    QUATERNARY,
    HIGHLIGHT,
    DETAIL,
    FANCY,
    ROD,
    SHRUB,
    CRACKED_WALL,
    FLOOR,
    FLOOR_STAIR_BLOCK,
    FLOOR_OUTSIDE,
    FLOOR_OUTSIDE_STAIR_BLOCK,
    CEILING,
    ROOF,

    PRIMARY_STAIR,
    SECONDARY_STAIR,
    TERTIARY_STAIR,
    QUATERNARY_STAIR,
    HIGHLIGHT_STAIR,
    FLOOR_STAIR,
    FLOOR_OUTSIDE_STAIR,
    ROOF_STAIR,

    PRIMARY_SLAB,
    SECONDARY_SLAB,
    TERTIARY_SLAB,
    QUATERNARY_SLAB,
    HIGHLIGHT_SLAB,
    FLOOR_SLAB,
    FLOOR_OUTSIDE_SLAB,
    ROOF_SLAB,

    PRIMARY_WALL,
    SECONDARY_WALL,
    TERTIARY_WALL,
    QUATERNARY_WALL,
    HIGHLIGHT_WALL,

    WINDOW_1,
    WINDOW_2,
    WINDOW_3,

    WOOD,
    WOOD_FENCE,
    WOOD_TRAPDOOR,
    WOOD_BUTTON,
    WOOD_SLAB,
    WOOD_STAIR,

    CARPET_LIGHT_1,
    CARPET_LIGHT_2,
    CARPET_MAIN_1,
    CARPET_MAIN_2,
    CARPET_DARK_1,
    CARPET_DARK_2,

    BANNER_1,
    BANNER_2,
    BANNER_3,

    CRATE,
    CHEST,
    SPAWNER,

    VOID,


    ;

    public static DungeonMaterial fromString(String s) {
        try {
            String material = s.split("\\[")[0].toUpperCase();
            if (material.equals(".")) return VOID;
            return valueOf(material);
        } catch (Exception e) {
            return null;
        }
    }

    public String getDataMaterialString() {
        return "minecraft:" + getDataMaterial().toString().toLowerCase();
    }


    private Material getDataMaterial() {
        if (isStair()) {
            return Material.COBBLESTONE_STAIRS;
        } else if (isSlab()) {
            return Material.COBBLESTONE_SLAB;
        } else if (isWall()) {
            return Material.COBBLESTONE_WALL;
        } else if (isBanner()) {
            return Material.WHITE_WALL_BANNER;
        } else if (isWindow()) {
            return Material.GLASS_PANE;
        } else if (isCrate()) {
            return Material.PLAYER_HEAD;
        } else if (this == WOOD) {
            return Material.STRIPPED_OAK_WOOD;
        } else if (this == WOOD_FENCE) {
            return Material.OAK_FENCE;
        } else if (this == WOOD_TRAPDOOR) {
            return Material.OAK_TRAPDOOR;
        } else if (this == WOOD_BUTTON) {
            return Material.OAK_BUTTON;
        } else if (this == ROD) {
            return Material.GLASS_PANE;
        } else if (this == FANCY) {
            return Material.WHITE_GLAZED_TERRACOTTA;
        } else if (this == SHRUB) {
            return Material.DEAD_FIRE_CORAL;
        } else if (this == CHEST) {
            return Material.CHEST;
        } else {
            return Material.COBBLESTONE;
        }
    }

    public boolean isStair() {
        return stairs.contains(this);
    }

    public boolean isSlab() {
        return slabs.contains(this);
    }

    public boolean isWall() {
        return walls.contains(this);
    }

    public boolean isBanner() {
        return banners.contains(this);
    }

    public boolean isWindow() {
        return windows.contains(this);
    }

    public boolean isCrate() {
        return this == CRATE;
    }

    public boolean hasData() {
        return dataMaterial.contains(this);
    }

    private static Set<DungeonMaterial> dataMaterial = new ImmutableSet.Builder<DungeonMaterial>()
            .add(PRIMARY_STAIR).add(SECONDARY_STAIR).add(TERTIARY_STAIR).add(QUATERNARY_STAIR).add(WOOD_STAIR).add(FLOOR_STAIR).add(FLOOR_OUTSIDE_STAIR).add(HIGHLIGHT_STAIR).add(ROOF_STAIR)
            .add(PRIMARY_SLAB).add(SECONDARY_SLAB).add(TERTIARY_SLAB).add(QUATERNARY_SLAB).add(WOOD_SLAB).add(FLOOR_SLAB).add(FLOOR_OUTSIDE_SLAB).add(HIGHLIGHT_SLAB).add(ROOF_SLAB)
            .add(PRIMARY_WALL).add(SECONDARY_WALL).add(TERTIARY_WALL).add(QUATERNARY_WALL).add(WOOD_FENCE).add(HIGHLIGHT_WALL)
            .add(WOOD_BUTTON).add(WOOD_TRAPDOOR)
            .add(BANNER_1).add(BANNER_2).add(BANNER_3)
            .add(WINDOW_1).add(WINDOW_2).add(WINDOW_3).add(ROD)
            .add(WOOD).add(WOOD_BUTTON).add(WOOD_FENCE).add(WOOD_TRAPDOOR)
            .add(CRATE).add(SHRUB).add(CHEST)
            .build();


    private static Set<DungeonMaterial> stairs = new ImmutableSet.Builder<DungeonMaterial>()
            .add(PRIMARY_STAIR).add(SECONDARY_STAIR).add(TERTIARY_STAIR).add(QUATERNARY_STAIR).add(WOOD_STAIR).add(FLOOR_STAIR).add(FLOOR_OUTSIDE_STAIR).add(HIGHLIGHT_STAIR).add(ROOF_STAIR)
            .build();


    private static Set<DungeonMaterial> slabs = new ImmutableSet.Builder<DungeonMaterial>()
            .add(PRIMARY_SLAB).add(SECONDARY_SLAB).add(TERTIARY_SLAB).add(QUATERNARY_SLAB).add(WOOD_SLAB).add(FLOOR_SLAB).add(FLOOR_OUTSIDE_SLAB).add(HIGHLIGHT_SLAB).add(ROOF_SLAB)
            .build();


    private static Set<DungeonMaterial> walls = new ImmutableSet.Builder<DungeonMaterial>()
            .add(PRIMARY_WALL).add(SECONDARY_WALL).add(TERTIARY_WALL).add(QUATERNARY_WALL).add(HIGHLIGHT_WALL)
            .build();

    private static Set<DungeonMaterial> banners = new ImmutableSet.Builder<DungeonMaterial>()
            .add(BANNER_1).add(BANNER_2).add(BANNER_3)
            .build();

    private static Set<DungeonMaterial> windows = new ImmutableSet.Builder<DungeonMaterial>()
            .add(WINDOW_1).add(WINDOW_2).add(WINDOW_3).add(ROD)
            .build();

}
