package me.sword7.adventuredungeon.structure.block;

import org.bukkit.block.data.BlockData;

public class ThemeBlock implements DungeonBlock {
    private DungeonMaterial dungeonMaterial;
    private BlockData data;

    public ThemeBlock(DungeonMaterial dungeonMaterial) {
        this.dungeonMaterial = dungeonMaterial;
    }

    public ThemeBlock(DungeonMaterial dungeonMaterial, BlockData data) {
        this.dungeonMaterial = dungeonMaterial;
        this.data = data;
    }

    public DungeonMaterial getDungeonMaterial() {
        return dungeonMaterial;
    }

    public BlockData getData() {
        return data;
    }

    public boolean hasData() {
        return data != null;
    }

    @Override
    public DungeonBlock clone() {
        if (hasData()) {
            return new ThemeBlock(dungeonMaterial, data.clone());
        } else {
            return new ThemeBlock(dungeonMaterial);
        }
    }
}
