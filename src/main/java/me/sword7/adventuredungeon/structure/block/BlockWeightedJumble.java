package me.sword7.adventuredungeon.structure.block;

import me.sword7.adventuredungeon.util.weight.IWeighted;
import me.sword7.adventuredungeon.util.weight.WeightedChoice;
import me.sword7.adventuredungeon.util.weight.WeightedRandomizer;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.Random;

public class BlockWeightedJumble implements IBlockFactory {

    private WeightedRandomizer<Material> weightedRandomizer = new WeightedRandomizer<>();

    public BlockWeightedJumble add(IWeighted<Material> weighted) {
        weightedRandomizer.add(weighted);
        return this;
    }

    public BlockWeightedJumble add(Material material, int weight) {
        weightedRandomizer.add(new WeightedChoice<>(material, weight));
        return this;
    }

    @Override
    public Material select(Random r, Location location) {
        return weightedRandomizer.get(r);
    }

}
