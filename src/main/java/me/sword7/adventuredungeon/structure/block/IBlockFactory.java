package me.sword7.adventuredungeon.structure.block;

import org.bukkit.Location;
import org.bukkit.Material;

import java.util.Random;

public interface IBlockFactory {

    Material select(Random r, Location location);

}
