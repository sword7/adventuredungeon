package me.sword7.adventuredungeon.structure.block;

import org.bukkit.Location;
import org.bukkit.Material;

import java.util.Random;

public class Checker implements IBlockFactory {

    private Material dark;
    private Material light;
    private IBlockFactory secondary;
    private double secondaryChance;

    public Checker(Material dark, Material light) {
        this.dark = dark;
        this.light = light;
    }

    public Checker(Material dark, Material light, IBlockFactory secondary, double secondaryChance) {
        this.dark = dark;
        this.light = light;
        this.secondary = secondary;
        this.secondaryChance = secondaryChance;
    }

    @Override
    public Material select(Random random, Location location) {
        if (secondary != null && random.nextDouble() < secondaryChance) {
            return secondary.select(random, location);
        } else {
            return (location.getBlockX() + location.getBlockZ()) % 2 == 0 ? dark : light;
        }
    }

}
