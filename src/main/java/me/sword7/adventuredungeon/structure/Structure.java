package me.sword7.adventuredungeon.structure;

import me.sword7.adventuredungeon.generate.RoomData;
import me.sword7.adventuredungeon.structure.block.DungeonBlock;
import me.sword7.adventuredungeon.structure.block.DungeonMaterial;
import me.sword7.adventuredungeon.structure.block.MinecraftBlock;
import me.sword7.adventuredungeon.structure.block.ThemeBlock;
import me.sword7.adventuredungeon.util.math.Point;
import me.sword7.adventuredungeon.util.math.Rotation;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

public class Structure {

    private Point dimensions;
    private int width;
    private int height;
    private int depth;

    private DungeonBlock[][][] blocks;

    public Structure(Point dimensions, DungeonBlock[][][] blocks) {
        this.dimensions = dimensions;
        this.blocks = blocks;
        width = dimensions.getX();
        height = dimensions.getY();
        depth = dimensions.getZ();
    }

    public void rotate(Rotation r) {
        if (r != Rotation.R_0) {

            Point rotatedDim = r.rotateDimensions(dimensions);

            DungeonBlock[][][] rotatedBlocks = new DungeonBlock[rotatedDim.getX()][height][rotatedDim.getZ()];

            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    for (int z = 0; z < depth; z++) {
                        int rx = r.rotateX(x, z, width, depth);
                        int rz = r.rotateZ(x, z, width, depth);
                        DungeonBlock block = blocks[x][y][z];
                        r.rotateBlock(block);
                        rotatedBlocks[rx][y][rz] = block;
                    }
                }
            }

            blocks = rotatedBlocks;
            setDimensions(rotatedDim);

        }
    }

    private void setDimensions(Point dimensions) {
        this.dimensions = dimensions;
        width = dimensions.getX();
        height = dimensions.getY();
        depth = dimensions.getZ();
    }

    public void draw(Location location, ITheme theme) {
        theme.beginStruct();
        World world = location.getWorld();
        int startX = location.getBlockX();
        int startY = location.getBlockY();
        int startZ = location.getBlockZ();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    Block block = world.getBlockAt(x + startX, y + startY, z + startZ);
                    DungeonBlock dungeonBlock = blocks[x][y][z];
                    if (dungeonBlock != null) {
                        if (dungeonBlock instanceof MinecraftBlock) {
                            MinecraftBlock minecraftBlock = (MinecraftBlock) dungeonBlock;
                            block.setBlockData(minecraftBlock.getBlockData());
                        } else if (dungeonBlock instanceof ThemeBlock) {
                            ThemeBlock themeBlock = (ThemeBlock) dungeonBlock;
                            if (themeBlock.getDungeonMaterial() != DungeonMaterial.VOID) {
                                theme.setBlock(block, themeBlock);
                            }
                        }
                    } else {
                        block.setType(Material.AIR);
                    }
                }
            }
        }
        theme.endStruct();
    }

    public RoomData drawForData(Location location, ITheme theme) {
        List<Point> chestLocations = new ArrayList<>();
        List<Point> spawnerLocations = new ArrayList<>();

        theme.beginStruct();
        World world = location.getWorld();
        int startX = location.getBlockX();
        int startY = location.getBlockY();
        int startZ = location.getBlockZ();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    Block block = world.getBlockAt(x + startX, y + startY, z + startZ);
                    DungeonBlock dungeonBlock = blocks[x][y][z];
                    if (dungeonBlock != null) {
                        if (dungeonBlock instanceof MinecraftBlock) {
                            MinecraftBlock minecraftBlock = (MinecraftBlock) dungeonBlock;
                            block.setBlockData(minecraftBlock.getBlockData());
                            if (minecraftBlock.getBlockData().getMaterial() == Material.CHEST) {
                                chestLocations.add(new Point(x + startX, y + startY, z + startZ));
                            } else if (minecraftBlock.getBlockData().getMaterial() == Material.SPAWNER) {
                                setSpawner(block);
                                spawnerLocations.add(new Point(x + startX, y + startY, z + startZ));
                            }
                        } else if (dungeonBlock instanceof ThemeBlock) {
                            ThemeBlock themeBlock = (ThemeBlock) dungeonBlock;
                            DungeonMaterial material = themeBlock.getDungeonMaterial();
                            if (material != DungeonMaterial.VOID) {
                                theme.setBlock(block, themeBlock);
                                if (material == DungeonMaterial.CHEST) {
                                    chestLocations.add(new Point(x + startX, y + startY, z + startZ));
                                } else if (material == DungeonMaterial.SPAWNER) {
                                    setSpawner(block);
                                    spawnerLocations.add(new Point(x + startX, y + startY, z + startZ));
                                }
                            }
                        }
                    } else {
                        block.setType(Material.AIR);
                    }
                }
            }
        }
        theme.endStruct();

        return new RoomData(chestLocations, spawnerLocations);
    }

    private void setSpawner(Block b) {
        b.setType(Material.SPAWNER);
        CreatureSpawner spawner = (CreatureSpawner) b.getState();
        spawner.setSpawnedType(EntityType.SKELETON);
        spawner.update();
    }

    public Structure clone() {
        DungeonBlock[][][] clonedBlocks = new DungeonBlock[width][height][depth];
        for(int x=0;x<width;x++){
            for(int y=0;y<height;y++){
                for(int z=0;z<depth;z++){
                    DungeonBlock block = blocks[x][y][z];
                    if(block != null){
                        clonedBlocks[x][y][z] = block.clone();
                    }
                }
            }
        }
        return new Structure(dimensions.clone(), clonedBlocks);
    }

    public DungeonBlock[][][] getBlocks() {
        return blocks;
    }

    public Point getDimensions() {
        return dimensions;
    }


}
