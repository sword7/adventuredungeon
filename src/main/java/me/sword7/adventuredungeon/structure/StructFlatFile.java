package me.sword7.adventuredungeon.structure;

import me.sword7.adventuredungeon.AdventureDungeon;
import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.structure.block.DungeonBlock;
import me.sword7.adventuredungeon.structure.block.DungeonMaterial;
import me.sword7.adventuredungeon.structure.block.MinecraftBlock;
import me.sword7.adventuredungeon.structure.block.ThemeBlock;
import me.sword7.adventuredungeon.util.math.Point;
import org.bukkit.Server;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class StructFlatFile {

    private static Server server = AdventureDungeon.getPlugin().getServer();

    public static boolean fileExists(String folder, String name) {
        InputStream in = AdventureDungeon.getPlugin().getResource("structure/" + folder + "/" + name + ".yml");
        return in != null;
    }

    public static Structure fetch(String folder, String name) {
        InputStream in = AdventureDungeon.getPlugin().getResource("structure/" + folder + "/" + name + ".yml");
        Reader reader = new InputStreamReader(in);
        FileConfiguration config = YamlConfiguration.loadConfiguration(reader);
        close(reader);
        Point dimensions = pointFromDimensionString(config.getString("dimensions"));
        DungeonBlock[][][] blocks = new DungeonBlock[dimensions.getX()][dimensions.getY()][dimensions.getZ()];
        for (String data : config.getStringList("blocks")) {
            try {
                String[] parts = data.split(";");
                int x = Integer.valueOf(parts[0]);
                int y = Integer.valueOf(parts[1]);
                int z = Integer.valueOf(parts[2]);
                DungeonBlock dungeonBlock;
                String blockString = parts[3];
                if (blockString.startsWith("minecraft:")) {
                    BlockData blockData = server.createBlockData(blockString);
                    dungeonBlock = new MinecraftBlock(blockData);
                } else {
                    DungeonMaterial material = DungeonMaterial.fromString(blockString);
                    if (material.hasData()) {
                        String dataString = material.getDataMaterialString();
                        String dataModifiers = blockString.contains("[") ? blockString.substring(blockString.indexOf("[")) : "";
                        dataString += dataModifiers;
                        if (material.isWall()) dataString = processWallString(dataString);
                        BlockData blockData = AdventureDungeon.getPlugin().getServer().createBlockData(dataString);
                        dungeonBlock = new ThemeBlock(material, blockData);
                    } else {
                        dungeonBlock = new ThemeBlock(material);
                    }
                }

                blocks[x][y][z] = dungeonBlock;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new Structure(dimensions, blocks);
    }

    private static String processWallString(String dataString) {
        if (Version.getValue() < 116) {
            dataString = dataString
                    .replaceAll("low", "true")
                    .replaceAll("tall", "true")
                    .replaceAll("none", "false");

        }
        return dataString;
    }

    private static void close(Reader reader) {
        try {
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Point pointFromDimensionString(String s) {
        String[] parts = s.split("x");
        try {
            int x = Integer.parseInt(parts[0]);
            int y = Integer.parseInt(parts[1]);
            int z = Integer.parseInt(parts[2]);
            return new Point(x, y, z);
        } catch (Exception e) {
            return new Point(0, 0, 0);
        }
    }


}
