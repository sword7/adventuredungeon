package me.sword7.adventuredungeon.structure;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Skull;

import java.lang.reflect.Field;
import java.util.UUID;

public enum HeadBlock {

    CRATE_1("Crate1",
            "475e3bb2-3aa1-4931-b448-b82b8fbe0d06", "eyJ0aW1lc3RhbXAiOjE1NzYyNDk2NDQyOTksInByb2ZpbGVJZCI6IjE5MjUyMWI0ZWZkYjQyNWM4OTMxZjAyYTg0OTZlMTFiIiwicHJvZmlsZU5hbWUiOiJTZXJpYWxpemFibGUiLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzRjNmEzMjdjZDNjN2M4NDdkZjE3ZGJmMjlmY2JiMWYyNmI3NTYyZjgyYWI3YzM4ZTI3ZGY0MGMyM2YxMmQ2Y2UifX19"),
    ;

    private String profileName;
    private UUID headId;
    private String url;

    HeadBlock(String profileName, String headIdString, String url) {
        this.profileName = profileName;
        headId = UUID.fromString(headIdString);
        this.url = url;
    }

    public boolean isSelf(Block block) {
        if (block.getState() instanceof Skull) {
            Skull skull = (Skull) block.getState();
            if (skull.hasOwner() && skull.getOwningPlayer().getUniqueId().equals(headId)) {
                return true;
            }
        }
        return false;
    }

    public void setState(Block block) {
        BlockState state = block.getState();
        if (state instanceof Skull) {
            Skull skull = (Skull) state;
            try {
                GameProfile profile = new GameProfile(headId, profileName);
                profile.getProperties().put("textures", new Property("textures",
                        url));
                Field profileField = skull.getClass().getDeclaredField("profile");
                profileField.setAccessible(true);
                profileField.set(skull, profile);
                skull.update();
            } catch (IllegalArgumentException | SecurityException | NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }

    }

}
