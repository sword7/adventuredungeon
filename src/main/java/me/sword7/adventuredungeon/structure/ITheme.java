package me.sword7.adventuredungeon.structure;

import me.sword7.adventuredungeon.structure.block.ThemeBlock;
import org.bukkit.block.Block;

import java.util.List;

public interface ITheme {

    void beginTask(List<RoomTag> tags);

    void endTask();

    void beginStruct();

    void endStruct();

    void clear();

    void setBlock(Block block, ThemeBlock themeBlock);

}
