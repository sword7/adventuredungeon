package me.sword7.adventuredungeon.structure;

import me.sword7.adventuredungeon.dungeon.DungeonType;
import me.sword7.adventuredungeon.util.math.Cardinal;
import me.sword7.adventuredungeon.util.math.Point;

import java.util.Map;
import java.util.Random;
import java.util.Set;

public interface IFrame {

    Point getDimensions();

    int getDoors();

    Structure getStructure();

    Map<Point, Set<Cardinal>> getGridCoordToEntrances();

    Decoration selectDecoration(Random random);

    DungeonType getDungeonType();

}
