package me.sword7.adventuredungeon.structure;

import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.structure.block.BlockSingleton;
import me.sword7.adventuredungeon.structure.block.DungeonMaterial;
import me.sword7.adventuredungeon.structure.block.IBlockFactory;
import me.sword7.adventuredungeon.structure.block.ThemeBlock;
import me.sword7.adventuredungeon.util.math.Cardinal;
import me.sword7.adventuredungeon.util.version.VersionUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.*;
import org.bukkit.block.data.type.*;

import java.util.*;

public class Theme implements ITheme {

    protected static Random random = new Random();
    private Map<DungeonMaterial, IBlockFactory> materialMap = new HashMap<>();
    private Map<DungeonMaterial, IBlockFactory> overrideMaterialMap = new HashMap<>();
    protected List<RoomTag> tags = new ArrayList<>();

    public Theme() {
        let(DungeonMaterial.CRATE, Material.PLAYER_HEAD);
        let(DungeonMaterial.CHEST, Material.CHEST);
        let(DungeonMaterial.SPAWNER, Material.SPAWNER);
    }

    protected void let(DungeonMaterial dungeonMaterial, IBlockFactory factory) {
        materialMap.put(dungeonMaterial, factory);
    }

    protected void let(DungeonMaterial dungeonMaterial, Material material) {
        materialMap.put(dungeonMaterial, new BlockSingleton(material));
    }

    protected void override(DungeonMaterial dungeonMaterial, IBlockFactory factory) {
        overrideMaterialMap.put(dungeonMaterial, factory);
    }

    protected void override(DungeonMaterial dungeonMaterial, Material material) {
        overrideMaterialMap.put(dungeonMaterial, new BlockSingleton(material));
    }

    @Override
    public void beginTask(List<RoomTag> toSet) {
        tags.clear();
        tags.addAll(toSet);
    }

    @Override
    public void endTask() {
        tags.clear();
    }


    @Override
    public void beginStruct() {

    }

    @Override
    public void endStruct() {
        overrideMaterialMap.clear();
    }

    @Override
    public void clear() {

    }


    @Override
    public void setBlock(Block block, ThemeBlock tBlock) {

        DungeonMaterial dMaterial = tBlock.getDungeonMaterial();
        Location location = block.getLocation();

        if (dMaterial.isBanner()) {
            Directional bannerData = (Directional) get(dMaterial, tBlock.getData(), location);
            makeBanner(block, bannerData, dMaterial);
        } else {
            BlockData blockData = tBlock.hasData() ? get(dMaterial, tBlock.getData(), location) : get(dMaterial, location);
            block.setBlockData(blockData);
        }

        if (dMaterial == DungeonMaterial.CRATE) {
            HeadBlock.CRATE_1.setState(block);
        }


    }

    protected BlockData get(DungeonMaterial dungeonMaterial, Location location) {
        BlockData blockData;
        if (overrideMaterialMap.containsKey(dungeonMaterial)) {
            blockData = overrideMaterialMap.get(dungeonMaterial).select(random, location).createBlockData();
        } else if (materialMap.containsKey(dungeonMaterial)) {
            blockData = materialMap.get(dungeonMaterial).select(random, location).createBlockData();
        } else {
            blockData = Material.COBBLESTONE.createBlockData();
        }

        if (dungeonMaterial == DungeonMaterial.CEILING || dungeonMaterial == DungeonMaterial.FLOOR) {
            if (blockData instanceof Stairs) {
                Stairs stairs = (Stairs) blockData;
                stairs.setFacing(Cardinal.getRandom2D().asFace());
                stairs.setHalf(dungeonMaterial == DungeonMaterial.FLOOR ? Bisected.Half.BOTTOM : Bisected.Half.TOP);
            } else if (blockData instanceof Slab) {
                Slab slab = (Slab) blockData;
                slab.setType(dungeonMaterial == DungeonMaterial.FLOOR ? Slab.Type.BOTTOM : Slab.Type.TOP);
            }
        }

        return blockData;

    }

    protected BlockData get(DungeonMaterial dungeonMaterial, BlockData blockData, Location location) {
        BlockData dungeonData = get(dungeonMaterial, location);
        if (dungeonMaterial.isStair() && dungeonData instanceof Stairs) {
            Stairs stairs = (Stairs) blockData;
            Stairs stairsData = (Stairs) dungeonData;
            stairsData.setShape(stairs.getShape());
            stairsData.setFacing(stairs.getFacing());
            stairsData.setWaterlogged(stairs.isWaterlogged());
            stairsData.setHalf(stairs.getHalf());
        } else if (dungeonMaterial.isSlab() && dungeonData instanceof Slab) {
            Slab slab = (Slab) blockData;
            Slab slabData = (Slab) dungeonData;
            slabData.setType(slab.getType());
            slabData.setWaterlogged(slab.isWaterlogged());
        } else if (dungeonMaterial.isWall()) {
            if (Version.getValue() >= 116) {
                Wall wall = (Wall) blockData;
                Wall wallData = (Wall) dungeonData;
                wallData.setUp(wall.isUp());
                wallData.setHeight(BlockFace.NORTH, wall.getHeight(BlockFace.NORTH));
                wallData.setHeight(BlockFace.EAST, wall.getHeight(BlockFace.EAST));
                wallData.setHeight(BlockFace.SOUTH, wall.getHeight(BlockFace.SOUTH));
                wallData.setHeight(BlockFace.WEST, wall.getHeight(BlockFace.WEST));
                wallData.setWaterlogged(wall.isWaterlogged());
            } else {
                MultipleFacing multipleFacing = (MultipleFacing) blockData;
                MultipleFacing wallData = (MultipleFacing) dungeonData;
                for (Cardinal cardinal : Cardinal.getCardinals2D()) {
                    BlockFace face = cardinal.asFace();
                    wallData.setFace(face, multipleFacing.hasFace(face));
                }
                Waterlogged waterlogged = (Waterlogged) blockData;
                Waterlogged wallData2 = (Waterlogged) dungeonData;
                wallData2.setWaterlogged(waterlogged.isWaterlogged());
            }
        } else if (dungeonMaterial.isBanner()) {
            Directional banner = (Directional) blockData;
            Directional bannerData = (Directional) dungeonData;
            bannerData.setFacing(banner.getFacing());
        } else if (dungeonMaterial.isWindow()) {
            MultipleFacing pane = (MultipleFacing) blockData;
            MultipleFacing paneData = (MultipleFacing) dungeonData;
            Set<BlockFace> faces = pane.getFaces();
            paneData.setFace(BlockFace.NORTH, faces.contains(BlockFace.NORTH));
            paneData.setFace(BlockFace.EAST, faces.contains(BlockFace.EAST));
            paneData.setFace(BlockFace.SOUTH, faces.contains(BlockFace.SOUTH));
            paneData.setFace(BlockFace.WEST, faces.contains(BlockFace.WEST));
            ((Waterlogged) paneData).setWaterlogged(((Waterlogged) pane).isWaterlogged());
        } else if (dungeonMaterial.isCrate()) {
            Rotatable rotatable = (Rotatable) blockData;
            Rotatable crateData = (Rotatable) dungeonData;
            crateData.setRotation(rotatable.getRotation());
        } else if (dungeonMaterial == DungeonMaterial.SHRUB) {
            Waterlogged waterlogged = (Waterlogged) blockData;
            Waterlogged shrubData = (Waterlogged) dungeonData;
            shrubData.setWaterlogged(waterlogged.isWaterlogged());
        } else if (dungeonMaterial == DungeonMaterial.CHEST) {
            Directional directional = (Directional) blockData;
            Directional chestData = (Directional) dungeonData;
            chestData.setFacing(directional.getFacing());
        } else if (dungeonMaterial == DungeonMaterial.WOOD) {
            VersionUtil.setWoodAxis(dungeonData, VersionUtil.getWoodAxis(blockData));
        } else if (dungeonMaterial == DungeonMaterial.WOOD_BUTTON) {
            Directional directional = (Directional) blockData;
            Directional buttonData = (Directional) dungeonData;
            buttonData.setFacing(directional.getFacing());
            FaceAttachable faceAttachable = (FaceAttachable) blockData;
            FaceAttachable buttonData2 = (FaceAttachable) dungeonData;
            buttonData2.setAttachedFace(faceAttachable.getAttachedFace());
        } else if (dungeonMaterial == DungeonMaterial.WOOD_FENCE) {
            Fence fence = (Fence) blockData;
            Fence fenceData = (Fence) dungeonData;
            for (Cardinal cardinal : Cardinal.getCardinals2D()) {
                BlockFace face = cardinal.asFace();
                fenceData.setFace(face, fence.hasFace(face));
            }
            fenceData.setWaterlogged(fence.isWaterlogged());
        } else if (dungeonMaterial == DungeonMaterial.WOOD_TRAPDOOR) {
            TrapDoor trapDoor = (TrapDoor) blockData;
            TrapDoor doorData = (TrapDoor) dungeonData;
            doorData.setFacing(trapDoor.getFacing());
            doorData.setHalf(trapDoor.getHalf());
            doorData.setOpen(trapDoor.isOpen());
            doorData.setPowered(trapDoor.isPowered());
            doorData.setWaterlogged(trapDoor.isWaterlogged());
        }
        return dungeonData;
    }

    protected void makeBanner(Block block, Directional directional, DungeonMaterial type) {

    }


}
