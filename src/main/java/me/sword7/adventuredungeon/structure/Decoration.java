package me.sword7.adventuredungeon.structure;

public class Decoration {

    private Structure structure;

    public Decoration(Structure structure) {
        this.structure = structure;
    }

    public Structure getStructure() {
        return structure;
    }

    public static Decoration[] load(IFrame frame) {
        String folder = frame.getDungeonType().getSlug() + "/decoration";
        String frameString = frame.toString();
        int vMax = 0;
        while (StructFlatFile.fileExists(folder, frameString + "_v" + (vMax + 1))) {
            vMax++;
        }

        Decoration[] decorations = new Decoration[vMax];

        for (int v = 0; v < vMax; v++) {
            Structure structure = StructFlatFile.fetch(folder, frameString + "_v" + (v + 1));
            decorations[v] = new Decoration(structure);
        }

        return decorations;
    }

}
