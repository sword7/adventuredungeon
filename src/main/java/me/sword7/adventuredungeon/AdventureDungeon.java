package me.sword7.adventuredungeon;

import me.sword7.adventuredungeon.config.ConfigLoader;
import me.sword7.adventuredungeon.config.Language;
import me.sword7.adventuredungeon.config.PluginConfig;
import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.dungeon.DungeonCache;
import me.sword7.adventuredungeon.dungeon.DungeonLoader;
import me.sword7.adventuredungeon.dungeon.DungeonType;
import me.sword7.adventuredungeon.dungeon.EffectListener;
import me.sword7.adventuredungeon.lootpool.LootUtil;
import me.sword7.adventuredungeon.mob.MobHead;
import me.sword7.adventuredungeon.mob.MobListener;
import me.sword7.adventuredungeon.util.AutoCompleteListener;
import me.sword7.adventuredungeon.worldgen.CommandDGen;
import me.sword7.adventuredungeon.worldgen.Generator;
import org.bukkit.plugin.java.JavaPlugin;

public final class AdventureDungeon extends JavaPlugin {

    private static AdventureDungeon plugin;

    @Override
    public void onEnable() {
        plugin = this;

        //config
        ConfigLoader.load();
        new PluginConfig();
        Language.load();

        //init
        LootUtil.init();
        MobHead.init();
        DungeonType.init();

        //load
        DungeonCache.load();

        //classes
        new Generator();

        //commands
        getCommand("adventuredungeon").setExecutor(new CommandAdventureDungeon());
        getCommand("dgen").setExecutor(new CommandDGen());
        getCommand("test").setExecutor(new CommandTest());

        //listeners
        if (Version.hasAutoComplete()) new AutoCompleteListener();
        new EffectListener();
        new MobListener();

        //runnable
        new DungeonLoader();

    }

    @Override
    public void onDisable() {
        DungeonCache.shutdown();
        Generator.shutdown();
    }

    public static AdventureDungeon getPlugin() {
        return plugin;
    }

}
