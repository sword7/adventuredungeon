package me.sword7.adventuredungeon.lootpool;

public enum RarityTag implements LootTag {

    COMMON,
    UNCOMMON,
    RARE,
    EPIC,
    LEGENDARY,

}
