package me.sword7.adventuredungeon.lootpool.loot;

import me.sword7.adventuredungeon.dungeon.DungeonTag;
import me.sword7.adventuredungeon.lootpool.LootTag;
import org.bukkit.Tag;
import org.bukkit.inventory.Inventory;

import java.util.List;
import java.util.Random;

public interface ILoot {

    void fill(Inventory inventory, Random rand, List<DungeonTag> dungeonTags, List<LootTag> lootTags, int luck);

}
