package me.sword7.adventuredungeon.lootpool.loot;

import me.sword7.adventuredungeon.dungeon.DungeonTag;
import me.sword7.adventuredungeon.lootpool.LootTag;
import me.sword7.adventuredungeon.lootpool.item.ItemCollection;
import me.sword7.adventuredungeon.util.Util;
import org.bukkit.inventory.Inventory;

import java.util.List;
import java.util.Random;

public abstract class FlavoredLoot extends Loot {

    private ItemCollection treasure;

    private double minItem = 0.3;
    private double maxItem = 0.8;
    private double minTreasure = 0.25;
    private double maxTreasure = 0.7;

    public FlavoredLoot(ItemCollection treasure) {
        this.treasure = treasure;
    }

    public FlavoredLoot(ItemCollection treasure, double minItem, double maxItem, double minTreasure, double maxTreasure) {
        this.treasure = treasure;
        this.minItem = minItem;
        this.maxItem = maxItem;
        this.minTreasure = minTreasure;
        this.maxTreasure = maxTreasure;
    }

    protected abstract ItemCollection selectFlavor(Random rand, List<DungeonTag> dungeonTags, List<LootTag> lootTags);

    @Override
    public void fill(Inventory inventory, Random rand, List<DungeonTag> dungeonTags, List<LootTag> lootTags, int luck) {
        double itemChance = Util.randomDouble(minItem, maxItem);
        double treasureChance = Util.randomDouble(minTreasure, maxTreasure);

        ItemCollection flavor = selectFlavor(rand, dungeonTags, lootTags);

        for (int index = 0; index < inventory.getSize(); index++) {
            if (rand.nextDouble() < itemChance) {
                if (rand.nextDouble() < treasureChance) {
                    inventory.setItem(index, treasure.create(rand));
                } else {
                    inventory.setItem(index, flavor.create(rand));
                }
            }
        }

    }
}
