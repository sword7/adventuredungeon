package me.sword7.adventuredungeon.lootpool.loot;

import me.sword7.adventuredungeon.dungeon.DungeonTag;
import me.sword7.adventuredungeon.lootpool.LootTag;
import me.sword7.adventuredungeon.util.weight.WeightedChoice;
import me.sword7.adventuredungeon.util.weight.WeightedRandomizer;
import org.bukkit.inventory.Inventory;

import java.util.List;
import java.util.Random;

public class LootGroup implements ILoot {

    private WeightedRandomizer<ILoot> loots = new WeightedRandomizer<>();

    public void add(WeightedChoice<ILoot> choice) {
        loots.add(choice);
    }

    @Override
    public void fill(Inventory inventory, Random rand, List<DungeonTag> dungeonTags, List<LootTag> lootTags, int luck) {
        loots.get(rand).fill(inventory, rand, dungeonTags, lootTags, luck);
    }

}
