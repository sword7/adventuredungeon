package me.sword7.adventuredungeon.lootpool;

import me.sword7.adventuredungeon.dungeon.DungeonTag;
import me.sword7.adventuredungeon.lootpool.loot.ILoot;
import me.sword7.adventuredungeon.util.weight.WeightedChoice;
import me.sword7.adventuredungeon.util.weight.WeightedRandomizer;

import java.util.List;
import java.util.Random;

public abstract class LootPool implements ILootPool {

    private WeightedRandomizer<ILoot> loots = new WeightedRandomizer<>();

    public void add(WeightedChoice<ILoot> choice) {
        loots.add(choice);
    }

    @Override
    public ILoot selectChest(Random rand, List<DungeonTag> dungeonTags, List<LootTag> lootTags, int luck) {
        return loots.get(rand);
    }

}
