package me.sword7.adventuredungeon.lootpool;

import me.sword7.adventuredungeon.dungeon.DungeonTag;
import me.sword7.adventuredungeon.dungeon.container.ContainerType;
import me.sword7.adventuredungeon.lootpool.item.ILootItem;
import me.sword7.adventuredungeon.lootpool.loot.ILoot;

import java.util.List;
import java.util.Random;

public interface ILootPool {

    ILoot selectChest(Random random, List<DungeonTag> dungeonTags, List<LootTag> lootTags, int luck);

    double getContainerDropChance(ContainerType containerType);

    ILootItem selectContainer(ContainerType containerType, Random random);

}
