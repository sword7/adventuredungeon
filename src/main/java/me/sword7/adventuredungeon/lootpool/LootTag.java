package me.sword7.adventuredungeon.lootpool;

import me.sword7.adventuredungeon.dungeon.DungeonType;
import me.sword7.adventuredungeon.dungeons.crypt.CryptLootTag;
import me.sword7.adventuredungeon.util.Tag;

public interface LootTag extends Tag {

    static LootTag getTag(DungeonType type, String string) {
        LootTag tag = null;
        try {
            tag = RarityTag.valueOf(string);
        } catch (Exception e) {
            //do nothing
        }

        if (tag == null) {
            try {
                switch (type) {
                    case CRYPT:
                        tag = CryptLootTag.valueOf(string);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                //do nothing
            }
        }

        return tag;

    }

}
