package me.sword7.adventuredungeon.lootpool;

import me.sword7.adventuredungeon.lootpool.item.*;
import me.sword7.adventuredungeon.util.weight.WeightedChoice;
import me.sword7.adventuredungeon.util.weight.WeightedRandomizer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LootUtil {

    public static ILootItem HEART;
    public static WeightedRandomizer<ItemStack> GEMS_AND_METALS = new WeightedRandomizer<>();
    public static ILootItem DIAMOND = new ItemSingleton(Material.DIAMOND);
    public static ILootItem EMERALDS = new ItemWeightedAmount(Material.EMERALD);
    public static ILootItem GOLD_INGOTS = new ItemWeightedAmount(Material.GOLD_INGOT);
    public static ILootItem IRON_INGOTS = new ItemWeightedAmount(Material.IRON_INGOT);
    public static ILootItem ARROWS = new ItemRangedAmount(new ItemStack(Material.ARROW), 1, 10);
    public static ILootItem SEEDS = new ItemCollection();

    public static void init() {

        ItemStack heartItem = new ItemStack(Material.RED_DYE);
        ItemMeta heartMeta = heartItem.getItemMeta();
        heartMeta.setCustomModelData(701);
        heartItem.setItemMeta(heartMeta);
        HEART = new ItemSingleton(heartItem);

        ItemWeightedAmount emeralds = (ItemWeightedAmount) EMERALDS;
        emeralds.addAmount(1, 6);
        emeralds.addAmount(2, 3);
        emeralds.addAmount(3, 2);
        emeralds.addAmount(4, 2);
        emeralds.addAmount(5, 2);
        emeralds.addAmount(6, 2);
        emeralds.addAmount(7, 1);
        ItemWeightedAmount goldIngots = (ItemWeightedAmount) GOLD_INGOTS;
        goldIngots.addAmount(1, 40);
        goldIngots.addAmount(2, 4);
        goldIngots.addAmount(3, 3);
        goldIngots.addAmount(4, 2);
        goldIngots.addAmount(5, 1);
        ItemWeightedAmount ironIngots = (ItemWeightedAmount) IRON_INGOTS;
        ironIngots.addAmount(1, 40);
        ironIngots.addAmount(2, 4);
        ironIngots.addAmount(3, 3);
        ironIngots.addAmount(4, 2);
        ironIngots.addAmount(5, 1);

        ItemCollection seeds = (ItemCollection) SEEDS;
        seeds.addItem(new WeightedChoice<>(new ItemRangedAmount(Material.WHEAT_SEEDS, 1, 6), 3));
        seeds.addItem(new WeightedChoice<>(new ItemRangedAmount(Material.PUMPKIN_SEEDS, 1, 4), 2));
        seeds.addItem(new WeightedChoice<>(new ItemRangedAmount(Material.MELON_SEEDS, 1, 4), 2));
        seeds.addItem(new WeightedChoice<>(new ItemRangedAmount(Material.BEETROOT_SEEDS, 1, 3), 1));
        seeds.addItem(new WeightedChoice<>(new ItemRangedAmount(Material.COCOA_BEANS, 1, 3), 1));

    }


}
