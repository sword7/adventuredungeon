package me.sword7.adventuredungeon.lootpool.item;

import org.bukkit.inventory.ItemStack;

import java.util.Random;

public interface ILootItem {

    ItemStack create(Random rand);

}
