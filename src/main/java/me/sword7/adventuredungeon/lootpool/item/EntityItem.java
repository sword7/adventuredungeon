package me.sword7.adventuredungeon.lootpool.item;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Slime;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class EntityItem implements ILootItem {

    private EntityType entityType;

    public EntityItem(EntityType entityType) {
        this.entityType = entityType;
    }

    public void spawnEntity(Location location) {
        if (entityType == EntityType.SLIME) {
            Slime slime = (Slime) location.getWorld().spawnEntity(location, entityType);
            slime.setSize(2);
        } else {
            location.getWorld().spawnEntity(location, entityType);
        }
    }

    @Override
    public ItemStack create(Random random) {
        return null;
    }

}
