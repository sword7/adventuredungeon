package me.sword7.adventuredungeon.lootpool.item;

import me.sword7.adventuredungeon.util.Util;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class ItemRangedAmount extends LootItem {

    private int min;
    private int max;

    public ItemRangedAmount(ItemStack base, int min, int max) {
        super(base);
        this.min = min;
        this.max = max;
    }

    public ItemRangedAmount(Material base, int min, int max) {
        super(new ItemStack(base));
        this.min = min;
        this.max = max;
    }

    @Override
    public ItemStack create(Random rand) {
        ItemStack toReturn = base.clone();
        toReturn.setAmount(Util.randomInt(min, max));
        return toReturn;
    }

}
