package me.sword7.adventuredungeon.lootpool.item;

import me.sword7.adventuredungeon.util.weight.WeightedChoice;
import me.sword7.adventuredungeon.util.weight.WeightedRandomizer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class ItemWeightedAmount extends LootItem {

    private WeightedRandomizer<Integer> amounts = new WeightedRandomizer<>();

    public ItemWeightedAmount(ItemStack base) {
        super(base);
    }

    public ItemWeightedAmount(Material base) {
        super(new ItemStack(base));
    }

    public void addAmount(int amount, int weight) {
        amounts.add(new WeightedChoice<>(amount, weight));
    }

    @Override
    public ItemStack create(Random rand) {
        ItemStack toReturn = base.clone();
        int amount = 1;
        if (!amounts.isEmpty()) amount = amounts.get(rand);
        if (amount <= 0) amount = 1;
        toReturn.setAmount(amount);
        return toReturn;
    }

}
