package me.sword7.adventuredungeon.lootpool.item;

import me.sword7.adventuredungeon.util.weight.WeightedChoice;
import me.sword7.adventuredungeon.util.weight.WeightedRandomizer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class ItemCollection implements ILootItem {

    private WeightedRandomizer<ILootItem> items = new WeightedRandomizer<>();

    public void addItem(WeightedChoice<ILootItem> choice) {
        items.add(choice);
    }

    public void addItem(ILootItem item, int weight) {
        items.add(new WeightedChoice<>(item, weight));
    }

    public void addItem(Material material, int weight) {
        items.add(new WeightedChoice<>(new ItemSingleton(material), weight));
    }

    public ILootItem select(Random rand) {
        return items.get(rand);
    }

    @Override
    public ItemStack create(Random rand) {
        return select(rand).create(rand);
    }

    public ItemCollection clone() {
        ItemCollection newItemCollection = new ItemCollection();
        newItemCollection.items = new WeightedRandomizer(this.items);
        return newItemCollection;
    }
}
