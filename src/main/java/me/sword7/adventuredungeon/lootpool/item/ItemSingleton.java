package me.sword7.adventuredungeon.lootpool.item;

import me.sword7.adventuredungeon.util.Util;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class ItemSingleton extends LootItem {

    private double minDurability = 0.05f;
    private double maxDurability = 0.6f;

    public ItemSingleton(ItemStack base) {
        super(base);
    }

    public ItemSingleton(ItemStack base, double minDurability, double maxDurability) {
        super(base);
        this.minDurability = minDurability;
        this.maxDurability = maxDurability;
    }

    public ItemSingleton(Material base) {
        super(new ItemStack(base));
    }

    public ItemSingleton(Material base, double minDurability, double maxDurability) {
        super(new ItemStack(base));
        this.minDurability = minDurability;
        this.maxDurability = maxDurability;
    }


    @Override
    public ItemStack create(Random rand) {
        ItemStack toReturn = base.clone();
        Util.applyDurability(toReturn, minDurability, maxDurability);
        return toReturn;
    }

}
