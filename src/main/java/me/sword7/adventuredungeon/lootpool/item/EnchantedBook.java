package me.sword7.adventuredungeon.lootpool.item;

import me.sword7.adventuredungeon.util.weight.WeightedRandomizer;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.util.Random;

public class EnchantedBook extends ItemSingleton {

    WeightedRandomizer<Enchantment> enchants;

    public EnchantedBook(WeightedRandomizer<Enchantment> enchants) {
        super(Material.ENCHANTED_BOOK);
        this.enchants = enchants;
    }

    @Override
    public ItemStack create(Random rand) {

        ItemStack toReturn = base.clone();

        double r = rand.nextDouble();
        int enchantNum = 1;
        if (r < 0.15) {
            enchantNum = 2;
        } else if (r < 0.20) {
            enchantNum = 3;
        }

        EnchantmentStorageMeta meta = (EnchantmentStorageMeta) toReturn.getItemMeta();

        for (int i = 0; i < enchantNum; i++) {
            Enchantment enchantment = enchants.get(rand);
            if (!meta.hasStoredEnchant(enchantment)) {
                meta.addStoredEnchant(enchantment, selectLevel(enchantment, rand), false);
            }
        }
        toReturn.setItemMeta(meta);

        return toReturn;

    }

    public int selectLevel(Enchantment enchantment, Random rand) {
        double r = rand.nextDouble();
        int maxLevel = enchantment.getMaxLevel();
        if (maxLevel == 1) {
            return 1;
        } else if (maxLevel == 2) {
            if (r < 0.4) {
                return 2;
            } else {
                return 1;
            }
        } else if (maxLevel == 3) {
            if (r < 0.33) {
                return 3;
            } else if (r < 0.66) {
                return 2;
            } else {
                return 1;
            }
        } else if (maxLevel == 4) {
            if (r < 0.15) {
                return 4;
            } else if (r < 0.50) {
                return 3;
            } else if (r < 0.85) {
                return 2;
            } else {
                return 1;
            }
        } else {
            if (r < 0.5) {
                return 5;
            } else if (r < 0.2) {
                return 4;
            } else if (r < 0.55) {
                return 3;
            } else if (r < 0.90) {
                return 2;
            } else {
                return 1;
            }
        }
    }


}
