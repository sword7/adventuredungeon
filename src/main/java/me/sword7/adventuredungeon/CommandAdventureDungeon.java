package me.sword7.adventuredungeon;

import me.sword7.adventuredungeon.config.Language;
import me.sword7.adventuredungeon.config.Version;
import me.sword7.adventuredungeon.util.Help;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginDescriptionFile;

public class CommandAdventureDungeon implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if (args.length > 0) {
            String sub = args[0].toLowerCase();
            if (sub.equals("help")) {
                Help.sendTo(sender);
            } else if (sub.equals("info")) {
                showInfo(sender);
            } else {
                showOverview(sender);
            }
        } else {
            showOverview(sender);
        }

        return false;
    }

    private void showInfo(CommandSender sender) {
        PluginDescriptionFile descriptionFile = AdventureDungeon.getPlugin().getDescription();
        sender.sendMessage("");
        sender.sendMessage(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "__Adventure Dungeon_______");
        sender.sendMessage(ChatColor.GRAY + Language.PLUGIN_AUTHOR.toString() + ": " + ChatColor.WHITE + descriptionFile.getAuthors().get(0));
        sender.sendMessage(ChatColor.GRAY + Language.PLUGIN_VERSION.toString() + ": " + ChatColor.WHITE + descriptionFile.getVersion());
    }


    private void showOverview(CommandSender sender) {
        sender.sendMessage("");
        sender.sendMessage(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "__Adventure Dungeon_______");
        sender.sendMessage(ChatColor.GRAY + Language.PLUGIN_DESCRIPTION.toString());
        sender.sendMessage("");

        if (Version.hasBungeeChat()) {
            sender.spigot().sendMessage(Language.PLUGIN_READ_MORE.getWithLink(ChatColor.GRAY, Language.PLUGIN_WIKI.toString(), "https://gitlab.com/sword7/adventuredungeon/-/wikis/home"));
        } else {
            sender.sendMessage(ChatColor.GRAY + Language.PLUGIN_READ_MORE.toString().replaceAll("\\[link\\]", Language.PLUGIN_WIKI.toString()) +
                    " " + ChatColor.AQUA + "https://gitlab.com/sword7/adventuredungeon/-/wikis/home");
        }
        sender.sendMessage(ChatColor.DARK_GRAY.toString() + ChatColor.BOLD + "------- " + Language.PLUGIN_OPTIONS + " -------");
        sender.sendMessage(ChatColor.GRAY + "/adventuredungeon help: " + ChatColor.WHITE + ChatColor.ITALIC + Language.HELP_ADVENTUREDUNGEON_HELP);
        sender.sendMessage(ChatColor.GRAY + "/adventuredungeon info: " + ChatColor.WHITE + ChatColor.ITALIC + Language.HELP_ADVENTUREDUNGEON_INFO);

    }


}
