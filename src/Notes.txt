

 //Next...
 - async (switch to sync on shutdown??, alter leaf blower to ignore logs within clear section when relevant)
 - more theme stuff
 - dungeon sounds
 - dungeon heart that removes protection when destroyed (boss/blocks/fire to break)
 - AltBlock ThemeBlock extension (chest/spawner/torch chance) + (more randomized pots/webs/skulls/small crates/cave-ins)
 - build/place detect/load/save all async
 - name generator?
 - config/lang
 - loot (brewing stands)


 Mechanics:
 - explosions destroy cracked blocks
 - can destroy web w/ sword (with attack click?)
 - can set webs on fire
 - spider dungeon spiders have chance to place more web if not much web close to them
 - can mine ore w/ pick (with attack click?)
 - rooms seal with iron bars mob fights


 Features:
 - natural/vortex/portal dungeons
 - party system
 - Boss room (skeleton horseman) (spawner that spawns boss then poofs and starts boss fight runnable)
 - fancy chests (organized contents)
 - More crypt entrances (mountain side/ravine-side/cave-side doorway) (one side open, other side solid)
 - key/door
 - special chests that animate to open and give 1 special item (key/item)?

 Misc:
 - 3D helmets w/ blockbench
 - bottles O enchantment in chests
 - invisible item frame bones to crypts (Custom Model Data?)
Generation -  https://vazgriz.com/119/procedurally-generated-dungeons/
Generation -  https://www.youtube.com/watch?v=t-VUpX-xVo4
Generation - https://www.youtube.com/watch?v=o00a2FsXsXM
Generation - https://marian42.itch.io/wfc
Zombie rise from ground - https://www.youtube.com/watch?v=olVhPNp-i-8
Water/Cave structures - https://www.youtube.com/watch?v=IqzWCPZtgv4
Perlin - https://www.youtube.com/watch?v=wbpMiKiSKm8
Mod - https://www.planetminecraft.com/project/ftc-dungeons-custom-bosses-enchants-and-rewards/
Mod - https://www.youtube.com/watch?v=6vclCbWww1g


Placement modes: (replace solid w/ air), (replace blocks except to set air), (don't place if not air), (place only if not air)
